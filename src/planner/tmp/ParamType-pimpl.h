// Not copyrighted - public domain.
//
// This sample parser implementation was generated by CodeSynthesis XSD/e,
// an XML Schema to C++ data binding compiler for embedded systems. You
// may use it in your programs without any restrictions.
//

#ifndef PARAM_TYPE_PIMPL_H
#define PARAM_TYPE_PIMPL_H

#include "ParamType-pskel.h"

namespace Operations
{
  class ParamType_pimpl: public ParamType_pskel
  {
    public:
    virtual void
    pre ();

    // Elements.
    //
    virtual void
    Symbol (const ::std::string&);

    virtual void
    FunctionExpression (VAL::expression*);

    virtual void
    ActionParameter (VAL::parameter_symbol*);

    virtual VAL::parse_category*
    post_ParamType ();
  };
}

#endif // PARAM_TYPE_PIMPL_H
