// Copyright (c) 2005-2011 Code Synthesis Tools CC
//
// This program was generated by CodeSynthesis XSD/e, an XML Schema
// to C++ data binding compiler for embedded systems.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
//
//

// Begin prologue.
//
//
// End prologue.

#include "TimeoutType-pskel.h"

namespace Operations
{
  // TimeoutType_pskel
  //

  void TimeoutType_pskel::
  MinTime_parser (::xml_schema::duration_pskel& p)
  {
    this->MinTime_parser_ = &p;
  }

  void TimeoutType_pskel::
  MaxTime_parser (::xml_schema::duration_pskel& p)
  {
    this->MaxTime_parser_ = &p;
  }

  void TimeoutType_pskel::
  parsers (::xml_schema::duration_pskel& MinTime,
           ::xml_schema::duration_pskel& MaxTime)
  {
    this->MinTime_parser_ = &MinTime;
    this->MaxTime_parser_ = &MaxTime;
  }

  TimeoutType_pskel::
  TimeoutType_pskel ()
  : TimeoutType_impl_ (0),
    MinTime_parser_ (0),
    MaxTime_parser_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }

  TimeoutType_pskel::
  TimeoutType_pskel (TimeoutType_pskel* impl, void*)
  : ::xsde::cxx::parser::validating::complex_content (impl, 0),
    TimeoutType_impl_ (impl),
    MinTime_parser_ (0),
    MaxTime_parser_ (0),
    v_state_stack_ (sizeof (v_state_), &v_state_first_)
  {
  }
}

#include <assert.h>

namespace Operations
{
  // TimeoutType_pskel
  //

  void TimeoutType_pskel::
  MinTime (const ::xml_schema::duration& x)
  {
    if (this->TimeoutType_impl_)
      this->TimeoutType_impl_->MinTime (x);
  }

  void TimeoutType_pskel::
  MaxTime (const ::xml_schema::duration& x)
  {
    if (this->TimeoutType_impl_)
      this->TimeoutType_impl_->MaxTime (x);
  }

  void TimeoutType_pskel::
  _reset ()
  {
    if (this->resetting_)
      return;

    typedef ::xsde::cxx::parser::validating::complex_content base;
    base::_reset ();

    this->v_state_stack_.clear ();

    this->resetting_ = true;

    if (this->MinTime_parser_)
      this->MinTime_parser_->_reset ();

    if (this->MaxTime_parser_)
      this->MaxTime_parser_->_reset ();

    this->resetting_ = false;
  }
}

#include <assert.h>

namespace Operations
{
  // Element validation and dispatch functions for TimeoutType_pskel.
  //
  bool TimeoutType_pskel::
  _start_element_impl (const ::xsde::cxx::ro_string& ns,
                       const ::xsde::cxx::ro_string& n)
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_* vd = vs.data + (vs.size - 1);

    if (vd->func == 0 && vd->state == 0)
    {
      typedef ::xsde::cxx::parser::validating::complex_content base;
      if (base::_start_element_impl (ns, n))
        return true;
      else
        vd->state = 1;
    }

    while (vd->func != 0)
    {
      (this->*vd->func) (vd->state, vd->count, ns, n, true);

      vd = vs.data + (vs.size - 1);

      if (vd->state == ~0UL && !ctx.error_type ())
        vd = vs.data + (--vs.size - 1);
      else
        break;
    }

    if (vd->func == 0)
    {
      if (vd->state != ~0UL)
      {
        unsigned long s = ~0UL;

        if (n == "MinTime" &&
            ns == "Operations")
          s = 0UL;
        else if (n == "MaxTime" &&
                 ns == "Operations")
          s = 1UL;

        if (s != ~0UL)
        {
          vd->count++;
          vd->state = ~0UL;

          vd = vs.data + vs.size++;
          vd->func = &TimeoutType_pskel::sequence_0;
          vd->state = s;
          vd->count = 0;

          this->sequence_0 (vd->state, vd->count, ns, n, true);
        }
        else
        {
          return false;
        }
      }
      else
        return false;
    }

    return true;
  }

  bool TimeoutType_pskel::
  _end_element_impl (const ::xsde::cxx::ro_string& ns,
                     const ::xsde::cxx::ro_string& n)
  {
    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_& vd = vs.data[vs.size - 1];

    if (vd.func == 0 && vd.state == 0)
    {
      typedef ::xsde::cxx::parser::validating::complex_content base;
      if (!base::_end_element_impl (ns, n))
        assert (false);
      return true;
    }

    assert (vd.func != 0);
    (this->*vd.func) (vd.state, vd.count, ns, n, false);

    if (vd.state == ~0UL)
      vs.size--;

    return true;
  }

  void TimeoutType_pskel::
  _pre_e_validate ()
  {
    this->v_state_stack_.push ();
    static_cast< v_state_* > (this->v_state_stack_.top ())->size = 0;

    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_& vd = vs.data[vs.size++];

    vd.func = 0;
    vd.state = 0;
    vd.count = 0;
  }

  void TimeoutType_pskel::
  _post_e_validate ()
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    v_state_& vs = *static_cast< v_state_* > (this->v_state_stack_.top ());
    v_state_descr_* vd = vs.data + (vs.size - 1);

    ::xsde::cxx::ro_string empty;
    while (vd->func != 0)
    {
      (this->*vd->func) (vd->state, vd->count, empty, empty, true);

      if (ctx.error_type ())
        return;

      assert (vd->state == ~0UL);
      vd = vs.data + (--vs.size - 1);
    }


    this->v_state_stack_.pop ();
  }

  void TimeoutType_pskel::
  sequence_0 (unsigned long& state,
              unsigned long& count,
              const ::xsde::cxx::ro_string& ns,
              const ::xsde::cxx::ro_string& n,
              bool start)
  {
    ::xsde::cxx::parser::context& ctx = this->_context ();

    XSDE_UNUSED (ctx);

    switch (state)
    {
      case 0UL:
      {
        if (n == "MinTime" &&
            ns == "Operations")
        {
          if (start)
          {
            if (this->MinTime_parser_)
            {
              this->MinTime_parser_->pre ();
              ctx.nested_parser (this->MinTime_parser_);
            }
          }
          else
          {
            if (this->MinTime_parser_ != 0)
            {
              const ::xml_schema::duration& tmp = this->MinTime_parser_->post_duration ();
              this->MinTime (tmp);
            }

            count = 0;
            state = 1UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = 1UL;
          // Fall through.
        }
      }
      case 1UL:
      {
        if (n == "MaxTime" &&
            ns == "Operations")
        {
          if (start)
          {
            if (this->MaxTime_parser_)
            {
              this->MaxTime_parser_->pre ();
              ctx.nested_parser (this->MaxTime_parser_);
            }
          }
          else
          {
            if (this->MaxTime_parser_ != 0)
            {
              const ::xml_schema::duration& tmp = this->MaxTime_parser_->post_duration ();
              this->MaxTime (tmp);
            }

            count = 0;
            state = ~0UL;
          }

          break;
        }
        else
        {
          assert (start);
          count = 0;
          state = ~0UL;
          // Fall through.
        }
      }
      case ~0UL:
        break;
    }
  }
}

namespace Operations
{
}

namespace Operations
{
}

// Begin epilogue.
//
//
// End epilogue.

