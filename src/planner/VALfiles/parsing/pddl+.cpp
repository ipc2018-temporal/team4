/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 17 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:339  */

/*
Error reporting:
Intention is to provide error token on most bracket expressions,
so synchronisation can occur on next CLOSE_BRAC.
Hence error should be generated for innermost expression containing error.
Expressions which cause errors return a NULL values, and parser
always attempts to carry on.
This won't behave so well if CLOSE_BRAC is missing.

Naming conventions:
Generally, the names should be similar to the PDDL2.1 spec.
During development, they have also been based on older PDDL specs,
older PDDL+ and TIM parsers, and this shows in places.

All the names of fields in the semantic value type begin with t_
Corresponding categories in the grammar begin with c_
Corresponding classes have no prefix.

PDDL grammar       yacc grammar      type of corresponding semantic val.

thing+             c_things          thing_list
(thing+)           c_thing_list      thing_list

*/

#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <ctype.h>

// This is now copied locally to avoid relying on installation
// of flex++.

//#include "FlexLexer.h"
//#include <FlexLexer.h>

#include "ptree.h"
#include "parse_error.h"

#define YYDEBUG 1

int yyerror(char *);

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", ((char *)msgid))
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) ((char *) msgid)
# endif
#endif

extern int yylex();

using namespace VAL;


#line 128 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif


/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    OPEN_BRAC = 258,
    CLOSE_BRAC = 259,
    MODULES = 260,
    OPEN_SQ = 261,
    CLOSE_SQ = 262,
    DOT = 263,
    CLASSES = 264,
    CLASS = 265,
    DEFINE = 266,
    PDDLDOMAIN = 267,
    REQS = 268,
    EQUALITY = 269,
    STRIPS = 270,
    ADL = 271,
    NEGATIVE_PRECONDITIONS = 272,
    TYPING = 273,
    DISJUNCTIVE_PRECONDS = 274,
    EXT_PRECS = 275,
    UNIV_PRECS = 276,
    QUANT_PRECS = 277,
    COND_EFFS = 278,
    FLUENTS = 279,
    OBJECTFLUENTS = 280,
    NUMERICFLUENTS = 281,
    ACTIONCOSTS = 282,
    TIME = 283,
    DURATIVE_ACTIONS = 284,
    DURATION_INEQUALITIES = 285,
    CONTINUOUS_EFFECTS = 286,
    DERIVED_PREDICATES = 287,
    TIMED_INITIAL_LITERALS = 288,
    PREFERENCES = 289,
    CONSTRAINTS = 290,
    ACTION = 291,
    PROCESS = 292,
    EVENT = 293,
    DURATIVE_ACTION = 294,
    DERIVED = 295,
    CONSTANTS = 296,
    PREDS = 297,
    FUNCTIONS = 298,
    TYPES = 299,
    ARGS = 300,
    PRE = 301,
    CONDITION = 302,
    PREFERENCE = 303,
    START_PRE = 304,
    END_PRE = 305,
    EFFECTS = 306,
    INITIAL_EFFECT = 307,
    FINAL_EFFECT = 308,
    INVARIANT = 309,
    DURATION = 310,
    AT_START = 311,
    AT_END = 312,
    OVER_ALL = 313,
    AND = 314,
    OR = 315,
    EXISTS = 316,
    FORALL = 317,
    IMPLY = 318,
    NOT = 319,
    WHEN = 320,
    WHENEVER = 321,
    EITHER = 322,
    PROBLEM = 323,
    FORDOMAIN = 324,
    INITIALLY = 325,
    OBJECTS = 326,
    GOALS = 327,
    EQ = 328,
    LENGTH = 329,
    SERIAL = 330,
    PARALLEL = 331,
    METRIC = 332,
    MINIMIZE = 333,
    MAXIMIZE = 334,
    HASHT = 335,
    DURATION_VAR = 336,
    TOTAL_TIME = 337,
    INCREASE = 338,
    DECREASE = 339,
    SCALE_UP = 340,
    SCALE_DOWN = 341,
    ASSIGN = 342,
    GREATER = 343,
    GREATEQ = 344,
    LESS = 345,
    LESSEQ = 346,
    Q = 347,
    COLON = 348,
    NUMBER = 349,
    ALWAYS = 350,
    SOMETIME = 351,
    WITHIN = 352,
    ATMOSTONCE = 353,
    SOMETIMEAFTER = 354,
    SOMETIMEBEFORE = 355,
    ALWAYSWITHIN = 356,
    HOLDDURING = 357,
    HOLDAFTER = 358,
    ISVIOLATED = 359,
    BOGUS = 360,
    CONTROL = 361,
    NAME = 362,
    FUNCTION_SYMBOL = 363,
    INTVAL = 364,
    FLOATVAL = 365,
    AT_TIME = 366,
    HYPHEN = 367,
    PLUS = 368,
    MUL = 369,
    DIV = 370,
    UMINUS = 371
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 79 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:355  */

    parse_category* t_parse_category;

    effect_lists* t_effect_lists;
    effect* t_effect;
    simple_effect* t_simple_effect;
    cond_effect*   t_cond_effect;
    forall_effect* t_forall_effect;
    timed_effect* t_timed_effect;

    quantifier t_quantifier;
    metric_spec*  t_metric;
    optimization t_optimization;

    symbol* t_symbol;
    var_symbol*   t_var_symbol;
    pddl_type*    t_type;
    pred_symbol*  t_pred_symbol;
    func_symbol*  t_func_symbol;
    const_symbol* t_const_symbol;
    class_symbol* t_class;

    parameter_symbol_list* t_parameter_symbol_list;
    var_symbol_list* t_var_symbol_list;
    const_symbol_list* t_const_symbol_list;
    pddl_type_list* t_type_list;

    proposition* t_proposition;
    pred_decl* t_pred_decl;
    pred_decl_list* t_pred_decl_list;
    func_decl* t_func_decl;
    func_decl_list* t_func_decl_list;

    goal* t_goal;
    con_goal * t_con_goal;
    goal_list* t_goal_list;

    func_term* t_func_term;
    assignment* t_assignment;
    expression* t_expression;
    num_expression* t_num_expression;
    assign_op t_assign_op;
    comparison_op t_comparison_op;

    structure_def* t_structure_def;
    structure_store* t_structure_store;

    action* t_action_def;
    event* t_event_def;
    process* t_process_def;
    durative_action* t_durative_action_def;
    derivation_rule* t_derivation_rule;

    problem* t_problem;
    length_spec* t_length_spec;

    domain* t_domain;

    pddl_req_flag t_pddl_req_flag;

    plan* t_plan;
    plan_step* t_step;

    int ival;
    double fval;

    char* cp;
    int t_dummy;

    var_symbol_table * vtab;

  class_def * t_class_def;
  //  classes_list* t_classes;


#line 358 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);



/* Copy the second part of user declarations.  */

#line 375 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  17
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   984

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  117
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  131
/* YYNRULES -- Number of rules.  */
#define YYNRULES  356
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  822

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   371

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   260,   260,   261,   262,   266,   272,   279,   280,   281,
     282,   284,   286,   288,   289,   292,   296,   299,   301,   303,
     304,   308,   315,   322,   323,   328,   330,   335,   337,   345,
     353,   355,   363,   368,   370,   374,   376,   383,   383,   386,
     399,   408,   417,   424,   430,   437,   439,   445,   454,   464,
     469,   470,   474,   475,   483,   490,   499,   505,   507,   509,
     516,   522,   526,   530,   534,   539,   546,   551,   553,   557,
     559,   563,   576,   578,   580,   583,   587,   593,   594,   596,
     598,   607,   608,   609,   610,   611,   615,   616,   620,   622,
     624,   631,   632,   633,   635,   639,   641,   649,   651,   659,
     664,   669,   672,   679,   680,   684,   686,   688,   692,   696,
     702,   706,   710,   716,   718,   726,   731,   737,   738,   742,
     743,   747,   749,   751,   758,   759,   760,   762,   767,   769,
     771,   773,   775,   780,   786,   792,   797,   798,   802,   803,
     805,   806,   807,   811,   813,   815,   817,   822,   824,   827,
     830,   836,   837,   838,   846,   850,   853,   857,   862,   869,
     874,   879,   884,   889,   891,   893,   895,   897,   902,   904,
     906,   908,   910,   912,   913,   914,   918,   920,   922,   928,
     929,   932,   935,   937,   940,   957,   959,   961,   967,   968,
     969,   970,   971,   983,   990,   992,   996,   997,  1001,  1003,
    1005,  1007,  1011,  1016,  1018,  1020,  1022,  1029,  1031,  1036,
    1038,  1042,  1047,  1049,  1054,  1056,  1059,  1061,  1063,  1065,
    1067,  1069,  1071,  1073,  1075,  1077,  1082,  1084,  1088,  1090,
    1093,  1096,  1099,  1102,  1108,  1110,  1115,  1117,  1127,  1134,
    1141,  1146,  1151,  1156,  1158,  1165,  1167,  1174,  1176,  1183,
    1185,  1192,  1193,  1197,  1198,  1199,  1200,  1201,  1202,  1205,
    1214,  1220,  1229,  1240,  1247,  1258,  1264,  1274,  1280,  1282,
    1285,  1303,  1310,  1312,  1314,  1318,  1320,  1325,  1328,  1332,
    1334,  1336,  1338,  1343,  1348,  1353,  1354,  1356,  1357,  1359,
    1361,  1362,  1363,  1364,  1365,  1367,  1370,  1373,  1374,  1375,
    1377,  1386,  1389,  1392,  1394,  1396,  1398,  1400,  1402,  1408,
    1412,  1417,  1429,  1436,  1437,  1438,  1439,  1440,  1442,  1444,
    1445,  1448,  1451,  1454,  1457,  1461,  1463,  1470,  1473,  1477,
    1484,  1485,  1490,  1491,  1492,  1493,  1494,  1496,  1500,  1501,
    1502,  1503,  1507,  1508,  1513,  1514,  1520,  1523,  1525,  1528,
    1532,  1536,  1542,  1546,  1552,  1560,  1561
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "OPEN_BRAC", "CLOSE_BRAC", "MODULES",
  "OPEN_SQ", "CLOSE_SQ", "DOT", "CLASSES", "CLASS", "DEFINE", "PDDLDOMAIN",
  "REQS", "EQUALITY", "STRIPS", "ADL", "NEGATIVE_PRECONDITIONS", "TYPING",
  "DISJUNCTIVE_PRECONDS", "EXT_PRECS", "UNIV_PRECS", "QUANT_PRECS",
  "COND_EFFS", "FLUENTS", "OBJECTFLUENTS", "NUMERICFLUENTS", "ACTIONCOSTS",
  "TIME", "DURATIVE_ACTIONS", "DURATION_INEQUALITIES",
  "CONTINUOUS_EFFECTS", "DERIVED_PREDICATES", "TIMED_INITIAL_LITERALS",
  "PREFERENCES", "CONSTRAINTS", "ACTION", "PROCESS", "EVENT",
  "DURATIVE_ACTION", "DERIVED", "CONSTANTS", "PREDS", "FUNCTIONS", "TYPES",
  "ARGS", "PRE", "CONDITION", "PREFERENCE", "START_PRE", "END_PRE",
  "EFFECTS", "INITIAL_EFFECT", "FINAL_EFFECT", "INVARIANT", "DURATION",
  "AT_START", "AT_END", "OVER_ALL", "AND", "OR", "EXISTS", "FORALL",
  "IMPLY", "NOT", "WHEN", "WHENEVER", "EITHER", "PROBLEM", "FORDOMAIN",
  "INITIALLY", "OBJECTS", "GOALS", "EQ", "LENGTH", "SERIAL", "PARALLEL",
  "METRIC", "MINIMIZE", "MAXIMIZE", "HASHT", "DURATION_VAR", "TOTAL_TIME",
  "INCREASE", "DECREASE", "SCALE_UP", "SCALE_DOWN", "ASSIGN", "GREATER",
  "GREATEQ", "LESS", "LESSEQ", "Q", "COLON", "NUMBER", "ALWAYS",
  "SOMETIME", "WITHIN", "ATMOSTONCE", "SOMETIMEAFTER", "SOMETIMEBEFORE",
  "ALWAYSWITHIN", "HOLDDURING", "HOLDAFTER", "ISVIOLATED", "BOGUS",
  "CONTROL", "NAME", "FUNCTION_SYMBOL", "INTVAL", "FLOATVAL", "AT_TIME",
  "HYPHEN", "PLUS", "MUL", "DIV", "UMINUS", "$accept", "mystartsymbol",
  "c_domain", "c_preamble", "c_domain_name", "c_new_class", "c_class",
  "c_classes", "c_class_seq", "c_domain_require_def", "c_reqs",
  "c_pred_decls", "c_pred_decl", "c_new_pred_symbol", "c_pred_symbol",
  "c_init_pred_symbol", "c_func_decls", "c_func_decl", "c_ntype",
  "c_new_func_symbol", "c_typed_var_list", "c_control_params_list",
  "c_var_symbol_list", "c_typed_consts", "c_const_symbols",
  "c_new_const_symbols", "c_typed_types", "c_parameter_symbols",
  "c_declaration_var_symbol", "c_var_symbol", "c_const_symbol",
  "c_new_const_symbol", "c_either_type", "c_new_primitive_type",
  "c_primitive_type", "c_new_primitive_types", "c_primitive_types",
  "c_init_els", "c_timed_initial_literal", "c_effects", "c_effect",
  "c_a_effect", "c_p_effect", "c_p_effects", "c_conj_effect",
  "c_da_effect", "c_da_effects", "c_timed_effect",
  "c_cts_only_timed_effect", "c_da_cts_only_effect",
  "c_da_cts_only_effects", "c_a_effect_da", "c_p_effect_da",
  "c_p_effects_da", "c_f_assign_da", "c_proc_effect", "c_proc_effects",
  "c_f_exp_da", "c_binary_expr_da", "c_duration_constraint", "c_d_op",
  "c_d_value", "c_duration_constraints", "c_neg_simple_effect",
  "c_pos_simple_effect", "c_init_neg_simple_effect",
  "c_init_pos_simple_effect", "c_forall_effect", "c_cond_effect",
  "c_assignment", "c_f_exp", "c_f_exp_t", "c_number", "c_f_head",
  "c_ground_f_head", "c_comparison_op", "c_pre_goal_descriptor",
  "c_pref_con_goal", "c_pref_goal", "c_pref_con_goal_list",
  "c_pref_goal_descriptor", "c_constraint_goal_list", "c_constraint_goal",
  "c_goal_descriptor", "c_pre_goal_descriptor_list", "c_goal_list",
  "c_forall", "c_exists", "c_proposition", "c_derived_proposition",
  "c_init_proposition", "c_predicates", "c_functions_def",
  "c_constraints_def", "c_constraints_probdef", "c_structure_defs",
  "c_structure_def", "c_class_def", "c_rule_head", "c_derivation_rule",
  "c_action_def", "c_event_def", "c_process_def", "c_control",
  "c_durative_action_def", "c_da_def_body", "c_da_gd", "c_da_gds",
  "c_timed_gd", "c_args_head", "c_require_key", "c_domain_constants",
  "c_type_names", "c_problem", "c_problem_body", "c_objects",
  "c_initial_state", "c_goals", "c_goal_spec", "c_metric_spec",
  "c_length_spec", "c_optimization", "c_ground_f_exp",
  "c_binary_ground_f_exp", "c_binary_ground_f_pexps",
  "c_binary_ground_f_mexps", "c_plan", "c_step_t_d", "c_step_d", "c_step",
  "c_float", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371
};
# endif

#define YYPACT_NINF -725

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-725)))

#define YYTABLE_NINF -81

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
      82,    39,   108,  -725,  -725,    64,  -725,  -725,  -725,   106,
    -725,    77,   -14,    96,   128,   106,   106,  -725,   131,  -725,
     161,   207,    13,   369,  -725,   247,   128,  -725,  -725,   273,
    -725,   157,    21,  -725,   751,   291,   301,   301,   301,   301,
     301,   310,  -725,  -725,  -725,  -725,  -725,  -725,  -725,   301,
     301,  -725,  -725,  -725,   367,  -725,   375,   280,   309,   449,
     402,    29,    37,    40,    47,  -725,   312,   422,   351,  -725,
     406,  -725,  -725,  -725,  -725,  -725,  -725,   513,  -725,  -725,
    -725,  -725,   421,  -725,   280,   426,  -725,   434,   484,   651,
     489,   653,   495,   507,   470,   509,   470,   516,   470,   518,
     470,  -725,   520,   414,   312,   533,    52,   550,   558,   568,
     282,   571,   249,   -19,   578,   527,  -725,  -725,   544,   585,
    -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,
    -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,
    -725,  -725,  -725,  -725,  -725,  -725,  -725,  -725,   578,   590,
    -725,   578,   578,   219,   578,   578,   578,   219,   219,   219,
     595,  -725,  -725,  -725,   633,  -725,   645,  -725,   646,  -725,
     647,  -725,    34,  -725,  -725,   655,  -725,   565,  -725,  -725,
    -725,    65,  -725,  -725,  -725,  -725,    34,  -725,  -725,  -725,
     565,   628,   657,  -725,   555,  -725,   659,   316,  -725,   660,
     690,  -725,  -725,   578,   691,   578,   578,   578,   219,   578,
     565,   565,   565,   565,   565,   626,  -725,   312,   312,  -725,
     589,   694,   587,   700,  -725,   565,  -725,  -725,   701,   578,
     578,  -725,   578,   578,   115,  -725,  -725,  -725,  -725,  -725,
      89,   703,   704,  -725,   705,  -725,  -725,  -725,  -725,  -725,
     710,  -725,   724,   726,   578,   578,   732,   733,   740,   742,
     743,   753,  -725,  -725,  -725,  -725,   565,  -725,    34,  -725,
     755,  -725,  -725,  -725,  -725,   337,   361,   578,   758,   192,
     270,   601,  -725,    89,  -725,  -725,   565,   565,   735,  -725,
    -725,  -725,   763,   765,  -725,   590,   667,   729,   730,   671,
     135,  -725,   565,   565,   666,  -725,  -725,  -725,   775,  -725,
    -725,   673,  -725,   773,  -725,    89,    89,    89,    89,   777,
    -725,   795,   803,   809,   244,   735,   735,   810,   735,   735,
     735,   735,   735,  -725,  -725,   811,   813,   578,   578,   814,
     764,  -725,  -725,  -725,  -725,   727,  -725,  -725,  -725,  -725,
     210,   230,    74,    89,    89,    89,   712,  -725,   578,   578,
     432,  -725,   312,  -725,   373,   126,   813,  -725,  -725,  -725,
    -725,  -725,  -725,  -725,  -725,  -725,   338,   767,  -725,  -725,
     771,   772,   -30,   821,  -725,  -725,  -725,  -725,   822,   823,
     824,   825,  -725,   826,   827,   828,   468,   829,  -725,   457,
     830,   716,   728,   831,  -725,  -725,    69,   832,  -725,    55,
     492,   835,   836,   837,   836,   838,   731,   709,  -725,  -725,
    -725,  -725,  -725,   246,  -725,  -725,  -725,    73,   841,   842,
    -725,   223,  -725,  -725,  -725,  -725,  -725,  -725,   202,   843,
    -725,   512,  -725,  -725,  -725,  -725,   844,  -725,   578,   845,
    -725,  -725,  -725,   494,   565,   441,   846,  -725,  -725,  -725,
    -725,  -725,  -725,   184,   847,   848,  -725,   747,   850,   851,
    -725,  -725,  -725,  -725,   754,    24,  -725,   590,   852,  -725,
     498,  -725,   565,   854,    42,  -725,  -725,  -725,  -725,   746,
    -725,   855,   756,  -725,  -725,    69,    69,    69,    69,   856,
    -725,   857,  -725,  -725,  -725,   858,   443,   861,   578,   862,
    -725,  -725,    42,    42,  -725,  -725,   -30,   185,   185,   515,
     785,  -725,   864,   865,   866,  -725,  -725,  -725,   867,   762,
     868,   344,   219,   525,   255,   869,  -725,   870,   256,   259,
      69,    69,    69,    69,  -725,  -725,   813,   871,   638,   872,
     874,  -725,  -725,  -725,   874,   874,  -725,   -19,   875,   874,
     565,   540,    60,    60,  -725,   766,   786,  -725,  -725,    89,
     253,  -725,  -725,   356,  -725,  -725,  -725,  -725,   877,  -725,
     878,  -725,  -725,  -725,  -725,  -725,  -725,  -725,    69,  -725,
      69,  -725,  -725,   879,   826,  -725,  -725,    42,    42,    42,
      42,    42,  -725,  -725,  -725,  -725,  -725,   880,   881,  -725,
    -725,   774,  -725,   882,   883,   800,   808,   886,  -725,   122,
     578,   578,   578,  -725,   887,   889,   889,  -725,   864,   578,
      42,    42,   890,   542,   891,  -725,  -725,  -725,  -725,   543,
      89,    89,    89,    89,    89,  -725,   836,    93,  -725,  -725,
      89,    89,  -725,   302,   893,   894,   895,   896,   897,   588,
    -725,   448,   898,  -725,  -725,  -725,  -725,   899,   599,   865,
     901,   173,   173,   565,   123,   841,   902,  -725,   524,  -725,
    -725,  -725,  -725,   903,   904,   905,   906,   907,   908,    89,
     817,   909,   910,   911,  -725,  -725,  -725,  -725,  -725,  -725,
    -725,    42,    42,    42,    42,    42,  -725,  -725,  -725,  -725,
     912,   348,  -725,   913,   619,   914,   915,   916,   590,   917,
     602,   565,  -725,  -725,  -725,  -725,  -725,  -725,   918,   919,
     920,   921,  -725,   609,   103,   103,   103,   103,   103,  -725,
     922,  -725,   578,    42,    42,   924,  -725,    93,  -725,  -725,
     865,   925,  -725,  -725,   926,  -725,  -725,  -725,  -725,   656,
    -725,  -725,  -725,  -725,   689,   -20,   927,  -725,  -725,  -725,
     928,   929,   930,   931,  -725,   614,   901,    60,    60,   565,
     197,   932,  -725,   877,   103,   103,   103,   103,  -725,  -725,
    -725,  -725,  -725,  -725,  -725,  -725,  -725,   933,   934,   935,
     936,  -725,   937,   103,   103,   103,   103,  -725,  -725,  -725,
     901,  -725,   938,   939,   940,   941,   942,  -725,  -725,  -725,
    -725,  -725
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
     349,     0,     0,   356,   355,     0,     2,     3,     4,   349,
     351,   353,     0,     0,    51,   349,   349,     1,     0,   346,
       0,     0,     0,     0,    62,     0,    51,   348,   347,     0,
     350,     0,     0,     6,     0,     0,     0,     0,     0,     0,
       0,    14,   252,   258,   257,   253,   254,   255,   256,     0,
       0,   354,    50,   352,     0,   312,     0,    20,     0,     0,
       0,     0,     0,     0,     0,   260,    53,     0,     0,    68,
       0,     5,    13,     7,    10,    11,    12,     0,   251,     9,
       8,    15,     0,    16,    20,     0,    17,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    63,     0,    49,    53,     0,     0,     0,    26,     0,
       0,     0,    56,     0,     0,     0,    19,    18,     0,     0,
      22,    21,   299,   285,   286,   300,   288,   287,   289,   290,
     291,   301,   292,   293,   297,   298,   296,   295,   294,   302,
     303,   304,   305,   306,   307,   308,    23,   248,     0,     0,
     238,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   247,   263,   284,     0,   267,     0,   265,     0,   271,
       0,   309,     0,    52,   244,     0,    29,    46,   243,    25,
     246,     0,   245,    33,   310,    65,     0,    67,    30,    31,
      46,     0,     0,   226,     0,   259,     0,     0,   213,     0,
       0,   179,   180,     0,     0,     0,     0,     0,     0,     0,
      46,    46,    46,    46,    46,     0,    66,    53,    53,    28,
       0,     0,    42,     0,    39,    46,    68,    68,     0,     0,
       0,   239,     0,     0,   192,   188,   189,   190,   191,    59,
       0,     0,     0,   261,     0,   216,   214,   212,   217,   218,
       0,   220,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    70,    48,    47,    60,    46,    27,     0,    36,
       0,    55,    54,   241,   237,     0,     0,     0,     0,     0,
       0,     0,   183,     0,   173,   175,    46,    46,   320,   219,
     221,   222,     0,     0,   225,     0,     0,     0,     0,   269,
       0,    45,    46,    46,    38,   228,   236,   229,     0,   227,
     240,     0,    57,    59,    59,     0,     0,     0,     0,     0,
     174,     0,     0,     0,     0,   320,   320,     0,   320,   320,
     320,   320,   320,   223,   224,     0,     0,     0,     0,     0,
       0,    64,    69,    41,    40,     0,    35,   230,    61,    58,
       0,     0,     0,     0,     0,     0,     0,   233,     0,     0,
       0,    75,    53,   323,     0,     0,     0,   313,   317,   311,
     314,   315,   316,   318,   319,   215,     0,     0,   193,   211,
       0,     0,    44,     0,    37,   182,   181,   168,     0,     0,
       0,     0,    59,     0,     0,     0,     0,     0,   202,     0,
       0,     0,     0,     0,   330,   331,     0,     0,   197,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   274,   170,
     169,   171,   172,     0,   231,   232,   250,     0,     0,     0,
     249,     0,   322,    74,    73,    72,   160,   321,     0,     0,
     326,     0,   335,   187,   334,   333,     0,   324,     0,     0,
     196,   235,   211,     0,    46,     0,     0,    81,    83,    82,
      85,    84,   158,     0,     0,     0,   268,     0,     0,     0,
     156,   153,   152,   151,     0,     0,   184,     0,     0,   208,
       0,   202,    46,     0,     0,    32,    75,    59,   328,     0,
     329,     0,     0,    59,    59,     0,     0,     0,     0,     0,
     325,     0,   209,   194,   234,     0,     0,     0,     0,     0,
     262,   137,     0,     0,   266,   264,    44,     0,     0,     0,
       0,   270,     0,     0,     0,   198,   200,   207,     0,     0,
       0,     0,     0,     0,     0,     0,   337,     0,     0,     0,
       0,     0,     0,     0,   332,   210,     0,     0,     0,     0,
      80,    87,    88,    89,    80,    80,    90,     0,     0,    80,
      46,     0,     0,     0,    43,     0,     0,   147,   155,     0,
       0,   273,   275,     0,   272,   101,   102,   199,     0,   159,
       0,    76,   242,   327,   336,   186,   185,   339,   342,   338,
     344,   340,   341,     0,     0,    96,    94,     0,     0,     0,
       0,     0,    95,    77,    79,    78,   157,     0,     0,   135,
     136,     0,   178,     0,     0,     0,     0,     0,   154,     0,
       0,     0,     0,   278,     0,     0,     0,   104,     0,     0,
       0,     0,     0,     0,     0,    71,   343,   345,   195,     0,
       0,     0,     0,     0,     0,   162,     0,     0,   133,   134,
       0,     0,   148,     0,     0,     0,     0,     0,     0,     0,
     109,     0,     0,   120,   123,   121,   122,     0,     0,     0,
       0,     0,     0,    46,     0,     0,     0,   201,     0,    86,
      91,    92,    93,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   283,   279,   280,   281,   276,   277,
     127,     0,     0,     0,     0,     0,   105,   106,    97,   103,
       0,     0,   116,     0,     0,     0,     0,     0,     0,     0,
       0,    46,   164,   165,   166,   167,   163,   161,     0,     0,
       0,     0,   282,     0,     0,     0,     0,     0,     0,    99,
       0,   118,     0,     0,     0,     0,   100,     0,   107,   108,
       0,     0,   203,   205,     0,   176,   177,   149,   150,     0,
     119,   126,   124,   125,     0,     0,     0,   138,   140,   141,
       0,     0,     0,     0,   112,     0,     0,     0,     0,    46,
       0,     0,   204,     0,     0,     0,     0,     0,   139,   142,
     129,   130,   131,   132,   128,   113,   117,     0,     0,     0,
       0,    98,     0,     0,     0,     0,     0,   115,   110,   111,
       0,   206,     0,     0,     0,     0,     0,   144,   143,   145,
     146,   114
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -725,  -725,  -725,   734,  -725,  -725,   892,  -725,   863,   248,
    -725,   798,  -725,  -725,   781,  -725,  -725,  -725,  -725,  -725,
    -179,   412,  -247,  -191,   923,   849,   395,  -254,  -725,  -725,
     195,  -725,   -74,  -725,  -178,  -725,  -725,   462,  -725,  -485,
    -410,  -725,  -725,  -725,  -725,  -601,  -725,  -725,  -725,  -724,
    -725,   325,  -725,  -725,   221,   391,  -725,  -246,  -725,   436,
     111,    -4,  -725,  -398,  -394,  -725,  -725,  -397,  -391,  -494,
    -185,  -516,  -152,  -411,  -725,  -725,  -353,  -351,   174,   281,
    -725,  -725,   -60,  -112,  -725,   736,   -88,  -725,  -402,  -725,
     475,  -725,   873,  -725,  -725,  -725,   943,  -725,  -725,  -725,
    -725,  -725,  -725,  -725,  -725,  -725,  -548,  -725,  -570,   308,
    -725,  -725,  -725,  -725,   480,  -725,  -725,  -725,  -725,  -725,
    -725,  -725,  -382,  -725,   371,   372,   257,  -725,   944,  -725,
     947
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     5,     6,    35,    23,    84,   319,    36,    85,    37,
      89,   107,   108,   177,   239,   487,   110,   183,   346,   225,
     221,   415,   222,   102,    25,   103,   111,   279,   266,   349,
     312,   104,   217,   187,   218,   112,   300,   399,   433,   549,
     456,   550,   551,   639,   457,   574,   668,   575,   712,   713,
     775,   662,   663,   733,   664,   464,   561,   766,   767,   418,
     474,   617,   519,   552,   553,   434,   435,   554,   555,   556,
     618,   613,   284,   285,   445,   240,   377,   479,   634,   480,
     378,   197,   335,   379,   453,   275,   509,   242,   193,   114,
     436,    38,    39,    40,   326,    41,    42,    43,    70,    44,
      45,    46,    47,   340,    48,   475,   571,   659,   572,   164,
     146,    49,    50,     7,   327,   328,   329,   366,   330,   331,
     332,   406,   588,   499,   589,   591,     8,     9,    10,    11,
      12
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      92,   203,   192,   160,   465,   207,   208,   209,   227,   397,
     462,   228,   462,   407,   458,   460,   458,   460,   459,   301,
     459,   461,    55,   461,   446,    31,   263,   264,   521,   576,
      93,   257,   258,   259,   260,   261,   196,   215,    95,   199,
     200,    97,   204,   205,   206,   531,   270,   614,    99,   655,
      13,   796,   797,   175,   188,   283,   255,   451,   191,   350,
     351,   788,   220,   611,    17,   603,   223,   709,   710,   604,
     605,   522,   441,   532,   607,   523,    91,   280,   387,    21,
     669,    32,   -46,    20,   693,     1,   816,   789,   189,   198,
     303,   250,   280,   252,   253,   254,   280,   256,   321,    22,
     504,   562,   563,   241,   462,   558,   764,   322,   323,    18,
       2,   699,   226,   540,   541,   542,   543,   274,   274,   -30,
     277,   278,   342,   343,   344,   653,    91,   403,    56,   527,
     352,   353,   354,   355,     2,   416,    94,   247,   423,   341,
     612,   216,   292,   293,    96,   682,    14,    98,   462,   781,
     282,   442,   462,   462,   100,   715,   716,   462,   587,   176,
     590,   592,   448,   306,   306,   308,   281,   388,   389,   390,
     391,   400,   224,   689,   576,   576,   714,   443,   201,   202,
     477,   281,   282,   201,   202,   281,   640,   641,   642,   643,
     644,     3,     4,   593,   302,   765,   310,   282,   201,   202,
     280,   282,   201,   202,   404,   405,   488,   -30,   590,    26,
      18,   282,   201,   202,   385,     3,     4,    15,    16,   671,
     672,    26,   -30,   462,   462,   380,   381,   665,   665,   654,
     718,   666,   666,   534,   386,    24,   688,   462,    14,   538,
     539,   680,   216,   511,   462,   681,   393,   394,   458,   460,
     476,    51,   459,   612,   444,   461,   576,    59,   471,   582,
     585,   798,   799,   586,    54,   281,    19,   512,   513,   416,
       3,     4,    27,    28,   472,   505,   473,   729,   489,   360,
      53,   282,   201,   202,   311,   181,   182,   483,   411,   281,
     734,   735,   736,   737,   738,    71,   484,   449,   452,    24,
     398,   619,   311,   528,    34,   282,   201,   202,   429,   620,
     621,   622,   623,    77,   361,   362,   363,    24,   364,    91,
     246,   365,   311,   769,   769,   769,   769,   769,   201,   202,
     485,   462,   777,   778,   486,   762,   501,    24,   311,   763,
     191,   305,   408,   444,   444,   444,   444,   311,   311,   740,
     619,   311,   109,    24,   -34,   -34,   185,   624,   620,   621,
     622,   186,    24,    24,   191,   307,    24,   478,   481,   527,
      33,    81,    34,   769,   769,   769,   769,   313,   314,    82,
     580,   608,   315,   316,   317,   318,   409,    83,   444,   444,
     444,   444,   769,   769,   769,   769,   559,   410,   230,   231,
     150,   232,   233,    90,   166,    91,   168,   741,   170,   113,
     150,   234,   625,   626,   742,   627,    86,   524,   150,   101,
     398,   628,   629,   105,   115,   106,   235,   236,   237,   238,
     117,   743,   744,   395,   594,   396,   444,   118,   444,   630,
     631,   599,   600,   601,   547,   189,   548,   -80,   401,   402,
      88,   313,   314,   -24,   -24,   683,   684,   685,   686,   687,
     431,   432,   690,   -24,   -24,   -24,   -24,   -24,   -24,   -24,
     -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,   -24,
     -24,   -24,   -24,   -24,   -24,   632,   683,   684,   120,   770,
     771,   772,   773,   147,   717,   376,   450,   376,   503,   161,
     506,   396,   526,   150,   728,   507,   508,   700,   656,   657,
     658,   162,   507,   165,   188,   163,   427,   670,   417,   567,
     167,   188,   169,    58,   171,   148,   172,   428,   431,   581,
     150,   701,   702,   703,   704,   705,   325,   174,   803,   804,
     805,   806,   754,   463,   609,   676,   678,   679,   189,    61,
      62,    63,    64,    65,   178,   189,   -24,   812,   813,   814,
     815,   106,   780,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   180,   325,   325,   184,   325,   325,   325,   325,
     325,   191,   768,   768,   768,   768,   768,    68,   507,   195,
     674,   570,   698,    91,   491,   390,   194,   188,   210,   148,
     800,   675,   573,   708,   150,   396,   753,   597,   598,   599,
     600,   601,   759,   760,   719,   481,   492,   711,   795,   493,
     494,   271,   272,   745,   495,   496,   497,   498,   565,   566,
     776,   189,   768,   768,   768,   768,   211,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   691,   692,   212,   213,
     214,   768,   768,   768,   768,   121,   122,   220,   751,   219,
     398,   243,   244,   245,   248,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   229,   230,   231,
     150,   232,   233,   262,   249,   251,   265,   596,   267,   268,
     150,   234,   507,   508,   269,   273,   286,   287,   320,   288,
     148,   188,   149,   336,   289,   150,   235,   236,   237,   238,
     507,   597,   598,   599,   600,   601,   313,   314,   290,   188,
     291,   315,   316,   747,   318,   189,   294,   295,   324,   701,
     702,   703,   704,   705,   296,   189,   297,   298,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   299,   145,   304,
      57,    58,   309,   189,    59,   468,   469,   333,   470,   334,
      72,    73,    74,    75,    76,   337,   338,   339,   345,   347,
     348,   -17,   471,    79,    80,   356,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,   313,   314,   472,   357,
     473,   784,   785,   786,   787,   367,   368,   358,   370,   371,
     372,   373,   374,   359,   369,   375,   376,   382,   412,   383,
     392,   384,   413,   414,   417,   438,   419,   420,   421,   422,
     424,   425,   426,   430,   437,   440,   447,   439,   454,   455,
     463,   516,   466,   467,   396,   482,   520,   490,   500,   502,
     510,   514,   515,   517,   518,   535,   525,   529,   615,   536,
     544,   545,   546,   537,   557,   560,   569,   570,   573,   485,
     577,   578,   579,   583,   584,   595,   602,   548,   616,   606,
     633,   650,   635,   638,   645,   646,   648,   649,   647,   651,
     652,   660,   661,   673,   190,   677,   653,   729,   694,   695,
     696,   697,   706,   707,   711,   721,   179,   722,   723,   724,
     725,   726,   727,   730,   731,   732,   739,   746,   748,   749,
     750,   752,   755,   756,   757,   758,   774,   779,   564,   782,
     783,   790,   791,   792,   793,   794,   801,   807,   808,   809,
     810,   811,   817,   818,   819,   820,   821,   116,   533,    52,
      87,   667,   610,   173,   761,   568,   720,   802,   530,   636,
     119,     0,   637,     0,     0,    30,   276,    29,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    78
};

static const yytype_int16 yycheck[] =
{
      60,   153,   114,    91,   414,   157,   158,   159,   186,   360,
     412,   190,   414,   366,   412,   412,   414,   414,   412,   266,
     414,   412,     1,   414,   406,    12,   217,   218,     4,   523,
       1,   210,   211,   212,   213,   214,   148,     3,     1,   151,
     152,     1,   154,   155,   156,     3,   225,   563,     1,   619,
      11,   775,   776,     1,    73,   240,   208,   410,     3,   313,
     314,    81,    92,     3,     0,   550,     1,   668,   669,   554,
     555,    47,     3,   484,   559,    51,     3,     3,     4,    93,
     628,    68,   112,     6,   654,     3,   810,   107,   107,   149,
     268,   203,     3,   205,   206,   207,     3,   209,   283,     3,
     453,   512,   513,   191,   506,   507,     3,   286,   287,     3,
      28,   659,   186,   495,   496,   497,   498,   229,   230,     4,
     232,   233,   300,   302,   303,     3,     3,     1,   107,   480,
     315,   316,   317,   318,    28,   382,   107,   197,   392,     4,
      80,   107,   254,   255,   107,   639,   107,   107,   550,   750,
     108,    82,   554,   555,   107,   671,   672,   559,   540,   107,
     542,   543,   107,   275,   276,   277,    92,   352,   353,   354,
     355,   362,   107,    80,   668,   669,     3,   108,   109,   110,
     107,    92,   108,   109,   110,    92,   597,   598,   599,   600,
     601,   109,   110,   546,   268,    92,     4,   108,   109,   110,
       3,   108,   109,   110,    78,    79,     4,    92,   590,    14,
       3,   108,   109,   110,     4,   109,   110,   109,   110,   630,
     631,    26,   107,   625,   626,   337,   338,   625,   626,   107,
     107,   625,   626,   487,     4,   107,   646,   639,   107,   493,
     494,   639,   107,    59,   646,   639,   358,   359,   646,   646,
       4,     4,   646,    80,   406,   646,   750,    13,    73,     4,
       4,   777,   778,     4,   107,    92,     9,    83,    84,   516,
     109,   110,    15,    16,    89,   454,    91,    80,    76,    35,
       7,   108,   109,   110,    92,     3,     4,    64,   376,    92,
     701,   702,   703,   704,   705,     4,    73,   409,   410,   107,
     360,    48,    92,   482,     3,   108,   109,   110,   396,    56,
      57,    58,    59,     3,    70,    71,    72,   107,    74,     3,
       4,    77,    92,   734,   735,   736,   737,   738,   109,   110,
     107,   733,   743,   744,   111,   733,   448,   107,    92,   733,
       3,     4,     4,   495,   496,   497,   498,    92,    92,     1,
      48,    92,     1,   107,     3,     4,   107,     1,    56,    57,
      58,   112,   107,   107,     3,     4,   107,   427,   428,   720,
       1,     4,     3,   784,   785,   786,   787,   107,   108,     4,
     532,   560,   112,   113,   114,   115,    48,   107,   540,   541,
     542,   543,   803,   804,   805,   806,   508,    59,    60,    61,
      62,    63,    64,     1,    96,     3,    98,    59,   100,     3,
      62,    73,    56,    57,    66,    59,   107,   477,    62,   107,
     480,    65,    66,     1,     3,     3,    88,    89,    90,    91,
       4,    83,    84,     1,   546,     3,   588,     3,   590,    83,
      84,    85,    86,    87,     1,   107,     3,     4,    75,    76,
       1,   107,   108,     4,     5,   640,   641,   642,   643,   644,
       3,     4,   647,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,   573,   671,   672,     4,   735,
     736,   737,   738,     4,   673,     3,     4,     3,     4,     4,
      59,     3,     4,    62,   689,    64,    65,    59,   620,   621,
     622,     4,    64,     4,    73,    45,    48,   629,     3,     4,
       4,    73,     4,    10,     4,    57,   112,    59,     3,     4,
      62,    83,    84,    85,    86,    87,   288,     4,   784,   785,
     786,   787,   721,     3,     4,   633,     3,     4,   107,    36,
      37,    38,    39,    40,     4,   107,   107,   803,   804,   805,
     806,     3,   747,    95,    96,    97,    98,    99,   100,   101,
     102,   103,     4,   325,   326,     4,   328,   329,   330,   331,
     332,     3,   734,   735,   736,   737,   738,    43,    64,     4,
      48,     3,     4,     3,    82,   780,    69,    73,     3,    57,
     779,    59,     3,     4,    62,     3,     4,    83,    84,    85,
      86,    87,     3,     4,   674,   675,   104,     3,     4,   107,
     108,   226,   227,   711,   112,   113,   114,   115,   517,   518,
     742,   107,   784,   785,   786,   787,     3,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   650,   651,     3,     3,
       3,   803,   804,   805,   806,     4,     5,    92,   718,     4,
     720,     4,   107,     4,     4,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    59,    60,    61,
      62,    63,    64,    67,     4,     4,   107,    59,     4,   112,
      62,    73,    64,    65,     4,     4,     3,     3,   107,     4,
      57,    73,    59,    46,     4,    62,    88,    89,    90,    91,
      64,    83,    84,    85,    86,    87,   107,   108,     4,    73,
       4,   112,   113,   114,   115,   107,     4,     4,     3,    83,
      84,    85,    86,    87,     4,   107,     4,     4,    95,    96,
      97,    98,    99,   100,   101,   102,   103,     4,   107,     4,
       9,    10,     4,   107,    13,    56,    57,     4,    59,     4,
      36,    37,    38,    39,    40,    46,    46,   106,   112,     4,
     107,     8,    73,    49,    50,     8,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,   107,   108,    89,     4,
      91,   112,   113,   114,   115,   325,   326,     4,   328,   329,
     330,   331,   332,     4,     4,     4,     3,     3,    51,    55,
     108,    94,    51,    51,     3,   109,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,   109,     3,     3,
       3,    94,     4,   112,     3,     3,    92,     4,     4,     4,
       4,     4,     4,     3,     3,   109,     4,     3,    92,     4,
       4,     4,     4,   107,     3,     3,    81,     3,     3,   107,
       4,     4,     4,     4,     4,     4,     4,     3,    92,     4,
       3,    81,     4,     4,     4,     4,     4,     4,   114,    81,
       4,     4,     3,     3,   113,     4,     3,    80,     4,     4,
       4,     4,     4,     4,     3,     3,   108,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     3,   516,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,    84,   486,    26,
      58,   626,   561,   104,   733,   519,   675,   783,   483,   588,
      87,    -1,   590,    -1,    -1,    21,   230,    20,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    41
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,    28,   109,   110,   118,   119,   230,   243,   244,
     245,   246,   247,    11,   107,   109,   110,     0,     3,   243,
       6,    93,     3,   121,   107,   141,   147,   243,   243,   247,
     245,    12,    68,     1,     3,   120,   124,   126,   208,   209,
     210,   212,   213,   214,   216,   217,   218,   219,   221,   228,
     229,     4,   141,     7,   107,     1,   107,     9,    10,    13,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
     215,     4,   120,   120,   120,   120,   120,     3,   213,   120,
     120,     4,     4,   107,   122,   125,   107,   123,     1,   127,
       1,     3,   199,     1,   107,     1,   107,     1,   107,     1,
     107,   107,   140,   142,   148,     1,     3,   128,   129,     1,
     133,   143,   152,     3,   206,     3,   125,     4,     3,   209,
       4,     4,     5,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,   107,   227,     4,    57,    59,
      62,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     203,     4,     4,    45,   226,     4,   226,     4,   226,     4,
     226,     4,   112,   142,     4,     1,   107,   130,     4,   128,
       4,     3,     4,   134,     4,   107,   112,   150,    73,   107,
     131,     3,   200,   205,    69,     4,   200,   198,   199,   200,
     200,   109,   110,   189,   200,   200,   200,   189,   189,   189,
       3,     3,     3,     3,     3,     3,   107,   149,   151,     4,
      92,   137,   139,     1,   107,   136,   149,   151,   137,    59,
      60,    61,    63,    64,    73,    88,    89,    90,    91,   131,
     192,   203,   204,     4,   107,     4,     4,   199,     4,     4,
     200,     4,   200,   200,   200,   189,   200,   137,   137,   137,
     137,   137,    67,   140,   140,   107,   145,     4,   112,     4,
     137,   143,   143,     4,   200,   202,   202,   200,   200,   144,
       3,    92,   108,   187,   189,   190,     3,     3,     4,     4,
       4,     4,   200,   200,     4,     4,     4,     4,     4,     4,
     153,   139,   149,   151,     4,     4,   200,     4,   200,     4,
       4,    92,   147,   107,   108,   112,   113,   114,   115,   123,
     107,   187,   137,   137,     3,   126,   211,   231,   232,   233,
     235,   236,   237,     4,     4,   199,    46,    46,    46,   106,
     220,     4,   151,   137,   137,   112,   135,     4,   107,   146,
     144,   144,   187,   187,   187,   187,     8,     4,     4,     4,
      35,    70,    71,    72,    74,    77,   234,   231,   231,     4,
     231,   231,   231,   231,   231,     4,     3,   193,   197,   200,
     200,   200,     3,    55,    94,     4,     4,     4,   187,   187,
     187,   187,   108,   200,   200,     1,     3,   194,   199,   154,
     140,    75,    76,     1,    78,    79,   238,   193,     4,    48,
      59,   203,    51,    51,    51,   138,   139,     3,   176,     4,
       4,     4,     4,   144,     4,     4,     4,    48,    59,   203,
       4,     3,     4,   155,   182,   183,   207,     4,   109,   109,
       4,     3,    82,   108,   189,   191,   239,     4,   107,   200,
       4,   193,   200,   201,     3,     3,   157,   161,   180,   181,
     184,   185,   205,     3,   172,   157,     4,   112,    56,    57,
      59,    73,    89,    91,   177,   222,     4,   107,   199,   194,
     196,   199,     3,    64,    73,   107,   111,   132,     4,    76,
       4,    82,   104,   107,   108,   112,   113,   114,   115,   240,
       4,   200,     4,     4,   193,   137,    59,    64,    65,   203,
       4,    59,    83,    84,     4,     4,    94,     3,     3,   179,
      92,     4,    47,    51,   199,     4,     4,   194,   137,     3,
     207,     3,   190,   154,   144,   109,     4,   107,   144,   144,
     239,   239,   239,   239,     4,     4,     4,     1,     3,   156,
     158,   159,   180,   181,   184,   185,   186,     3,   205,   200,
       3,   173,   190,   190,   138,   177,   177,     4,   176,    81,
       3,   223,   225,     3,   162,   164,   186,     4,     4,     4,
     189,     4,     4,     4,     4,     4,     4,   239,   239,   241,
     239,   242,   239,   193,   200,     4,    59,    83,    84,    85,
      86,    87,     4,   156,   156,   156,     4,   156,   137,     4,
     172,     3,    80,   188,   188,    92,    92,   178,   187,    48,
      56,    57,    58,    59,     1,    56,    57,    59,    65,    66,
      83,    84,   203,     3,   195,     4,   241,   242,     4,   160,
     190,   190,   190,   190,   190,     4,     4,   114,     4,     4,
      81,    81,     4,     3,   107,   225,   200,   200,   200,   224,
       4,     3,   168,   169,   171,   180,   181,   168,   163,   223,
     200,   190,   190,     3,    48,    59,   203,     4,     3,     4,
     180,   181,   186,   187,   187,   187,   187,   187,   157,    80,
     187,   178,   178,   225,     4,     4,     4,     4,     4,   223,
      59,    83,    84,    85,    86,    87,     4,     4,     4,   162,
     162,     3,   165,   166,     3,   188,   188,   137,   107,   199,
     196,     3,     4,     4,     4,     4,     4,     4,   187,    80,
       4,     4,     4,   170,   190,   190,   190,   190,   190,     4,
       1,    59,    66,    83,    84,   203,     4,   114,     4,     4,
       4,   199,     4,     4,   137,     4,     4,     4,     4,     3,
       4,   171,   180,   181,     3,    92,   174,   175,   189,   190,
     174,   174,   174,   174,     4,   167,   200,   190,   190,     3,
     187,   162,     4,     4,   112,   113,   114,   115,    81,   107,
       4,     4,     4,     4,     4,     4,   166,   166,   188,   188,
     137,     4,   195,   174,   174,   174,   174,     4,     4,     4,
       4,     4,   174,   174,   174,   174,   166,     4,     4,     4,
       4,     4
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   117,   118,   118,   118,   119,   119,   120,   120,   120,
     120,   120,   120,   120,   120,   121,   122,   123,   124,   125,
     125,   126,   126,   127,   127,   128,   128,   129,   129,   130,
     131,   131,   132,   133,   133,   134,   134,   135,   135,   136,
     137,   137,   137,   138,   138,   139,   139,   140,   140,   140,
     141,   141,   142,   142,   143,   143,   143,   144,   144,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   152,   153,
     153,   154,   154,   154,   154,   154,   155,   156,   156,   156,
     156,   157,   157,   157,   157,   157,   158,   158,   159,   159,
     159,   160,   160,   160,   160,   161,   161,   162,   162,   162,
     162,   162,   162,   163,   163,   164,   164,   164,   164,   164,
     165,   165,   165,   166,   166,   166,   166,   167,   167,   168,
     168,   169,   169,   169,   170,   170,   170,   170,   171,   171,
     171,   171,   171,   172,   172,   172,   173,   173,   174,   174,
     174,   174,   174,   175,   175,   175,   175,   176,   176,   176,
     176,   177,   177,   177,   178,   179,   179,   180,   181,   182,
     183,   184,   185,   186,   186,   186,   186,   186,   187,   187,
     187,   187,   187,   187,   187,   187,   188,   188,   188,   189,
     189,   190,   190,   190,   190,   191,   191,   191,   192,   192,
     192,   192,   192,   193,   193,   193,   193,   193,   194,   194,
     194,   194,   194,   195,   195,   195,   195,   196,   196,   197,
     197,   197,   198,   198,   199,   199,   199,   199,   199,   199,
     199,   199,   199,   199,   199,   199,   200,   200,   200,   200,
     200,   200,   200,   200,   201,   201,   202,   202,   203,   204,
     205,   206,   207,   208,   208,   209,   209,   210,   210,   211,
     211,   212,   212,   213,   213,   213,   213,   213,   213,   214,
     215,   216,   217,   217,   218,   218,   219,   219,   220,   220,
     221,   221,   222,   222,   222,   223,   223,   224,   224,   225,
     225,   225,   225,   225,   226,   227,   227,   227,   227,   227,
     227,   227,   227,   227,   227,   227,   227,   227,   227,   227,
     227,   227,   227,   227,   227,   227,   227,   227,   227,   228,
     229,   230,   230,   231,   231,   231,   231,   231,   231,   231,
     231,   232,   233,   234,   235,   236,   236,   237,   237,   237,
     238,   238,   239,   239,   239,   239,   239,   239,   240,   240,
     240,   240,   241,   241,   242,   242,   243,   243,   243,   243,
     244,   244,   245,   245,   246,   247,   247
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     5,     4,     2,     2,     2,
       2,     2,     2,     2,     1,     4,     1,     1,     4,     2,
       0,     4,     4,     2,     0,     2,     1,     4,     3,     1,
       1,     1,     1,     2,     0,     5,     3,     2,     0,     1,
       4,     4,     1,     4,     0,     3,     0,     4,     4,     1,
       2,     0,     2,     0,     4,     4,     1,     2,     3,     0,
       1,     1,     1,     1,     4,     1,     1,     2,     0,     2,
       0,     6,     2,     2,     2,     0,     4,     2,     2,     2,
       0,     1,     1,     1,     1,     1,     4,     1,     1,     1,
       1,     2,     2,     2,     0,     4,     4,     4,     7,     5,
       5,     1,     1,     2,     0,     4,     4,     5,     5,     3,
       5,     5,     3,     4,     7,     5,     1,     2,     0,     4,
       1,     1,     1,     1,     2,     2,     2,     0,     5,     5,
       5,     5,     5,     5,     5,     4,     2,     0,     1,     2,
       1,     1,     2,     5,     5,     5,     5,     4,     6,     9,
       9,     1,     1,     1,     1,     2,     0,     4,     1,     4,
       1,     7,     5,     5,     5,     5,     5,     5,     4,     5,
       5,     5,     5,     1,     2,     1,     5,     5,     1,     1,
       1,     4,     4,     1,     6,     4,     4,     1,     1,     1,
       1,     1,     1,     1,     4,     7,     3,     2,     4,     5,
       4,     7,     1,     4,     5,     4,     7,     2,     1,     4,
       5,     1,     2,     1,     4,     7,     4,     4,     4,     5,
       4,     5,     5,     6,     6,     5,     1,     4,     4,     4,
       5,     7,     7,     5,     2,     1,     2,     1,     1,     1,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     2,     1,     1,     1,     1,     1,     1,     1,     5,
       1,     5,    12,     4,    12,     4,    12,     4,     4,     0,
      12,     4,     3,     3,     0,     1,     4,     2,     0,     4,
       4,     4,     5,     4,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     4,
       4,    12,     5,     2,     2,     2,     2,     2,     2,     2,
       0,     4,     4,     1,     4,     5,     4,     7,     5,     5,
       1,     1,     3,     1,     1,     1,     4,     3,     3,     3,
       3,     3,     1,     2,     1,     2,     2,     3,     3,     0,
       3,     1,     4,     1,     4,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 260 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {top_thing= (yyvsp[0].t_domain); current_analysis->the_domain= (yyvsp[0].t_domain);}
#line 2093 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 3:
#line 261 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {top_thing= (yyvsp[0].t_problem); current_analysis->the_problem= (yyvsp[0].t_problem);}
#line 2099 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 4:
#line 262 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {top_thing= (yyvsp[0].t_plan); }
#line 2105 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 5:
#line 267 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[-1].t_domain); (yyval.t_domain)->name= (yyvsp[-2].cp);delete [] (yyvsp[-2].cp);
	if (types_used && !types_defined) {
		yyerrok; log_error(E_FATAL,"Syntax error in domain - no :types section, but types used in definitions.");
	}
	}
#line 2115 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 6:
#line 273 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_domain)=static_cast<domain*>(NULL);
       	log_error(E_FATAL,"Syntax error in domain"); }
#line 2122 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 7:
#line 279 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); (yyval.t_domain)->req= (yyvsp[-1].t_pddl_req_flag);}
#line 2128 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 8:
#line 280 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {types_defined = true; (yyval.t_domain)= (yyvsp[0].t_domain); (yyval.t_domain)->types= (yyvsp[-1].t_type_list);}
#line 2134 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 9:
#line 281 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain); (yyval.t_domain)->constants= (yyvsp[-1].t_const_symbol_list);}
#line 2140 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 10:
#line 282 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain);
                                       (yyval.t_domain)->predicates= (yyvsp[-1].t_pred_decl_list); }
#line 2147 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 11:
#line 284 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain);
                                       (yyval.t_domain)->functions= (yyvsp[-1].t_func_decl_list); }
#line 2154 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 12:
#line 286 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= (yyvsp[0].t_domain);
   				       (yyval.t_domain)->constraints = (yyvsp[-1].t_con_goal);}
#line 2161 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 13:
#line 288 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain) = (yyvsp[0].t_domain);}
#line 2167 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 14:
#line 289 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_domain)= new domain((yyvsp[0].t_structure_store)); }
#line 2173 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 15:
#line 292 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.cp)=(yyvsp[-1].cp);}
#line 2179 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 16:
#line 296 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_class)=current_analysis->classes_tab.new_symbol_put((yyvsp[0].cp));
       delete [] (yyvsp[0].cp); }
#line 2186 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 17:
#line 299 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_class) = current_analysis->classes_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2192 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 18:
#line 301 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy) = 0;}
#line 2198 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 19:
#line 303 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy) = 0;}
#line 2204 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 20:
#line 304 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy) = 0;}
#line 2210 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 21:
#line 309 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
	// Stash in analysis object --- we need to refer to it during parse
	//   but domain object is not created yet,
	current_analysis->req |= (yyvsp[-1].t_pddl_req_flag);
	(yyval.t_pddl_req_flag)=(yyvsp[-1].t_pddl_req_flag);
    }
#line 2221 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 22:
#line 316 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
       log_error(E_FATAL,"Syntax error in requirements declaration.");
       (yyval.t_pddl_req_flag)= 0; }
#line 2229 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 23:
#line 322 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pddl_req_flag)= (yyvsp[-1].t_pddl_req_flag) | (yyvsp[0].t_pddl_req_flag); }
#line 2235 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 24:
#line 323 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pddl_req_flag)= 0; }
#line 2241 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 25:
#line 329 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pred_decl_list)=(yyvsp[0].t_pred_decl_list); (yyval.t_pred_decl_list)->push_front((yyvsp[-1].t_pred_decl));}
#line 2247 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 26:
#line 331 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {  (yyval.t_pred_decl_list)=new pred_decl_list;
           (yyval.t_pred_decl_list)->push_front((yyvsp[0].t_pred_decl)); }
#line 2254 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 27:
#line 336 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pred_decl)= new pred_decl((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_var_symbol_list),current_analysis->var_tab_stack.pop());}
#line 2260 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 28:
#line 338 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
        // hope someone makes this error someday
        log_error(E_FATAL,"Syntax error in predicate declaration.");
	(yyval.t_pred_decl)= static_cast<pred_decl*>(NULL); }
#line 2269 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 29:
#line 346 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.new_symbol_put((yyvsp[0].cp));
           current_analysis->var_tab_stack.push(
           				current_analysis->buildPredTab());
           delete [] (yyvsp[0].cp); }
#line 2278 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 30:
#line 353 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_ref("=");
	      requires(E_EQUALITY); }
#line 2285 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 31:
#line 355 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2291 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 32:
#line 363 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_pred_symbol)=current_analysis->pred_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2297 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 33:
#line 369 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_func_decl_list)=(yyvsp[-1].t_func_decl_list); (yyval.t_func_decl_list)->push_back((yyvsp[0].t_func_decl));}
#line 2303 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 34:
#line 370 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_decl_list)=new func_decl_list; }
#line 2309 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 35:
#line 375 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_func_decl)= new func_decl((yyvsp[-3].t_func_symbol),(yyvsp[-2].t_var_symbol_list),current_analysis->var_tab_stack.pop());}
#line 2315 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 36:
#line 377 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
	 log_error(E_FATAL,"Syntax error in functor declaration.");
	 (yyval.t_func_decl)= (int) NULL; }
#line 2323 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 37:
#line 383 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy) = (int) NULL;}
#line 2329 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 38:
#line 383 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy)= (int) NULL;}
#line 2335 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 39:
#line 387 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_symbol)=current_analysis->func_tab.new_symbol_put((yyvsp[0].cp));
           current_analysis->var_tab_stack.push(
           		current_analysis->buildFuncTab());
           delete [] (yyvsp[0].cp); }
#line 2344 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 40:
#line 400 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
      (yyval.t_var_symbol_list)= (yyvsp[-3].t_var_symbol_list);
      (yyval.t_var_symbol_list)->set_types((yyvsp[-1].t_type));           /* Set types for variables */
      (yyval.t_var_symbol_list)->splice((yyval.t_var_symbol_list)->end(),*(yyvsp[0].t_var_symbol_list));   /* Join lists */
      delete (yyvsp[0].t_var_symbol_list);                   /* Delete (now empty) list */
      requires(E_TYPING);
      types_used = true;
   }
#line 2357 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 41:
#line 409 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
      (yyval.t_var_symbol_list)= (yyvsp[-3].t_var_symbol_list);
      (yyval.t_var_symbol_list)->set_either_types((yyvsp[-1].t_type_list));    /* Set types for variables */
      (yyval.t_var_symbol_list)->splice((yyval.t_var_symbol_list)->end(),*(yyvsp[0].t_var_symbol_list));   /* Join lists */
      delete (yyvsp[0].t_var_symbol_list);                   /* Delete (now empty) list */
      requires(E_TYPING);
      types_used = true;
   }
#line 2370 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 42:
#line 418 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
       (yyval.t_var_symbol_list)= (yyvsp[0].t_var_symbol_list);
   }
#line 2378 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 43:
#line 425 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
      (yyval.t_var_symbol_list)= (yyvsp[-3].t_var_symbol_list);
      (yyval.t_var_symbol_list)->set_types(0);           /* Set types for variables */
      (yyval.t_var_symbol_list)->splice((yyval.t_var_symbol_list)->end(),*(yyvsp[0].t_var_symbol_list));   /* Join lists */
      delete (yyvsp[0].t_var_symbol_list);                   /* Delete (now empty) list */
   }
#line 2389 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 44:
#line 430 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list) = new var_symbol_list;}
#line 2395 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 45:
#line 438 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list)=(yyvsp[0].t_var_symbol_list); (yyvsp[0].t_var_symbol_list)->push_front((yyvsp[-1].t_var_symbol)); }
#line 2401 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 46:
#line 439 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list)= new var_symbol_list; }
#line 2407 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 47:
#line 446 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
      (yyval.t_const_symbol_list)= (yyvsp[-3].t_const_symbol_list);
      (yyvsp[-3].t_const_symbol_list)->set_types((yyvsp[-1].t_type));           /* Set types for constants */
      (yyvsp[-3].t_const_symbol_list)->splice((yyvsp[-3].t_const_symbol_list)->end(),*(yyvsp[0].t_const_symbol_list)); /* Join lists */
      delete (yyvsp[0].t_const_symbol_list);                   /* Delete (now empty) list */
      requires(E_TYPING);
      types_used = true;
   }
#line 2420 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 48:
#line 455 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
      (yyval.t_const_symbol_list)= (yyvsp[-3].t_const_symbol_list);
      (yyvsp[-3].t_const_symbol_list)->set_either_types((yyvsp[-1].t_type_list));
      (yyvsp[-3].t_const_symbol_list)->splice((yyvsp[-3].t_const_symbol_list)->end(),*(yyvsp[0].t_const_symbol_list));
      delete (yyvsp[0].t_const_symbol_list);
      requires(E_TYPING);
      types_used = true;
   }
#line 2433 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 49:
#line 464 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)= (yyvsp[0].t_const_symbol_list);}
#line 2439 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 50:
#line 469 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[0].t_const_symbol_list); (yyvsp[0].t_const_symbol_list)->push_front((yyvsp[-1].t_const_symbol));}
#line 2445 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 51:
#line 470 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=new const_symbol_list;}
#line 2451 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 52:
#line 474 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[0].t_const_symbol_list); (yyvsp[0].t_const_symbol_list)->push_front((yyvsp[-1].t_const_symbol));}
#line 2457 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 53:
#line 475 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=new const_symbol_list;}
#line 2463 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 54:
#line 484 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
       (yyval.t_type_list)= (yyvsp[-3].t_type_list);
       (yyval.t_type_list)->set_types((yyvsp[-1].t_type));           /* Set types for constants */
       (yyval.t_type_list)->splice((yyval.t_type_list)->end(),*(yyvsp[0].t_type_list)); /* Join lists */
       delete (yyvsp[0].t_type_list);                   /* Delete (now empty) list */
   }
#line 2474 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 55:
#line 491 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {
   // This parse needs to be excluded, we think (DPL&MF: 6/9/01)
       (yyval.t_type_list)= (yyvsp[-3].t_type_list);
       (yyval.t_type_list)->set_either_types((yyvsp[-1].t_type_list));
       (yyval.t_type_list)->splice((yyvsp[-3].t_type_list)->end(),*(yyvsp[0].t_type_list));
       delete (yyvsp[0].t_type_list);
   }
#line 2486 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 56:
#line 500 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type_list)= (yyvsp[0].t_type_list); }
#line 2492 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 57:
#line 506 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_parameter_symbol_list)=(yyvsp[-1].t_parameter_symbol_list); (yyval.t_parameter_symbol_list)->push_back((yyvsp[0].t_const_symbol)); }
#line 2498 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 58:
#line 508 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_parameter_symbol_list)=(yyvsp[-2].t_parameter_symbol_list); (yyval.t_parameter_symbol_list)->push_back((yyvsp[0].t_var_symbol)); }
#line 2504 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 59:
#line 509 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_parameter_symbol_list)= new parameter_symbol_list;}
#line 2510 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 60:
#line 516 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_var_symbol)= current_analysis->var_tab_stack.top()->symbol_put((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2516 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 61:
#line 522 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_var_symbol)= current_analysis->var_tab_stack.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2522 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 62:
#line 526 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_const_symbol)= current_analysis->const_tab.symbol_get((yyvsp[0].cp)); delete [] (yyvsp[0].cp); }
#line 2528 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 63:
#line 530 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_const_symbol)= current_analysis->const_tab.new_symbol_put((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2534 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 64:
#line 535 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type_list)= (yyvsp[-1].t_type_list); }
#line 2540 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 65:
#line 540 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type)= current_analysis->pddl_type_tab.symbol_ref((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2546 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 66:
#line 547 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_type)= current_analysis->pddl_type_tab.symbol_ref((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 2552 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 67:
#line 552 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= (yyvsp[-1].t_type_list); (yyval.t_type_list)->push_back((yyvsp[0].t_type));}
#line 2558 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 68:
#line 553 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= new pddl_type_list;}
#line 2564 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 69:
#line 558 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= (yyvsp[-1].t_type_list); (yyval.t_type_list)->push_back((yyvsp[0].t_type));}
#line 2570 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 70:
#line 559 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)= new pddl_type_list;}
#line 2576 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 71:
#line 564 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-5].t_effect_lists);
	  (yyval.t_effect_lists)->assign_effects.push_back(new assignment((yyvsp[-2].t_func_term),E_ASSIGN,(yyvsp[-1].t_num_expression)));
          if((yyvsp[-2].t_func_term)->getFunction()->getName()=="total-cost")
          {
          	requires(E_ACTIONCOSTS);
          	// Should also check that $5 is 0...
		  }
          else
          {
          	requires(E_NFLUENTS);
          }
	}
#line 2593 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 72:
#line 577 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->add_effects.push_back((yyvsp[0].t_simple_effect)); }
#line 2599 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 73:
#line 579 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->del_effects.push_back((yyvsp[0].t_simple_effect)); }
#line 2605 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 74:
#line 581 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->timed_effects.push_back((yyvsp[0].t_timed_effect)); }
#line 2611 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 75:
#line 583 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;}
#line 2617 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 76:
#line 588 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { requires(E_TIMED_INITIAL_LITERALS);
   		(yyval.t_timed_effect)=new timed_initial_literal((yyvsp[-1].t_effect_lists),(yyvsp[-2].fval));}
#line 2624 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 77:
#line 593 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[0].t_effect_lists); (yyval.t_effect_lists)->append_effects((yyvsp[-1].t_effect_lists)); delete (yyvsp[-1].t_effect_lists);}
#line 2630 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 78:
#line 594 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[0].t_effect_lists); (yyval.t_effect_lists)->cond_effects.push_front((yyvsp[-1].t_cond_effect));
                                      requires(E_COND_EFFS);}
#line 2637 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 79:
#line 596 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[0].t_effect_lists); (yyval.t_effect_lists)->forall_effects.push_front((yyvsp[-1].t_forall_effect));
                                      requires(E_COND_EFFS);}
#line 2644 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 80:
#line 598 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists(); }
#line 2650 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 81:
#line 607 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[0].t_effect_lists);}
#line 2656 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 82:
#line 608 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->add_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2662 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 83:
#line 609 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->del_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2668 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 84:
#line 610 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->cond_effects.push_front((yyvsp[0].t_cond_effect));}
#line 2674 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 85:
#line 611 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->forall_effects.push_front((yyvsp[0].t_forall_effect));}
#line 2680 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 86:
#line 615 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists);}
#line 2686 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 87:
#line 616 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[0].t_effect_lists);}
#line 2692 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 88:
#line 621 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->del_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2698 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 89:
#line 623 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->add_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2704 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 90:
#line 625 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->assign_effects.push_front((yyvsp[0].t_assignment));
         requires(E_NFLUENTS);}
#line 2711 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 91:
#line 631 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->del_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2717 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 92:
#line 632 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->add_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2723 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 93:
#line 633 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->assign_effects.push_back((yyvsp[0].t_assignment));
                                     requires(E_NFLUENTS); }
#line 2730 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 94:
#line 635 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2736 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 95:
#line 640 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); }
#line 2742 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 96:
#line 642 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_effect_lists)=NULL;
	 log_error(E_FATAL,"Syntax error in (and ...)");
	}
#line 2750 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 97:
#line 650 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); }
#line 2756 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 98:
#line 655 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
          (yyval.t_effect_lists)->forall_effects.push_back(
	       new forall_effect((yyvsp[-1].t_effect_lists), (yyvsp[-3].t_var_symbol_list), current_analysis->var_tab_stack.pop()));
          requires(E_COND_EFFS);}
#line 2765 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 99:
#line 660 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
	  (yyval.t_effect_lists)->cond_effects.push_back(
	       new cond_effect((yyvsp[-2].t_goal),(yyvsp[-1].t_effect_lists)));
          requires(E_COND_EFFS); }
#line 2774 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 100:
#line 665 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
	  (yyval.t_effect_lists)->cond_assign_effects.push_back(
	       new cond_effect((yyvsp[-2].t_goal),(yyvsp[-1].t_effect_lists)));
          requires(E_COND_EFFS); }
#line 2783 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 101:
#line 670 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=new effect_lists;
          (yyval.t_effect_lists)->timed_effects.push_back((yyvsp[0].t_timed_effect)); }
#line 2790 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 102:
#line 673 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
	  (yyval.t_effect_lists)->assign_effects.push_front((yyvsp[0].t_assignment));
          requires(E_NFLUENTS); }
#line 2798 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 103:
#line 679 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyvsp[-1].t_effect_lists)->append_effects((yyvsp[0].t_effect_lists)); delete (yyvsp[0].t_effect_lists); }
#line 2804 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 104:
#line 680 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2810 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 105:
#line 685 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect((yyvsp[-1].t_effect_lists),E_AT_START);}
#line 2816 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 106:
#line 687 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect((yyvsp[-1].t_effect_lists),E_AT_END);}
#line 2822 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 107:
#line 689 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_timed_effect)->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression))); }
#line 2830 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 108:
#line 693 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_timed_effect)->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression))); }
#line 2838 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 109:
#line 697 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_timed_effect)=NULL;
	log_error(E_FATAL,"Syntax error in timed effect"); }
#line 2845 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 110:
#line 703 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_timed_effect)->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression))); }
#line 2853 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 111:
#line 707 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_timed_effect)=new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_timed_effect)->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression))); }
#line 2861 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 112:
#line 711 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_timed_effect)=NULL;
	log_error(E_FATAL,"Syntax error in conditional continuous effect"); }
#line 2868 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 113:
#line 717 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); }
#line 2874 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 114:
#line 722 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
          (yyval.t_effect_lists)->forall_effects.push_back(
	       new forall_effect((yyvsp[-1].t_effect_lists), (yyvsp[-3].t_var_symbol_list), current_analysis->var_tab_stack.pop()));
          requires(E_COND_EFFS);}
#line 2883 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 115:
#line 727 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists;
	  (yyval.t_effect_lists)->cond_assign_effects.push_back(
	       new cond_effect((yyvsp[-2].t_goal),(yyvsp[-1].t_effect_lists)));
          requires(E_COND_EFFS); }
#line 2892 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 116:
#line 732 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=new effect_lists;
          (yyval.t_effect_lists)->timed_effects.push_back((yyvsp[0].t_timed_effect)); }
#line 2899 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 117:
#line 737 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyvsp[-1].t_effect_lists)->append_effects((yyvsp[0].t_effect_lists)); delete (yyvsp[0].t_effect_lists); }
#line 2905 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 118:
#line 738 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2911 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 119:
#line 742 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists);}
#line 2917 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 120:
#line 743 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[0].t_effect_lists);}
#line 2923 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 121:
#line 748 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->del_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2929 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 122:
#line 750 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->add_effects.push_front((yyvsp[0].t_simple_effect));}
#line 2935 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 123:
#line 752 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists; (yyval.t_effect_lists)->assign_effects.push_front((yyvsp[0].t_assignment));
         requires(E_NFLUENTS);}
#line 2942 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 124:
#line 758 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->del_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2948 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 125:
#line 759 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->add_effects.push_back((yyvsp[0].t_simple_effect));}
#line 2954 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 126:
#line 760 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)= (yyvsp[-1].t_effect_lists); (yyval.t_effect_lists)->assign_effects.push_back((yyvsp[0].t_assignment));
                                     requires(E_NFLUENTS); }
#line 2961 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 127:
#line 762 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 2967 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 128:
#line 768 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_ASSIGN,(yyvsp[-1].t_expression)); }
#line 2973 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 129:
#line 770 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression)); }
#line 2979 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 130:
#line 772 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression)); }
#line 2985 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 131:
#line 774 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_UP,(yyvsp[-1].t_expression)); }
#line 2991 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 132:
#line 776 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_DOWN,(yyvsp[-1].t_expression)); }
#line 2997 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 133:
#line 781 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists;
         timed_effect * te = new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_effect_lists)->timed_effects.push_front(te);
         te->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression))); }
#line 3007 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 134:
#line 787 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=new effect_lists;
         timed_effect * te = new timed_effect(new effect_lists,E_CONTINUOUS);
         (yyval.t_effect_lists)->timed_effects.push_front(te);
         te->effs->assign_effects.push_front(
	     new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression))); }
#line 3017 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 135:
#line 793 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists) = (yyvsp[-1].t_effect_lists);}
#line 3023 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 136:
#line 797 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists); (yyvsp[-1].t_effect_lists)->append_effects((yyvsp[0].t_effect_lists)); delete (yyvsp[0].t_effect_lists); }
#line 3029 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 137:
#line 798 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_effect_lists)= new effect_lists; }
#line 3035 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 138:
#line 802 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_expression);}
#line 3041 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 139:
#line 803 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= new special_val_expr(E_DURATION_VAR);
                    requires( E_DURATION_INEQUALITIES );}
#line 3048 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 140:
#line 805 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)=(yyvsp[0].t_num_expression); }
#line 3054 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 141:
#line 806 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= (yyvsp[0].t_func_term); }
#line 3060 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 142:
#line 807 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression) = current_analysis->getControlParam((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 3066 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 143:
#line 812 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new plus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 3072 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 144:
#line 814 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new minus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 3078 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 145:
#line 816 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 3084 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 146:
#line 818 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new div_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); }
#line 3090 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 147:
#line 823 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)= new conj_goal((yyvsp[-1].t_goal_list)); }
#line 3096 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 148:
#line 825 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)= new timed_goal(new comparison((yyvsp[-4].t_comparison_op),
        			new special_val_expr(E_DURATION_VAR),(yyvsp[-1].t_expression)),E_AT_START); }
#line 3103 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 149:
#line 828 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal) = new timed_goal(new comparison((yyvsp[-5].t_comparison_op),
					new special_val_expr(E_DURATION_VAR),(yyvsp[-2].t_expression)),E_AT_START);}
#line 3110 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 150:
#line 831 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal) = new timed_goal(new comparison((yyvsp[-5].t_comparison_op),
					new special_val_expr(E_DURATION_VAR),(yyvsp[-2].t_expression)),E_AT_END);}
#line 3117 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 151:
#line 836 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_comparison_op)= E_LESSEQ; requires(E_DURATION_INEQUALITIES);}
#line 3123 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 152:
#line 837 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_comparison_op)= E_GREATEQ; requires(E_DURATION_INEQUALITIES);}
#line 3129 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 153:
#line 838 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_comparison_op)= E_EQUALS; }
#line 3135 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 154:
#line 846 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_expression); }
#line 3141 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 155:
#line 851 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyval.t_goal_list)->push_back((yyvsp[0].t_goal)); }
#line 3147 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 156:
#line 853 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)= new goal_list; }
#line 3153 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 157:
#line 858 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[-1].t_proposition)); }
#line 3159 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 158:
#line 863 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[0].t_proposition)); }
#line 3165 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 159:
#line 870 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[-1].t_proposition)); }
#line 3171 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 160:
#line 875 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_simple_effect)= new simple_effect((yyvsp[0].t_proposition)); }
#line 3177 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 161:
#line 880 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_forall_effect)= new forall_effect((yyvsp[-1].t_effect_lists), (yyvsp[-3].t_var_symbol_list), current_analysis->var_tab_stack.pop());}
#line 3183 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 162:
#line 885 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_cond_effect)= new cond_effect((yyvsp[-2].t_goal),(yyvsp[-1].t_effect_lists)); }
#line 3189 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 163:
#line 890 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_ASSIGN,(yyvsp[-1].t_expression)); }
#line 3195 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 164:
#line 892 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_INCREASE,(yyvsp[-1].t_expression)); }
#line 3201 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 165:
#line 894 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_DECREASE,(yyvsp[-1].t_expression)); }
#line 3207 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 166:
#line 896 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_UP,(yyvsp[-1].t_expression)); }
#line 3213 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 167:
#line 898 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_assignment)= new assignment((yyvsp[-2].t_func_term),E_SCALE_DOWN,(yyvsp[-1].t_expression)); }
#line 3219 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 168:
#line 903 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new uminus_expression((yyvsp[-1].t_expression)); requires(E_NFLUENTS); }
#line 3225 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 169:
#line 905 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new plus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_NFLUENTS); }
#line 3231 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 170:
#line 907 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new minus_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_NFLUENTS); }
#line 3237 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 171:
#line 909 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_NFLUENTS); }
#line 3243 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 172:
#line 911 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new div_expression((yyvsp[-2].t_expression),(yyvsp[-1].t_expression)); requires(E_NFLUENTS); }
#line 3249 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 173:
#line 912 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)=(yyvsp[0].t_num_expression); }
#line 3255 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 174:
#line 913 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression) = current_analysis->getControlParam((yyvsp[0].cp)); delete [] (yyvsp[0].cp);}
#line 3261 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 175:
#line 914 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= (yyvsp[0].t_func_term); requires(E_NFLUENTS); }
#line 3267 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 176:
#line 919 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression(new special_val_expr(E_HASHT),(yyvsp[-1].t_expression)); }
#line 3273 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 177:
#line 921 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-2].t_expression), new special_val_expr(E_HASHT)); }
#line 3279 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 178:
#line 923 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new special_val_expr(E_HASHT); }
#line 3285 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 179:
#line 928 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_num_expression)=new int_expression((yyvsp[0].ival));   }
#line 3291 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 180:
#line 929 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_num_expression)=new float_expression((yyvsp[0].fval)); }
#line 3297 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 181:
#line 933 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3303 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 182:
#line 936 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3309 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 183:
#line 938 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[0].cp)),
                            new parameter_symbol_list); delete [] (yyvsp[0].cp);}
#line 3316 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 184:
#line 941 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term) = new class_func_term( (yyvsp[-4].t_class), current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp);}
#line 3322 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 185:
#line 958 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3328 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 186:
#line 960 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[-2].cp)), (yyvsp[-1].t_parameter_symbol_list)); delete [] (yyvsp[-2].cp); }
#line 3334 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 187:
#line 962 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_func_term)=new func_term( current_analysis->func_tab.symbol_get((yyvsp[0].cp)),
                            new parameter_symbol_list); delete [] (yyvsp[0].cp);}
#line 3341 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 188:
#line 967 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_GREATER; }
#line 3347 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 189:
#line 968 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_GREATEQ; }
#line 3353 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 190:
#line 969 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_LESS; }
#line 3359 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 191:
#line 970 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_LESSEQ; }
#line 3365 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 192:
#line 971 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_comparison_op)= E_EQUALS; }
#line 3371 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 193:
#line 984 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= (yyvsp[0].t_goal);}
#line 3377 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 194:
#line 991 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal) = new conj_goal((yyvsp[-1].t_goal_list));}
#line 3383 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 195:
#line 994 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());
        requires(E_UNIV_PRECS);}
#line 3390 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 196:
#line 996 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal) = new conj_goal(new goal_list);}
#line 3396 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 197:
#line 997 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal) = new conj_goal(new goal_list);}
#line 3402 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 198:
#line 1002 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3408 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 199:
#line 1004 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-2].cp),(yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3414 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 200:
#line 1006 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new conj_goal((yyvsp[-1].t_goal_list));}
#line 3420 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 201:
#line 1009 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal)= new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_con_goal),current_analysis->var_tab_stack.pop());
                requires(E_UNIV_PRECS);}
#line 3427 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 202:
#line 1012 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = (yyvsp[0].t_con_goal);}
#line 3433 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 203:
#line 1017 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3439 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 204:
#line 1019 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new preference((yyvsp[-2].cp),(yyvsp[-1].t_con_goal));requires(E_PREFERENCES);}
#line 3445 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 205:
#line 1021 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new conj_goal((yyvsp[-1].t_goal_list));}
#line 3451 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 206:
#line 1024 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal)= new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_con_goal),current_analysis->var_tab_stack.pop());
                requires(E_UNIV_PRECS);}
#line 3458 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 207:
#line 1030 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyvsp[-1].t_goal_list)->push_back((yyvsp[0].t_con_goal));}
#line 3464 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 208:
#line 1032 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)= new goal_list; (yyval.t_goal_list)->push_back((yyvsp[0].t_con_goal));}
#line 3470 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 209:
#line 1037 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new preference((yyvsp[-1].t_goal)); requires(E_PREFERENCES);}
#line 3476 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 210:
#line 1039 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new preference((yyvsp[-2].cp),(yyvsp[-1].t_goal)); requires(E_PREFERENCES);}
#line 3482 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 211:
#line 1043 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)=(yyvsp[0].t_goal);}
#line 3488 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 212:
#line 1048 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list) = (yyvsp[-1].t_goal_list); (yyval.t_goal_list)->push_back((yyvsp[0].t_con_goal));}
#line 3494 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 213:
#line 1050 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list) = new goal_list; (yyval.t_goal_list)->push_back((yyvsp[0].t_con_goal));}
#line 3500 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 214:
#line 1055 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal)= new conj_goal((yyvsp[-1].t_goal_list));}
#line 3506 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 215:
#line 1057 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new qfied_goal(E_FORALL,(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_con_goal),current_analysis->var_tab_stack.pop());
        requires(E_UNIV_PRECS);}
#line 3513 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 216:
#line 1060 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ATEND,(yyvsp[-1].t_goal));}
#line 3519 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 217:
#line 1062 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ALWAYS,(yyvsp[-1].t_goal));}
#line 3525 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 218:
#line 1064 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_SOMETIME,(yyvsp[-1].t_goal));}
#line 3531 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 219:
#line 1066 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_WITHIN,(yyvsp[-1].t_goal),NULL,(yyvsp[-2].t_num_expression)->double_value(),0.0);delete (yyvsp[-2].t_num_expression);}
#line 3537 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 220:
#line 1068 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ATMOSTONCE,(yyvsp[-1].t_goal));}
#line 3543 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 221:
#line 1070 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_SOMETIMEAFTER,(yyvsp[-1].t_goal),(yyvsp[-2].t_goal));}
#line 3549 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 222:
#line 1072 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_SOMETIMEBEFORE,(yyvsp[-1].t_goal),(yyvsp[-2].t_goal));}
#line 3555 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 223:
#line 1074 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_ALWAYSWITHIN,(yyvsp[-1].t_goal),(yyvsp[-2].t_goal),(yyvsp[-3].t_num_expression)->double_value(),0.0);delete (yyvsp[-3].t_num_expression);}
#line 3561 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 224:
#line 1076 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_HOLDDURING,(yyvsp[-1].t_goal),NULL,(yyvsp[-2].t_num_expression)->double_value(),(yyvsp[-3].t_num_expression)->double_value());delete (yyvsp[-3].t_num_expression);delete (yyvsp[-2].t_num_expression);}
#line 3567 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 225:
#line 1078 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = new constraint_goal(E_HOLDAFTER,(yyvsp[-1].t_goal),NULL,0.0,(yyvsp[-2].t_num_expression)->double_value());delete (yyvsp[-2].t_num_expression);}
#line 3573 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 226:
#line 1083 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new simple_goal((yyvsp[0].t_proposition),E_POS);}
#line 3579 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 227:
#line 1085 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new neg_goal((yyvsp[-1].t_goal));simple_goal * s = dynamic_cast<simple_goal *>((yyvsp[-1].t_goal));
       if(s && s->getProp()->head->getName()=="=") {requires(E_EQUALITY);}
       else{requires(E_NEGATIVE_PRECONDITIONS);};}
#line 3587 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 228:
#line 1089 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new conj_goal((yyvsp[-1].t_goal_list));}
#line 3593 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 229:
#line 1091 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new disj_goal((yyvsp[-1].t_goal_list));
        requires(E_DISJUNCTIVE_PRECONDS);}
#line 3600 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 230:
#line 1094 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new imply_goal((yyvsp[-2].t_goal),(yyvsp[-1].t_goal));
        requires(E_DISJUNCTIVE_PRECONDS);}
#line 3607 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 231:
#line 1098 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new qfied_goal((yyvsp[-5].t_quantifier),(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());}
#line 3613 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 232:
#line 1101 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new qfied_goal((yyvsp[-5].t_quantifier),(yyvsp[-3].t_var_symbol_list),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());}
#line 3619 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 233:
#line 1103 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new comparison((yyvsp[-3].t_comparison_op),(yyvsp[-2].t_expression),(yyvsp[-1].t_expression));
        requires(E_NFLUENTS);}
#line 3626 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 234:
#line 1109 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyvsp[-1].t_goal_list)->push_back((yyvsp[0].t_goal));}
#line 3632 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 235:
#line 1111 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)= new goal_list; (yyval.t_goal_list)->push_back((yyvsp[0].t_goal));}
#line 3638 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 236:
#line 1116 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyvsp[-1].t_goal_list)->push_back((yyvsp[0].t_goal));}
#line 3644 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 237:
#line 1118 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal_list)= new goal_list; (yyval.t_goal_list)->push_back((yyvsp[0].t_goal));}
#line 3650 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 238:
#line 1128 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_quantifier)=E_FORALL;
        current_analysis->var_tab_stack.push(
        		current_analysis->buildForallTab());}
#line 3658 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 239:
#line 1135 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_quantifier)=E_EXISTS;
        current_analysis->var_tab_stack.push(
        	current_analysis->buildExistsTab());}
#line 3666 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 240:
#line 1142 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_proposition)=new proposition((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_parameter_symbol_list));}
#line 3672 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 241:
#line 1147 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_proposition) = new proposition((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_var_symbol_list));}
#line 3678 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 242:
#line 1152 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_proposition)=new proposition((yyvsp[-2].t_pred_symbol),(yyvsp[-1].t_parameter_symbol_list));}
#line 3684 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 243:
#line 1157 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pred_decl_list)= (yyvsp[-1].t_pred_decl_list);}
#line 3690 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 244:
#line 1159 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_pred_decl_list)=NULL;
	 log_error(E_FATAL,"Syntax error in (:predicates ...)");
	}
#line 3698 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 245:
#line 1166 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_func_decl_list)= (yyvsp[-1].t_func_decl_list);}
#line 3704 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 246:
#line 1168 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_func_decl_list)=NULL;
	 log_error(E_FATAL,"Syntax error in (:functions ...)");
	}
#line 3712 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 247:
#line 1175 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = (yyvsp[-1].t_con_goal);}
#line 3718 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 248:
#line 1177 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_con_goal)=NULL;
      log_error(E_FATAL,"Syntax error in (:constraints ...)");
      }
#line 3726 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 249:
#line 1184 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_con_goal) = (yyvsp[-1].t_con_goal);}
#line 3732 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 250:
#line 1186 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_con_goal)=NULL;
      log_error(E_FATAL,"Syntax error in (:constraints ...)");
      }
#line 3740 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 251:
#line 1192 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_store)=(yyvsp[-1].t_structure_store); (yyval.t_structure_store)->push_back((yyvsp[0].t_structure_def)); }
#line 3746 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 252:
#line 1193 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_store)= new structure_store; (yyval.t_structure_store)->push_back((yyvsp[0].t_structure_def)); }
#line 3752 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 253:
#line 1197 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_action_def); }
#line 3758 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 254:
#line 1198 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_event_def); requires(E_TIME); }
#line 3764 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 255:
#line 1199 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_process_def); requires(E_TIME); }
#line 3770 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 256:
#line 1200 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_durative_action_def); requires(E_DURATIVE_ACTIONS); }
#line 3776 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 257:
#line 1201 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def)= (yyvsp[0].t_derivation_rule); requires(E_DERIVED_PREDICATES);}
#line 3782 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 258:
#line 1202 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_structure_def) = (yyvsp[0].t_class_def); requires(E_MODULES);}
#line 3788 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 259:
#line 1210 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_class_def) = new class_def((yyvsp[-2].t_class),(yyvsp[-1].t_func_decl_list));}
#line 3794 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 260:
#line 1214 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy)= 0;
    	current_analysis->var_tab_stack.push(
    					current_analysis->buildRuleTab());}
#line 3802 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 261:
#line 1225 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_derivation_rule) = new derivation_rule((yyvsp[-2].t_proposition),(yyvsp[-1].t_goal),current_analysis->var_tab_stack.pop());}
#line 3808 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 262:
#line 1237 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_action_def)= current_analysis->buildAction(current_analysis->op_tab.new_symbol_put((yyvsp[-9].cp)),
			(yyvsp[-6].t_var_symbol_list),(yyvsp[-3].t_goal),(yyvsp[-1].t_effect_lists),
			current_analysis->var_tab_stack.pop()); delete [] (yyvsp[-9].cp); }
#line 3816 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 263:
#line 1241 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
	 log_error(E_FATAL,"Syntax error in action declaration.");
	 (yyval.t_action_def)= NULL; }
#line 3824 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 264:
#line 1254 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_event_def)= current_analysis->buildEvent(current_analysis->op_tab.new_symbol_put((yyvsp[-9].cp)),
		   (yyvsp[-6].t_var_symbol_list),(yyvsp[-3].t_goal),(yyvsp[-1].t_effect_lists),
		   current_analysis->var_tab_stack.pop()); delete [] (yyvsp[-9].cp);}
#line 3832 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 265:
#line 1259 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
	 log_error(E_FATAL,"Syntax error in event declaration.");
	 (yyval.t_event_def)= NULL; }
#line 3840 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 266:
#line 1271 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_process_def)= current_analysis->buildProcess(current_analysis->op_tab.new_symbol_put((yyvsp[-9].cp)),
		     (yyvsp[-6].t_var_symbol_list),(yyvsp[-3].t_goal),(yyvsp[-1].t_effect_lists),
                     current_analysis->var_tab_stack.pop()); delete [] (yyvsp[-9].cp);}
#line 3848 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 267:
#line 1275 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
	 log_error(E_FATAL,"Syntax error in process declaration.");
	 (yyval.t_process_def)= NULL; }
#line 3856 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 268:
#line 1281 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list) = (yyvsp[-1].t_var_symbol_list); current_analysis->setControlParams((yyval.t_var_symbol_list));}
#line 3862 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 269:
#line 1282 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_var_symbol_list) = NULL; current_analysis->setControlParams((yyval.t_var_symbol_list));}
#line 3868 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 270:
#line 1293 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_durative_action_def)= (yyvsp[-1].t_durative_action_def);
      (yyval.t_durative_action_def)->name= current_analysis->op_tab.new_symbol_put((yyvsp[-9].cp));
      (yyval.t_durative_action_def)->symtab= current_analysis->var_tab_stack.pop();
      (yyval.t_durative_action_def)->control_parameters = current_analysis->getControlParams();
      (yyval.t_durative_action_def)->parameters= (yyvsp[-6].t_var_symbol_list);
      (yyval.t_durative_action_def)->dur_constraint= (yyvsp[-2].t_goal);
      delete [] (yyvsp[-9].cp);
      delete (yyvsp[-4].t_var_symbol_list);
    }
#line 3882 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 271:
#line 1304 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
	 log_error(E_FATAL,"Syntax error in durative-action declaration.");
	 (yyval.t_durative_action_def)= NULL; }
#line 3890 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 272:
#line 1311 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_durative_action_def)=(yyvsp[-2].t_durative_action_def); (yyval.t_durative_action_def)->effects=(yyvsp[0].t_effect_lists);}
#line 3896 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 273:
#line 1313 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_durative_action_def)=(yyvsp[-2].t_durative_action_def); (yyval.t_durative_action_def)->precondition=(yyvsp[0].t_goal);}
#line 3902 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 274:
#line 1314 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_durative_action_def)= current_analysis->buildDurativeAction();}
#line 3908 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 275:
#line 1319 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)=(yyvsp[0].t_goal); }
#line 3914 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 276:
#line 1321 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal)= new conj_goal((yyvsp[-1].t_goal_list)); }
#line 3920 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 277:
#line 1326 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)=(yyvsp[-1].t_goal_list); (yyval.t_goal_list)->push_back((yyvsp[0].t_goal)); }
#line 3926 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 278:
#line 1328 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_goal_list)= new goal_list; }
#line 3932 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 279:
#line 1333 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new timed_goal((yyvsp[-1].t_goal),E_AT_START);}
#line 3938 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 280:
#line 1335 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new timed_goal((yyvsp[-1].t_goal),E_AT_END);}
#line 3944 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 281:
#line 1337 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)= new timed_goal((yyvsp[-1].t_goal),E_OVER_ALL);}
#line 3950 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 282:
#line 1339 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {timed_goal * tg = dynamic_cast<timed_goal *>((yyvsp[-1].t_goal));
		(yyval.t_goal) = new timed_goal(new preference((yyvsp[-2].cp),tg->clearGoal()),tg->getTime());
			delete tg;
			requires(E_PREFERENCES);}
#line 3959 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 283:
#line 1344 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal) = new preference((yyvsp[-1].t_goal));requires(E_PREFERENCES);}
#line 3965 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 284:
#line 1348 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_dummy)= 0; current_analysis->var_tab_stack.push(
    				current_analysis->buildOpTab());}
#line 3972 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 285:
#line 1353 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_EQUALITY;}
#line 3978 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 286:
#line 1354 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_STRIPS;}
#line 3984 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 287:
#line 1356 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_TYPING;}
#line 3990 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 288:
#line 1358 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_NEGATIVE_PRECONDITIONS;}
#line 3996 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 289:
#line 1360 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_DISJUNCTIVE_PRECONDS;}
#line 4002 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 290:
#line 1361 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_EXT_PRECS;}
#line 4008 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 291:
#line 1362 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_UNIV_PRECS;}
#line 4014 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 292:
#line 1363 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_COND_EFFS;}
#line 4020 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 293:
#line 1364 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_NFLUENTS | E_OFLUENTS;}
#line 4026 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 294:
#line 1366 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_DURATIVE_ACTIONS;}
#line 4032 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 295:
#line 1367 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_TIME |
                      E_NFLUENTS |
                      E_DURATIVE_ACTIONS; }
#line 4040 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 296:
#line 1370 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)=E_ACTIONCOSTS | E_NFLUENTS;}
#line 4046 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 297:
#line 1373 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)=E_OFLUENTS;}
#line 4052 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 298:
#line 1374 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)=E_NFLUENTS;}
#line 4058 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 299:
#line 1375 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)=E_MODULES;}
#line 4064 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 300:
#line 1377 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_STRIPS |
		      E_TYPING |
		      E_NEGATIVE_PRECONDITIONS |
		      E_DISJUNCTIVE_PRECONDS |
		      E_EQUALITY |
		      E_EXT_PRECS |
		      E_UNIV_PRECS |
		      E_COND_EFFS;}
#line 4077 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 301:
#line 1386 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_EXT_PRECS |
		      E_UNIV_PRECS;}
#line 4084 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 302:
#line 1390 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_DURATION_INEQUALITIES;}
#line 4090 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 303:
#line 1393 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag)= E_CONTINUOUS_EFFECTS;}
#line 4096 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 304:
#line 1395 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_DERIVED_PREDICATES;}
#line 4102 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 305:
#line 1397 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_TIMED_INITIAL_LITERALS;}
#line 4108 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 306:
#line 1399 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_PREFERENCES;}
#line 4114 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 307:
#line 1401 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_pddl_req_flag) = E_CONSTRAINTS;}
#line 4120 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 308:
#line 1403 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {log_error(E_WARNING,"Unrecognised requirements declaration ");
       (yyval.t_pddl_req_flag)= 0; delete [] (yyvsp[0].cp);}
#line 4127 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 309:
#line 1409 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[-1].t_const_symbol_list);}
#line 4133 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 310:
#line 1413 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_type_list)=(yyvsp[-1].t_type_list); requires(E_TYPING);}
#line 4139 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 311:
#line 1423 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[-1].t_problem); (yyval.t_problem)->name = (yyvsp[-7].cp); (yyval.t_problem)->domain_name = (yyvsp[-3].cp);
		if (types_used && !types_defined) {
			yyerrok; log_error(E_FATAL,"Syntax error in problem file - types used, but no :types section in domain file.");
		}

	}
#line 4150 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 312:
#line 1430 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok; (yyval.t_problem)=NULL;
       	log_error(E_FATAL,"Syntax error in problem definition."); }
#line 4157 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 313:
#line 1436 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->req= (yyvsp[-1].t_pddl_req_flag);}
#line 4163 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 314:
#line 1437 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->objects= (yyvsp[-1].t_const_symbol_list);}
#line 4169 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 315:
#line 1438 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->initial_state= (yyvsp[-1].t_effect_lists);}
#line 4175 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 316:
#line 1439 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->the_goal= (yyvsp[-1].t_goal);}
#line 4181 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 317:
#line 1441 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->constraints = (yyvsp[-1].t_con_goal);}
#line 4187 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 318:
#line 1442 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); if((yyval.t_problem)->metric == 0) {(yyval.t_problem)->metric= (yyvsp[-1].t_metric);}
											else {(yyval.t_problem)->metric->add((yyvsp[-1].t_metric));}}
#line 4194 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 319:
#line 1444 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=(yyvsp[0].t_problem); (yyval.t_problem)->length= (yyvsp[-1].t_length_spec);}
#line 4200 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 320:
#line 1445 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_problem)=new problem;}
#line 4206 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 321:
#line 1448 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_const_symbol_list)=(yyvsp[-1].t_const_symbol_list);}
#line 4212 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 322:
#line 1451 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_effect_lists)=(yyvsp[-1].t_effect_lists);}
#line 4218 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 323:
#line 1454 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.vtab) = current_analysis->buildOpTab();}
#line 4224 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 324:
#line 1457 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_goal)=(yyvsp[-1].t_goal);delete (yyvsp[-2].vtab);}
#line 4230 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 325:
#line 1462 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_metric)= new metric_spec((yyvsp[-2].t_optimization),(yyvsp[-1].t_expression)); }
#line 4236 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 326:
#line 1464 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {yyerrok;
        log_error(E_FATAL,"Syntax error in metric declaration.");
        (yyval.t_metric)= NULL; }
#line 4244 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 327:
#line 1471 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_length_spec)= new length_spec(E_BOTH,(yyvsp[-3].ival),(yyvsp[-1].ival));}
#line 4250 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 328:
#line 1474 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_length_spec) = new length_spec(E_SERIAL,(yyvsp[-1].ival));}
#line 4256 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 329:
#line 1478 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_length_spec) = new length_spec(E_PARALLEL,(yyvsp[-1].ival));}
#line 4262 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 330:
#line 1484 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_optimization)= E_MINIMIZE;}
#line 4268 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 331:
#line 1485 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_optimization)= E_MAXIMIZE;}
#line 4274 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 332:
#line 1490 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[-1].t_expression);}
#line 4280 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 333:
#line 1491 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_func_term);}
#line 4286 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 334:
#line 1492 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression)= (yyvsp[0].t_num_expression);}
#line 4292 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 335:
#line 1493 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new special_val_expr(E_TOTAL_TIME); }
#line 4298 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 336:
#line 1495 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = new violation_term((yyvsp[-1].cp));}
#line 4304 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 337:
#line 1496 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new special_val_expr(E_TOTAL_TIME); }
#line 4310 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 338:
#line 1500 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new plus_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 4316 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 339:
#line 1501 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new minus_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 4322 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 340:
#line 1502 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new mul_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 4328 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 341:
#line 1503 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    { (yyval.t_expression)= new div_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression)); }
#line 4334 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 342:
#line 1507 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = (yyvsp[0].t_expression);}
#line 4340 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 343:
#line 1509 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = new plus_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression));}
#line 4346 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 344:
#line 1513 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = (yyvsp[0].t_expression);}
#line 4352 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 345:
#line 1515 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_expression) = new mul_expression((yyvsp[-1].t_expression),(yyvsp[0].t_expression));}
#line 4358 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 346:
#line 1521 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan)= (yyvsp[0].t_plan);
         (yyval.t_plan)->push_front((yyvsp[-1].t_step)); }
#line 4365 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 347:
#line 1524 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan) = (yyvsp[0].t_plan);(yyval.t_plan)->insertTime((yyvsp[-1].fval));}
#line 4371 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 348:
#line 1526 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan) = (yyvsp[0].t_plan);(yyval.t_plan)->insertTime((yyvsp[-1].ival));}
#line 4377 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 349:
#line 1528 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_plan)= new plan;}
#line 4383 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 350:
#line 1533 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)=(yyvsp[0].t_step);
         (yyval.t_step)->start_time_given=1;
         (yyval.t_step)->start_time=(yyvsp[-2].fval);}
#line 4391 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 351:
#line 1537 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)=(yyvsp[0].t_step);
	 (yyval.t_step)->start_time_given=0;}
#line 4398 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 352:
#line 1543 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)= (yyvsp[-3].t_step);
	 (yyval.t_step)->duration_given=1;
         (yyval.t_step)->duration= (yyvsp[-1].fval);}
#line 4406 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 353:
#line 1547 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)= (yyvsp[0].t_step);
         (yyval.t_step)->duration_given=0;}
#line 4413 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 354:
#line 1553 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.t_step)= new plan_step(
              current_analysis->op_tab.symbol_get((yyvsp[-2].cp)),
	      (yyvsp[-1].t_const_symbol_list)); delete [] (yyvsp[-2].cp);
      }
#line 4422 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 355:
#line 1560 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.fval)= (yyvsp[0].fval);}
#line 4428 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;

  case 356:
#line 1561 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1646  */
    {(yyval.fval)= (float) (yyvsp[0].ival);}
#line 4434 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
    break;


#line 4438 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.cpp" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1564 "/home/emre/popCorn/planner/VALfiles/parsing/pddl+.yacc" /* yacc.c:1906  */


#include <cstdio>
#include <iostream>
int line_no= 1;
using std::istream;
#include "lex.yy.h"

namespace VAL {
extern yyFlexLexer* yfl;
};


int yyerror(char * s)
{
    return 0;
}

int yylex()
{
    return yfl->yylex();
}
