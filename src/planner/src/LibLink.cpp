#include "LibLink.h"
extern "C" {

#include <dlfcn.h>

}
#include <string>
#include <sstream>

using std::string;
using std::stringstream;
using VAL::class_def;

namespace Planner {

void * LibLoader::getLib(class_def * cd)
{
    void * h = cd->getLib();
    if(!h)
    {
        stringstream s;
 #ifndef LINUX
        s << cd->name->getName() << ".dll";
 #else
        s << cd->name->getName() << ".so";
 #endif
        h = dlopen(s.str().c_str(),RTLD_LAZY);
        cd->setLib(h);
    }
    return h;
}

double LibLoader::makeCall(class_def * cd,const string & nm,vector<string> args)
{
    const char * argvs[args.size()];
    for(int i = 0;i < args.size();++i)
    {
        argvs[i] = args[i].c_str();
    }
    void * h = getLib(cd);
    if(!h)
    {
        cout << "Failed to find and load library\n";
    }
    double (*fn)(const char * *);
    //cout << "Want to call " << nm.c_str() << "\n";
    * (void * *) (&fn) = dlsym(h,nm.c_str());
    return (*fn)(argvs);
}

}
