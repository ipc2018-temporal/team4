#ifndef __IPSA
#define __IPSA

#include <iostream>
#include <utility>
#include <string>
#include <list>
#include <sstream>
#include <fstream>
#include <map>
#include "Network.h"
#include "solver.h"

namespace IPSA {
    extern char* fileName;
    extern char* moduleName;
    extern bool isIPSA;
    extern std::map <std::string, double> newStatus;
    extern int count;
    extern std::ostringstream datfn;
    extern std::string fn;
    extern bool isGood;
    extern std::ofstream datfile;
    extern bool restore;
    extern Network theNetwork;
    extern Solver* solver;
};

#endif
