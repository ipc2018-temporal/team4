#ifndef __GeneratorCostData_h__
#define __GeneratorCostData_h__

#include <iostream>
#include <map>

class GeneratorCostData{

 public:
  GeneratorCostData();
  GeneratorCostData(string line,int id=0);
  void printGeneratorCostData(ostream & o) const;
  double getX1(){
    return x1;
  }
 private:
  string name;
  int gcd_i;
  int status;
  double startup;
  double shutdown;
  int gcd_n;
  double x1;
  double x2;
  double x3;
};

#endif
