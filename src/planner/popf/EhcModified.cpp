#ifndef __EhcModified
#define __EhcModified

namespace EhcModified{
  extern bool isActive = false;
  extern bool isDebug = false;
  extern int level = 0;
}

#endif
