#ifndef __DrawGraph
#define __DrawGraph

#include <iostream>
#include <utility>
#include <map>
#include <sstream>
#include <list>

using namespace std;

namespace DrawGraph{

    extern int stateID = 0;
  extern int orderID = 0;
    extern map<int, double> heuristic = map<int, double>();
  extern map<int, list<int> > order = map<int, list<int> >();
    extern stringstream graph(" ");
}

#endif
