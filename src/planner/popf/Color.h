#ifndef __Color_h__
#define __Color_h__

#include <iostream>
#include <string>

using namespace std;

class Color{
 public:
    Color();
    Color(int red, int green, int blue);
    //Color(string hex);
    int getRed(){
	return red;
    };
    int getGreen(){
	return green;
    }
    int getBlue(){
	return blue;
    }
    string getHex(){
	return hex;
    }
 private:
    string hex;
    int red;
    int green;
    int blue;
    string fromRGB2Hex(int red, int green, int blue);
    int* fromHex2RGB(string Hex);
    string byte2Hex(int number);
};

void makeColorGradien(double frequency1, double frequency2, double frequency3, double phase1, double phase2, double phase3, double center, double width, int len = 50, Color color[] = NULL);

void makeColorGradien(double frequency, double phase1, double phase2, double phase3, double center, double width, int len = 50, Color color[] = NULL);

void makeColorGradien(double frequency, double phase, double center, double width, int len = 50, Color color[] = NULL);

void makeColorGradien(double frequency, double center, double width, int len = 50, Color color[] = NULL);

void makeGrayGradien(double frequency, double phase, double center, double width, int len = 50, Color color[] = NULL);

Color createColor(double i, double min, double max, double center = 128., double width = 127.);
#endif
