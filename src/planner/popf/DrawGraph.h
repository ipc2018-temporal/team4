#ifndef __DrawGraph
#define __DrawGraph

#include <iostream>
#include <utility>
#include <map>
#include <sstream>
#include <list>

using namespace std;

namespace DrawGraph{

    extern int stateID;
    extern int orderID;
    extern map <int, double> heuristic;
    extern map <int, list<int> > order;
    extern stringstream graph;
}

#endif
