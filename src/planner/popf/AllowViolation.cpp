#ifndef __AllowViolation
#define __AllowViolation

#include <iostream>
#include <utility>
#include <string>
#include <list>
#include <set>

using namespace std;

namespace AllowViolation{
    bool allowViolation=false;
    set<int> precViolated;
    bool isNowViolated = false;
    list<int> stepInBetween;
    bool isNotViolatedAnymore = false;
    int oldStepID=-1;
}

#endif
