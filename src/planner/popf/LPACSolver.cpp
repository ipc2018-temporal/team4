#include <iostream>
#include <cassert>
//#include <OsiClpSolverInterface.hpp>
//#include <CoinPackedMatrix.hpp>
//#include <CoinPackedVector.hpp>
#include <math.h>
#include <map>
#include <sstream>
#include "Network.h"
#include "Busbar.h"
#include "Solver.h"
#include "LPACSolver.h"

using namespace std;

LPACSolver::LPACSolver(Network *net, int t, int seg):Solver(net){
  type = t;
  s = seg;
  model = new OsiClpSolverInterface();
  nBus = theNetwork->getNBus();
  nBranch = theNetwork->getNBranch();
  map<int, Busbar*>::iterator busIt = busbars.begin();
  map<int, Busbar*>::iterator busEnd = busbars.end();
  for(;busIt!=busEnd;++busIt){
    Busbar *busbar = busIt->second;
    int idBus = busIt->first;
    if(busbar->getType()==1){
      voltageTarget[idBus]=1.;
    }else{
      voltageTarget[idBus]=1.;//busbar->getVm();
    }
  }
}

bool LPACSolver::solvePF(){

  cout << "Inside LPAC solver " << endl;
  buildModel();
  return solveModel();
}


void LPACSolver::buildModel(){

  calculateCosApproximation();
  map<pair<int, int>, double> mapTap = theNetwork->getMapTap();
  // column definition
  nCol = 2*nBus+nBranch;

  cout << "N variables : " << nCol << endl;
  double *objective = new double[nCol];
  double *col_lb = new double[nCol];
  double *col_ub = new double[nCol];
  string *col_name = new string[nCol];
  map<int, Busbar*>::iterator busIt = busbars.begin();
  map<int, Busbar*>::iterator busEnd = busbars.end();
  for(int i=0;busIt!=busEnd;++i, ++busIt){
    stringstream namePhi;
    int indexPhi = i+nBus;
    stringstream name;
    name << "delta_{" << busIt->second->getBus_i() <<"}";

    col_name[i]=name.str();
    //    cout << i << " " << name.str() << " added " << endl;
    col_lb[i]=-model->getInfinity();
    col_ub[i]=model->getInfinity();
    objective[i]=0;
    model->setColName(i, col_name[i]);
    mapDelta[busIt->second->getBus_i()]=i;
    mapPhi[busIt->second->getBus_i()]=indexPhi;

    namePhi <<"phi_{" << busIt->second->getBus_i() << "}";
    //    cout << indexPhi << " " << namePhi.str() << " added " << endl;
    col_name[indexPhi]=namePhi.str();
    col_lb[indexPhi]=-voltageTarget[busIt->first];
    col_ub[indexPhi]=model->getInfinity();
    objective[indexPhi]=0;
    model->setColName(indexPhi, col_name[indexPhi]);
  }


  map<int, Branch*>::iterator brIt = branches.begin();
  map<int, Branch*>::iterator brEnd = branches.end();
  for(int i=2*nBus;brIt!=brEnd;++i,++brIt){
    Branch *branch = brIt->second;
    stringstream name;
    int fBus = branch->getFBus();
    int tBus = branch->getTBus();
    name << "cos" << fBus << "_"<< tBus;
    mapCos[make_pair(fBus,tBus)]=i;
    mapCos[make_pair(tBus,fBus)]=i;
    col_name[i]=name.str();
    col_lb[i]=0;
    col_ub[i]=1.;
    objective[i]=-1;
    model->setColName(i, col_name[i]);
    //    cout << i << " " << name.str() << " added " << endl;
  }

  // set row
  nRow = 2*nBus+s*nBranch;
  double *row_lb = new double[nRow];
  double *row_ub = new double[nRow];

  // Define constraint matrix;

  CoinPackedMatrix *matrix = new CoinPackedMatrix(false, 0, 0);
  matrix->setDimensions(0, nCol);



  //
  busIt = busbars.begin();
  map<int,CoinPackedVector> rows;
  for(int i=0;busIt!=busEnd;++i, ++busIt){
    int idBus = busIt->first;
    Busbar *busbar = busIt->second;
    int type = busbar->getType();
    int indexP = i;
    int indexQ = i+nBus;
    if(type==3){
      rows[i].insert(i,1.);
      row_lb[i]=0.;
      row_ub[i]=0.;

      rows[indexQ].insert(indexQ,1.);
      row_lb[indexQ]=voltage[idBus]-1.;
      row_ub[indexQ]=voltage[idBus]-1.;
    }else{
      int indexB = busIt->first;
      double totalP = -activePowerGen[indexB]-activePowerLoad[indexB];
      double totalQ = -reactivePowerGen[indexB]-reactivePowerLoad[indexB];
      //      cout << "bus " << i << " " <totalP << " ";
      map<int,Busbar*>::iterator busSIt=busbars.begin();
      map<int,Busbar*>::iterator busSEnd=busbars.end();
      map<int, double> coefficientsP;
      map<int, double> coefficientsQ;
      for(;busSIt!=busSEnd;++busSIt){
	Busbar* busbarS = busSIt->second;
	int idBusS = busbarS->getBus_i();
	if(idBus==idBusS){
	  continue;
	}
	pair<int,int> index(idBus,idBusS);
	if(mapCos.find(index)==mapCos.end()){
	  continue;
	}
	//cout << idBus << " " << idBusS<< " "<<  totalP << " " << totalQ << endl;
	totalP-=reY[index]/mapTap[index];
	totalQ+=(imY[index]-bY[index])/mapTap[index];
	int idCos = mapCos[index];
	//cout << indexP << " " << indexQ << " " << idBus << " " << idBusS << " " << idCos << " " << imY[index] << " " << reY[index] << " " << mapDelta[idBus]<< endl;
	coefficientsP[idCos]-=reY[index];
	coefficientsP[mapDelta[idBus]]-=(imY[index]);
	coefficientsP[mapDelta[idBusS]]+=(imY[index]);
	if(type==1){
	  coefficientsQ[mapDelta[idBus]]-=reY[index];
	  coefficientsQ[mapDelta[idBusS]]+=reY[index];
	  coefficientsQ[idCos]+=(imY[index]);
	  coefficientsQ[mapPhi[idBus]]-=(imY[index]);
	  coefficientsQ[mapPhi[idBusS]]+=(imY[index]);
	}
      }
      row_lb[indexP]=totalP;
      row_ub[indexP]=totalP;
      if(type==2){
	coefficientsQ[indexQ]=1;
	row_lb[indexQ]=voltage[idBus]-1;
	row_ub[indexQ]=voltage[idBus]-1;
      }else{
	row_lb[indexQ]=totalQ;
	row_ub[indexQ]=totalQ;
      }

      map<int, double>::iterator cpIt = coefficientsP.begin();
      map<int, double>::iterator cpEnd = coefficientsP.end();
      for(;cpIt!=cpEnd;++cpIt){
	rows[indexP].insert(cpIt->first,cpIt->second);
      }
      map<int, double>::iterator cqIt = coefficientsQ.begin();
      map<int, double>::iterator cqEnd = coefficientsQ.end();
      for(;cqIt!=cqEnd;++cqIt){
	rows[indexQ].insert(cqIt->first,cqIt->second);
      }
    }
  }
  brIt=branches.begin();
  //map<int,Branch*>::iterator brEnd=branches.end();
  for(int i=2*nBus;brIt!=brEnd;++brIt){
    Branch *branch = brIt->second;
    int idBusT = branch->getTBus();
    int idBusF = branch->getFBus();
    pair<int,int>index(idBusT,idBusF);
    for(int j=0;j<s;j++,i++){
      double sloc = mCos[j];
      double max = qCos[j];
      double min = -model->getInfinity();
      rows[i].insert(mapCos[index],1);
      rows[i].insert(mapDelta[idBusT],-sloc);
      rows[i].insert(mapDelta[idBusF],sloc);
      row_lb[i]=min;
      row_ub[i]=max;
      //      cout << "b: " << i << " " << row_lb[i]<< " " << row_ub[i] <<endl;
    }

  }

  //  cout << "rows completed " << endl;
  for(int i=0;i<nRow;i++){
    matrix->appendRow(rows[i]);
  }
  model->loadProblem(*matrix, col_lb, col_ub, objective, row_lb, row_ub);

}


bool LPACSolver::solveModel(){
  model->writeLp("example");
  model->initialSolve();
  if(model->isProvenOptimal()){
    takeSolution();
    return true;
  }
  return false;
}

void LPACSolver::takeSolution(){

  int n = model->getNumCols();
  solution = model->getColSolution();
  map<int,Busbar*>::iterator busIt=busbars.begin();
  map<int,Busbar*>::iterator busEnd=busbars.end();
  for(int i=0;i<n;i++){
  }
  for(int i=0;busIt!=busEnd;++busIt,++i){
    int index = busIt->first;
    voltage[index]=voltageTarget[index]+solution[i+nBus];
    delta[index]=solution[i];
  }
  map<int, Branch*>::iterator brIt=branches.begin();
  map<int, Branch*>::iterator brEnd=branches.end();
  for(;brIt!=brEnd;++brIt){
    Branch *branch = brIt->second;
    int idBusT = branch->getTBus();
    int idBusF = branch->getFBus();
    pair <int, int> index(idBusF, idBusT);
    int solIndex = mapCos[index];
    cosine[index]=solution[solIndex];
  }
}
void LPACSolver::calculateCosApproximation(){
  mCos = new double[s];
  qCos = new double[s];
  double l=-cos(0.5);
  double h=cos(0.5);
  double inc = (h-l)/(s+1);
  double loc = l+inc;
  for (int k=0;k<s;k++){
    double floc = cos(loc);
    double sloc = -sin(loc);
    mCos[k] = sloc;
    qCos[k]=-sloc*loc+floc;
    loc=loc+inc;
  }
}

void LPACSolver::readModel(){
  model->readMps("test");
  model->writeLp("testLp");
}

void LPACSolver::calculateLinePF(){
  int typeOfLine = 0;
  if(typeOfLine == 0){
    Solver::calculateLinePF();
  }else{
    calculateAccordingModel();
  }

}

void LPACSolver::calculateAccordingModel(){
  map<pair<int, int>, double> mapTap = theNetwork->getMapTap();
  map<int, Branch*>::iterator brIt = branches.begin();
  map<int, Branch*>::iterator brEnd = branches.end();
  for(;brIt!=brEnd;++brIt){
    Branch *branch = brIt->second;
    int indexI = branch->getFBus();
    int indexJ = branch->getTBus();
    pair<int, int> index (indexI,indexJ);
    double tap;
    if(branch->getRatio()==0){
      tap = 1.;
    }else{
      tap = branch->getRatio();
    }

    double activePowerIn =-(reY[index]/mapTap[index]-reY[index]*cosine[index]-imY[index]*(delta[indexI]-delta[indexJ]));
    double reactivePowerIn = -(-((imY[index]-bY[index])/mapTap[index])-reY[index]*(delta[indexI]-delta[indexJ])+imY[index]*cosine[index]-imY[index]*(voltage[indexI]-voltage[indexJ]));
    pair<int, int> index2(indexJ,indexI);
    double activePowerOut = -(reY[index2]/mapTap[index2]-reY[index2]*cosine[index]-imY[index2]*(delta[indexJ]-delta[indexI]));
    double reactivePowerOut =-(-((imY[index2]-bY[index2])/mapTap[index2])-reY[index2]*(delta[indexJ]-delta[indexI])+imY[index2]*cosine[index]-imY[index2]*(voltage[indexJ]-voltage[indexI]));

    branch->setActivePowerIn(activePowerIn*baseMVA);
    branch->setReactivePowerIn(reactivePowerIn*baseMVA);
    branch->setActivePowerOut(activePowerOut*baseMVA);
    branch->setReactivePowerOut(reactivePowerOut*baseMVA);
  }

}
