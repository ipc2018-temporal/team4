#include <iostream>
#include <stdlib.h>
#include "Network.h"
//#include "WritePDDLProblem.h"

using namespace std;

int main(int argc, char** argv){
  if(argc<4){
    cerr << "Usage : \n";
    cerr << "\t" << argv[0] << " matlabFile output scaleFactor\n";
    return 1;
  }else{
    cout << "Welcome in " << argv[0] << endl;
  }
  string inputFile = argv[1];
  string outputFile = argv[2];
  string completeOutput = "/home/chiara/Code/Network/TestCases/";
  completeOutput+=outputFile;
  string line = "mkdir " + completeOutput;
  cout << line << endl;
  system(line.c_str());
  completeOutput+="/case";
  double scaleFactor = atof(argv[3]);
  Network theNetwork(inputFile);
  cout << theNetwork << endl;
  theNetwork.printNetworkAsMatlabFile(cout);

  theNetwork.changeLoadsFromFile("/home/chiara/Code/Network/TestCases/input/Demand/Divided/2012/01-Jan-12.txt", completeOutput,scaleFactor);
  //WritePDDLProblem problemFile(theNetwork, "/home/chiara/Code/Network/TestCases/input/Demand/Divided/2012/01-Jan-12.txt", "/home/chiara/Code/Network/TestCases/problem2.pddl");
  //problemFile.writePDDLProblemInFile();

}
