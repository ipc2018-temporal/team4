#ifndef __LPACSolverOPF_h__
#define __LPACSolverOPF_h__

#include "Solver.h"
#include "LPACSolver.h"
#include "LPACSolverOPF.h"
#include <OsiClpSolverInterface.hpp>

class LPACSolverOPF: public LPACSolver{
 public:
  LPACSolverOPF(Network* net, int t = 2, int seg = 7);
  virtual bool solvePF();
 protected:
  map<int, int> mapPGen;
  map<int, int> mapQGen;
  virtual void buildModel();
  virtual void calculatePowerGenerators();
};

#endif
