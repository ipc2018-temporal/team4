#include "Comparator.h"
//#include "gnuplot-iostream/gnuplot-iostream.h"
#include "Network.h"
#include <iostream>
#include <vector>
#include <iomanip>

using namespace std;

string names[]={"voltage","delta","active power","reactive power"};

Comparator::Comparator(Network net, int i, int j){
  network1 = net;
  network2 = net;
  mode1 = i;
  mode2 = j;
  network1.solvePowerFlow(mode1);
  network2.solvePowerFlow(mode2);
}

void Comparator::compare(int variable, bool printGraph){
  vector<double> x;
  vector<double> y;
  vector<pair<double, double> >xy;
  bool gotVariables = getVariables(variable, x, y);
  if(!gotVariables){
    return;
  }
  int n = x.size();
  double meanX=0, meanY=0;
  vector<double> diff;
  for(int i=0;i<n;i++){
    meanX+=x.at(i);
    meanY+=y.at(i);
    xy.push_back(make_pair(x.at(i),y.at(i)));
    diff.push_back(fabs(x.at(i)-y.at(i)));
  }
  meanX=meanX/n;
  meanY=meanY/n;
  double correlation;
  double sigmaX=0;
  double sigmaY=0;
  double meanDiff=0;
  double maxDelta=0;
  double maxI=0;
  for(int i=0;i<n;i++){
    correlation+=(x.at(i)-meanX)*(y.at(i)-meanY);
    sigmaX+=(x.at(i)-meanX)*(x.at(i)-meanX);
    sigmaY+=(y.at(i)-meanY)*(y.at(i)-meanY);
    meanDiff+=diff.at(i);
    if(diff.at(i)>maxDelta){
      maxDelta=diff.at(i);
      maxI=i;
    }
  }

  correlation = correlation/(sqrt(sigmaX*sigmaY));
  meanDiff=meanDiff/n;
  double delta=diff.at(maxI)/max(fabs(x.at(maxI)), fabs(y.at(maxI)))*100;
  cout << setprecision(3)<< names[variable] << " correlation " << correlation << ", \mu(\Delta) " << meanDiff << ", max(\Delta) " << maxDelta <<  ", delta " << delta << endl;

  if(printGraph){
    drawGraph(xy,names[variable]);
  }
}

bool Comparator::getVariables(int variable, vector<double>& x, vector<double>&y){
  if (variable==0){
    map<int,Busbar*> busbars = network1.getBusbars();
    map<int,Busbar*>::iterator busIt=busbars.begin();
    map<int,Busbar*>::iterator busEnd=busbars.end();
    for(int i=0;busIt!=busEnd;++busIt,++i){
      x.push_back(busIt->second->getVm());
    }

    busbars = network2.getBusbars();
    busIt=busbars.begin();
    busEnd=busbars.end();
    for(int i=0;busIt!=busEnd;++busIt,++i){
      y.push_back(busIt->second->getVm());
    }

  }else if(variable==1){
    map<int,Busbar*> busbars = network1.getBusbars();
    map<int,Busbar*>::iterator busIt=busbars.begin();
    map<int,Busbar*>::iterator busEnd=busbars.end();
    for(int i=0;busIt!=busEnd;++busIt,++i){
      x.push_back(busIt->second->getVa());
    }

    busbars = network2.getBusbars();
    busIt=busbars.begin();
    busEnd=busbars.end();
    for(int i=0;busIt!=busEnd;++busIt,++i){
      y.push_back(busIt->second->getVa());
    }

  }else if(variable==2){
    map<int,Branch*> branches = network1.getBranches();
    map<int,Branch*>::iterator brIt=branches.begin();
    map<int,Branch*>::iterator brEnd=branches.end();
    for(int i=0;brIt!=brEnd;++brIt,++i){
      x.push_back(brIt->second->getActivePowerIn());
    }

    branches = network2.getBranches();
    brIt=branches.begin();
    brEnd=branches.end();
    for(int i=0;brIt!=brEnd;++brIt,++i){
      y.push_back(brIt->second->getActivePowerIn());
    }


  }else if(variable==3){
    map<int,Branch*> branches = network1.getBranches();
    map<int,Branch*>::iterator brIt=branches.begin();
    map<int,Branch*>::iterator brEnd=branches.end();
    for(int i=0;brIt!=brEnd;++brIt,++i){
      x.push_back(brIt->second->getReactivePowerIn());
    }

    branches = network2.getBranches();
    brIt=branches.begin();
    brEnd=branches.end();
    for(int i=0;brIt!=brEnd;++brIt,++i){
      y.push_back(brIt->second->getReactivePowerIn());
    }




  }else{
    cerr << "Variable not found" << endl;
    return false;
  }
  return true;
}

void Comparator::drawGraph(vector<pair<double,double> >& xy, string name){
//  Gnuplot gp;
  double xMin=9999999;
  double xMax=-9999999;
  for(int i=0;i<xy.size();++i){
    double x = xy[i].first;
    double y = xy[i].second;
    if(x<xMin){
      xMin = x;
    }
    if(x>xMax){
      xMax = x;
    }
    if(y<xMin){
      xMin = y;
    }
    if(y>xMax){
      xMax = y;
    }
  }
  vector<pair<int, int> > function(2);
  function[0]=make_pair(xMin,xMin);
  function[1]=make_pair(xMax,xMax);
//  gp << " set xrange["<< xMin<<":"<<xMax<<"]\n";
//  gp << " set yrange["<< xMin<<":"<<xMax<<"]\n";
//  gp << "set title '" << name << "'\n";
//  gp << "set key left\n";
//  gp << "plot '-' with points title 'data',";
//  gp << "'-' with lines title 'y=f(x)'\n";

//  gp.send(xy);
//  gp.send(function);

}
