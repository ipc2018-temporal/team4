#ifndef __Busbar_h__
#define __Busbar_h__
#include <iostream>
#include <map>

using namespace std;

class Busbar{

 public:
  Busbar();
  Busbar(string line);
  void printBusbar(ostream & o) const;
  void changeLoad(double newPd, double newQd);
  void changePdLoad(double newPd);
  void changeQdLoad(double newQd);
  bool getIsLoad() const {
    return isLoad;
  }
  int getBus_i() const {
    return bus_i;
  }
  string getName() const {
    return name;
  }
  double getPdLoad() const {
    return Pd;
  }
  double getQdLoad() const {
    return Qd;
  }
  double getGs() const {
    return Gs;
  }
  double getBs() const {
    return Bs;
  }
  double getType() const {
    return type;
  }
  double getVm() const{
    return Vm;
  }
  double getVa() const{
    return Va;
  }
  void setVm(double n){
    Vm = n;
  }
  void setVa(double n){
    Va = n;
  }
  double getVmax(){
    return Vmax;
  }
  double getVmin(){
    return Vmin;
  }
  void setPd(double n){
    Pd = n;
  }
  void setQd(double n){
    Qd = n;
  }
 private:
  string name;
  int bus_i;
  int type;
  double Pd;
  double Qd;
  double Gs;
  double Bs;
  int area;
  double Vm;
  double Va;
  double baseKV;
  int zone;
  double Vmax;
  double Vmin;
  bool isLoad;
};


#endif
