#include <iostream>
#include <list>
#include <sstream>
#include <fstream>
#include "Solver.h"
#include "Network.h"
#include "WritePDDLProblem.h"
#include "Comparator.h"
#include "MatlabSolverOPF.h"
#include "NewtonRaphson.h"
using namespace std;


void solvePowerFlowEquations(string inputFile){

  Network theNetwork(inputFile);
  cout << theNetwork << endl;
  
  Solver *solver = new NewtonRaphson(&theNetwork);
  theNetwork.setSolver(solver);
  theNetwork.solvePowerFlow(true);
}


void compareZeroThree(string inputFile){

  Network theNetwork(inputFile);
  Comparator compare(theNetwork, 0, 3);
  bool printGraph = true;       
  compare.compare(0,printGraph);
  compare.compare(1,printGraph);
  compare.compare(2,printGraph);
  compare.compare(3,printGraph);
}

void testChangeLoad(string inputFile){

  Network theNetwork(inputFile);
  map<string, pair<double, double> > nameLoad;
  string load = "load0";
  nameLoad[load] = make_pair(0,0);
  load = "load1";
  nameLoad[load] = make_pair(0,0);
  theNetwork.changeLoadParameters(nameLoad);
  theNetwork.solvePowerFlow(0,true);
}

void writeProblemFile(string inputFile){

  Network theNetwork(inputFile);
  theNetwork.printNetworkAsMatlabFile(cout);
  theNetwork.changeLoadsFromFile("/home/chiara/Code/Network/TestCases/input/Demand/Divided/2012/01-Jan-12.txt", "/home/chiara/Code/Network/TestCases/output/case14",1.);
  WritePDDLProblem problemFile(theNetwork, "/home/chiara/Code/Network/TestCases/input/Demand/Divided/2012/01-Jan-12.txt", "/home/chiara/Code/Network/TestCases/problem2.pddl");
  problemFile.writePDDLProblemInFile();
}


void findPosition(int number, int base, int*&result, int nMax){

  int i=0;
  bool good=true;
  while(number>0){
    result[i]=number%base;
    number=number/base;
    i++;
    if(i>nMax){
      cerr << "number to big"<<endl;
      good = false;
    }
  }
  for(;i<nMax;i++){
    result[i]=0;
  }
}
void findBestTapSetting(string inputFile, double scaling){
  string name ="TestCases/test";
  Network theNetwork(inputFile);
  Network backup(inputFile);
  Solver *solver;
  solver = new MatlabSolverOPF(&theNetwork);
  theNetwork.setSolver(solver);
  theNetwork.changeLoads(scaling);
  //  theNetwork->printNetworkAsMatlabFile(cout);
  //theNetwork->solvePowerFlow(false);
  //  cout << theNetwork->isOptimal() << endl;
  backup.changeLoads(scaling);
  map<string, int> tap = theNetwork.getTaps();
  map<int, Branch*> branches = theNetwork.getBranches();
  cout << tap.size() << " transfomers found " << endl;
  map<string, int>::iterator tapIt = tap.begin();
  map<string, int>::iterator tapEnd = tap.end();
  map<string,pair<double, double> > newtap;
  map<string, double> oldtap;
  for(;tapIt!=tapEnd;++tapIt){
    string name = tapIt->first;
    int id = tapIt->second;
    Branch* branch = branches[id];
    double tapRatio = (-1+branch->getRatio())*100;
    cout << name << " has " << tapRatio <<endl;
    newtap[name]=make_pair((-1+branch->getRatio())*100,0);
    oldtap[name]=(-1+branch->getRatio())*100;
  }
  // theNetwork.solvePowerFlow(true);
  // double obj = theNetwork.getObjectiveValue();
  // cout << theNetwork.isOptimal() << " " << obj <<  endl;


  int n = tap.size();
  int nPos = 10;
  int total = pow(nPos,n);
  double min = 99999999.;
  list<map<string, pair<double, double> > > results;
  for(int i=0;i<total;i++){
    int *result = new int[n];
    findPosition(i, nPos,result,n);
    if(i%100==0){
      cout << i << "/" << total<<endl;
    }
    map<string, pair<double, double> >::iterator ntIt=newtap.begin();
    map<string, double>::iterator otIt=oldtap.begin();
    map<string, pair<double, double> >::iterator ntEnd=newtap.end();
    cout << i << " ";
    for(int j=0;ntIt!=ntEnd;++ntIt,++j,++otIt){
      ntIt->second.first=otIt->second+nPos/2-result[j];
      cout <<  ntIt->first << " " << otIt->second+nPos/2-result[j] << "; ";
    }
    cout << " ";
    
    //theNetwork.copyNetwork(backup);
    theNetwork.changeTransformerParameters(newtap);
    // cout<<"---- backup ----" << endl;
    //backup.printNetworkAsMatlabFile(cout);
    //cout << "---- theNetwork ----"<<endl;
    
    /*
    stringstream  oName;
    oName<<name<<i<<".m";
    ofstream oFile(oName.str().c_str(), std::ofstream::out);
    theNetwork.printNetworkAsMatlabFile(oFile);
    cout << oName.str() << endl;
    oFile.close();
    */

    theNetwork.solvePowerFlow(false);
    double obj = theNetwork.getObjectiveValue();
    cout << theNetwork.isOptimal() << " " << obj <<  endl;
    if(obj <= min && theNetwork.isOptimal()){
      if(obj<min){
    	min = obj;
    	results.clear();
      }
      //if(theNetwork.isOptimal()){
      results.push_back(newtap);
    }
    delete []result;
  }
  cout << "min objective value for scaling " << scaling << " is " <<  min;
  cout << " obtained with " << endl;
  list<map<string, pair<double, double> > >::iterator resIt = results.begin();
  list<map<string, pair<double, double> > >::iterator resEnd = results.end();
  for(;resIt!=resEnd;++resIt){
    map<string, pair<double, double> >::iterator tapIt = resIt->begin();
    map<string, pair<double, double> >::iterator tapEnd = resIt->end();
    cout << "\t*";
    for(;tapIt!=tapEnd;++tapIt){
      cout << tapIt->first << " : " << tapIt->second.first << "; ";
    }
    cout << endl;
    //theNetwork.changeTransformerParameters(*resIt);
    //theNetwork.printNetworkAsMatlabFile(cout);
  }
  //theNetwork->printNetworkAsMatlabFile(cout);
  delete solver;
}
void runOPFMatlab(string inputFile){

  Network theNetwork(inputFile);
  theNetwork.changeLoads(1.95);
  Solver *solver;
  solver = new MatlabSolverOPF(&theNetwork);
  theNetwork.setSolver(solver);
  theNetwork.printNetworkAsMatlabFile(cout);
  theNetwork.solvePowerFlow(true);
  cout << theNetwork.isOptimal() << endl;
}

int main(int argc, char** argv){
  if(argc<2){
    cerr << "Usage : \n";
    cerr << "\t" << argv[0] << " matlabFile [what to do]\n";
    cerr << "\t0 : power flow equation with newton raphson method\n";
    cerr << "\t1 : compare results newton raphson and matlab\n";
    cerr << "\t2 : test change loads parameters\n";
    cerr << "\t3 : test print problem Files\n";
    cerr << "\t4 : find best tap settings\n";
    cerr << "\t5 : test MatlabSolverOPF\n";
    return 1;
  }else{
    cout << "Welcome in " << argv[0] << endl;
  }
  string inputFile = argv[1];
  int wtd=0;
  if (argc!=2){
    wtd = atoi(argv[2]);
  }
  
  if(wtd==0){
    solvePowerFlowEquations(inputFile);
  }else if(wtd==1){
    compareZeroThree(inputFile);
  }else if(wtd==2){
    testChangeLoad(inputFile);
  }else if(wtd==3){
    writeProblemFile(inputFile);
  }else if(wtd==4){
    double scaling;
    if (argc<4){
      scaling = 0;      
    }else{
      scaling = atof(argv[3]);
    }
    findBestTapSetting(inputFile, scaling);
  }else if(wtd==5){
    runOPFMatlab(inputFile);
  }
}
