/************************************************************************
 * Copyright 2009, 2011, Strathclyde Planning Group,
 * Department of Computer and Information Sciences,
 * University of Strathclyde, Glasgow, UK
 * http://planning.cis.strath.ac.uk/
 *
 * Amanda Coles, Andrew Coles, Maria Fox, Derek Long - POPF
 * Maria Fox, Richard Howey and Derek Long - VAL
 * Stephen Cresswell - PDDL Parser
 *
 * This file is part of POPF.
 *
 * POPF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * POPF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with POPF.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

#ifndef __RPGBUILDER
#define __RPGBUILDER

#include <vector>
#include <list>
#include <set>
#include <map>
#include <limits>

using std::vector;
using std::list;
using std::set;
using std::map;

#include "globals.h"

#include "minimalstate.h"

#include "instantiation.h"

#include "ptree.h"

#include <assert.h>
#include <values.h>
#include <math.h>
#include "IPSA.h"
#include "FFEvent.h"

using namespace Inst;

namespace Planner
{

class RPGHeuristic;
class StartEvent;

struct EpsilonComp {

    bool operator()(const double & a, const double & b) const {
        if (fabs(b - a) < 0.00005) return false;
        return (a < b);
    }

};

class MinimalState;


struct ActionFluentModification {

    int act;
    VAL::time_spec ts;
    bool openEnd;
    double change;
    int howManyTimes;
    bool assignment;

    ActionFluentModification(const int & a, const VAL::time_spec & startOrEnd, const bool & oe, const double & incr, const int & shots, const bool & ass)
            : act(a), ts(startOrEnd), openEnd(oe), change(incr), howManyTimes(shots), assignment(ass) {};

};

struct InvData;
class TemporalAnalysis;

class RPGBuilder
{
    friend class TemporalAnalysis;
    friend class NumericAnalysis;

public:
    enum math_op {
        NE_ADD, NE_SUBTRACT, NE_MULTIPLY, NE_DIVIDE, NE_CONSTANT, NE_FLUENT, NE_VIOLATION
        #ifdef STOCHASTICDURATIONS
        , NE_STOCHASTIC_DURATION_TERM
        #endif
    };

    enum op_type {
        OT_NORMAL_ACTION=0, OT_INVALID_ACTION=1, OT_PROCESS=2
    };


    /** Description of the pointless effect an action has on a fact. */
    enum pointless_effect { PE_ADDED=1, PE_DELETED=2, PE_DELETED_THEN_ADDED=3 };

    static bool doSkipAnalysis;
    static bool forceMakespanMetric;

    struct Operand {
        math_op numericOp;
        int fluentValue;
        double constantValue;
        string isviolated;
        #ifdef STOCHASTICDURATIONS
        PNE* durationVar;
        Operand(PNE * p) : numericOp(NE_STOCHASTIC_DURATION_TERM), fluentValue(-1), constantValue(NAN), durationVar(p) {};
        #endif
        Operand(const math_op & o) : numericOp(o), fluentValue(-1), constantValue(NAN) {};
        Operand(const int & f) : numericOp(NE_FLUENT), fluentValue(f), constantValue(NAN) {};
        Operand(const double & c) : numericOp(NE_CONSTANT), fluentValue(-1), constantValue(c) {};
        Operand(const string & s) : numericOp(NE_VIOLATION), fluentValue(-1), constantValue(NAN), isviolated(s) {};
    };


    static double calculateRHS(const list<Operand> & formula, vector<double> & fluents);
    static pair<double, bool> constRHS(const list<Operand> & formula);

    class NumericEffect
    {

    public:

        int fluentIndex;
        VAL::assign_op op; // how to update given fluent index (add to it, assign to it, etc.)
        list<Operand> formula; // formula in postfix notation
        NumericEffect(const VAL::assign_op & opIn, const int & fIn, VAL::expression * formulaIn, VAL::FastEnvironment * f, VAL::TypeChecker * t = 0);
        double applyEffect(vector<double> & fluents) const;
        void display(ostream & o) const;
        double evaluate(const double & minDur, const double & maxDur) const {
            double toReturn = DBL_MAX;
            return toReturn;
        };
    };

    class NumericPrecondition
    {

    public:
        VAL::comparison_op op; // how to compare given fluent index to calculated RHS
        list<Operand> LHSformula; // formula for LHS in postfix notation
        list<Operand> RHSformula; // formula for RHS postfix notation
        bool valid;
        bool polarity; // false = it needs to be negated
        NumericPrecondition(const VAL::comparison_op & opIn, VAL::expression * LHSformulaIn, VAL::expression * RHSformulaIn, VAL::FastEnvironment * f, VAL::TypeChecker * t = 0, const bool polarity=true);
        bool isSatisfied(vector<double> & fluents) const;
        void display(ostream & o) const;

        double evaluateRHS(vector<double> & fluentTable) const;
        pair<double, bool> constRHS() const;

    };

    class ArtificialVariable
    {

    public:
        int ID;
        int size;
        vector<double> weights;
        vector<int> fluents;
        double constant;
        int actID; /// it only matters if the precondition has a control parameter. This is because control params are local to the actions it is defined.
        /// if actID == -3, there is no control param preconditions in this action.
        double maxNeed;
        ArtificialVariable() : ID(-1), size(0), constant(0.0), maxNeed(-DBL_MAX), actID(-1) {};
        ArtificialVariable(const int & id, const int & s, const vector<double> & w, const vector<int> & f, const double & d, const double & maxIn, const int & aid) : ID(id), size(s), weights(w), fluents(f), constant(d), maxNeed(maxIn), actID(aid) {};

        double evaluate(const vector<double> & fluentTable) { ///max value
            double toReturn = constant;

            for (int i = 0; i < size; ++i) {
                if (fluents[i] == -5) {
                    if (current_analysis->the_problem->metric->opt.front() == VAL::E_MINIMIZE) {
                        toReturn -= weights[i] * Planner::Globals::bestSolutionQuality;
                    } else {
                        toReturn += weights[i] * Planner::Globals::bestSolutionQuality;
                    }
                    continue;
                } else if (fluents[i] == -21) {
                    if (current_analysis->the_problem->metric->opt.front() == VAL::E_MINIMIZE) {
                        toReturn += weights[i] * Planner::Globals::bestSolutionQuality;
                    } else {
                        toReturn -= weights[i] * Planner::Globals::bestSolutionQuality;
                    }
                    continue;
                } else if (fluents[i] <= -100 && fluents[i] > -116) {
                    list<pair<int,int> >::iterator cItr = RPGBuilder::actionsToArtificialVariables.begin();
                    const list<pair<int,int> >::iterator cEnd = RPGBuilder::actionsToArtificialVariables.end();
                        for(; cItr != cEnd; cItr++){
                            if(ID == cItr->first){
                            toReturn += weights[i] * RPGBuilder::getOpMaxControl(cItr->second)[-fluents[i]-100]; ///plus max is more
                    }
                }

            //cout << "ID : " << ID << " weights[i] :: " << weights[i] << " fluents[i] :: " << fluents[i] <<" toReturn :: " << toReturn << endl;

                    continue;
                } else if (fluents[i] <= -116){
                    list<pair<int,int> >::iterator cItr = RPGBuilder::actionsToArtificialVariables.begin();
                    const list<pair<int,int> >::iterator cEnd = RPGBuilder::actionsToArtificialVariables.end();
                for(; cItr != cEnd; cItr++){
                    if(ID == cItr->first){
                            toReturn -= weights[i] * RPGBuilder::getOpMinControl(cItr->second)[-fluents[i]-116]; ///minus min is more..
                    }
                }
                    continue;
                }
                if (fluents[i] < 0) {
                    return std::numeric_limits<double>::signaling_NaN();
                }
                toReturn += weights[i] * fluentTable[fluents[i]];
              //  cout << "weights[i] :: " << weights[i] << " fluents[i] :: " << fluents[i] <<" toReturn :: " << toReturn << endl;
            }
            return toReturn;
        };

        double evaluateGradient(const vector<double> & fluentTable) {
            double toReturn = 0.0;
            for (int i = 0; i < size; ++i) {
                if (fluents[i] == -5 || fluents[i] == -21) {
                    continue;
                }
                if (fluents[i] < 0) {
                    return std::numeric_limits<double>::signaling_NaN();
                }
                toReturn += weights[i] * fluentTable[fluents[i]];
            }
            return toReturn;
        };

        double evaluateWCalculate(const vector<double> & fluentTable, const int & pneCount);
        double evaluateWCalculate(const vector<double> & minFluentTable, const vector<double> & maxFluentTable, const int & pneCount, const VAL::comparison_op & op) const;

        void evaluateCPBounds(const vector<double> & fluentTable) {
        ///  this only helps to roughly estimate the true bounds of cp in the first state visited. (where LP is not called yet.)
        double upperLimit = constant;

            for (int i = 0; i < size; ++i) {

                if (fluents[i] >= 0) {
                    upperLimit += weights[i] * fluentTable[fluents[i]];
                    continue;
                } else if (fluents[i] < 0) {
                    continue;
                } else if (fluents[i] == std::numeric_limits<double>::signaling_NaN()) {
                    continue;
                }
            };

            for (int i2 = 0; i2 < size; ++i2) {

                if (fluents[i2] <= -116 && actID >= 0) {
                    int cpID = -fluents[i2]-116;

                    if (RPGBuilder::getOpMaxControl(actID)[cpID] > upperLimit/weights[i2] ) {
                        RPGBuilder::setOpMaxControl(actID,cpID, upperLimit/weights[i2]);
                    }

                continue;
                }

                if (fluents[i2] < 0) {
                    continue;
                } else if (fluents[i2] == std::numeric_limits<double>::signaling_NaN()) {
                    continue;
                }

            }
        };

        bool operator <(const ArtificialVariable & v) const;
        void display(ostream & o) const;

        void updateMax(const double & m) {
            if (m > maxNeed) maxNeed = m;
        }
    };

    class RPGNumericPrecondition
    {

    public:
        int ID;

        int LHSVariable;
        double LHSConstant;

        VAL::comparison_op op;

        int RHSVariable;
        double RHSConstant;

        int actID; /// it only matters if the precondition has a control parameter. This is because control params are local to the actions it is defined.
        /// if actID == -3, there is no control param preconditions in this action.
        RPGNumericPrecondition() : actID(-1), ID(-1), LHSVariable(-1), LHSConstant(0.0), op(VAL::E_GREATEQ), RHSVariable(-1), RHSConstant(0.0) {};
        RPGNumericPrecondition(const int & i, const int & lv, const double & lw, const VAL::comparison_op & o, const double & rw, const int & aid)
            : ID(i), LHSVariable(lv), LHSConstant(lw), op(o), RHSVariable(-1), RHSConstant(rw),  actID(aid) {

                assert(op == VAL::E_GREATER || op == VAL::E_GREATEQ);
        };

        bool isSatisfied(const vector<double> & maxFluents) const {
            if (LHSVariable < 0) return false;

            const double lv = maxFluents[LHSVariable];
            if (lv == std::numeric_limits<double>::signaling_NaN()) return false;

            if (op == VAL::E_GREATER) {
                return (lv > RHSConstant);
            } else {
                return (lv >= RHSConstant);
            }
        };

        bool isSatisfiedWCalculate(const vector<double> & maxFluents) const;
        bool isSatisfiedWCalculate(const vector<double> & minFluents, const vector<double> & maxFluents) const;
	double howMuchNotSatisfiedWCalculate(const vector<double> & minFluents, const vector<double> & maxFluents) const;
        bool operator <(const RPGNumericPrecondition & r) const;

        void display(ostream & o) const;

    };

    class RPGNumericEffect
    {
    public:
        int ID;

        int fluentIndex;

        bool isAssignment; // if it's an assignmentOp - assume it's a += otherwise
        bool isDecreaseEffect;

        vector<double> weights;
        vector<int> variables;

        double constant;

        int size;

        RPGNumericEffect() : ID(-1), fluentIndex(-1), isAssignment(false), isDecreaseEffect(false) ,constant(NAN), size(-1) {}; //initial

        RPGNumericEffect(const int & idIn, const int & fluent, const bool & ass, const bool & decEff,
                         const vector<double> & weightsIn, const vector<int> & vars, const int & s,
                         const double & con) :
                ID(idIn), fluentIndex(fluent), isAssignment(ass), isDecreaseEffect(decEff),
                weights(weightsIn), variables(vars), constant(con), size(s) {};
            ///this does not include the fluentIndex!!!!
        double evaluate(const vector<double> & maxFluents, const double & minDur, const double & maxDur, const vector<double> & minControl, const vector<double> & maxControl, const double & contribute) const {

            double toReturn = constant;

            bool finiteContribution = false;
            if (contribute != -1.0) {
                finiteContribution = true;
            }
            assert(contribute != -2.0);

            for (int i = 0; i < size; ++i) {
                if (variables[i] >= 0) {
                    const double val = maxFluents[variables[i]];
                    if (val == DBL_MAX) return DBL_MAX;
                    if (val == -DBL_MAX) return -DBL_MAX;
                    toReturn += weights[i] * val;
                } else if (variables[i] == -3) {
                    double lw = weights[i];
                    double toUse = maxDur;
                    if (lw < 0) {
                        lw *= -1.0;
                        toUse = -minDur;
                    }
                    if (toUse == DBL_MAX) return DBL_MAX;
                    if (toUse == -DBL_MAX) return -DBL_MAX;
                    toReturn += lw * toUse;
                } else if (variables[i] == -19) {
                    double lw = weights[i];
                    double toUse = (minDur == 0.0 ? 0.0 : -minDur);
                    #ifndef NDEBUG
                    if (lw < 0) {
                        static bool printedMessage = false;
                        if (printedMessage == false) {
                            std::cerr << "Warning: negative weight attached to negative duration term - is this intentional?\n";
                            printedMessage = true;
                        }
                    }
                    #endif
                    if (lw < 0) {
                        lw *= -1.0;
                        toUse = maxDur;
                    }
                    if (toUse == DBL_MAX) return DBL_MAX;
                    if (toUse == -DBL_MAX) return -DBL_MAX;
                    toReturn += lw * toUse;
                } else if (variables[i] <= -100 && variables[i] > -116) { // +?controlparameter
                double lw = weights[i];
                    //double toUse = maxControl[(-variables[i] - 100)];
                    double toUse = (finiteContribution ? contribute : maxControl[(-variables[i] - 100)]);
                    if (lw < 0) {
                        lw *= -1.0;
                        toUse = -minControl[(-variables[i] - 100)];

                        if (finiteContribution) { ///if there is some contribution
                            if (contribute < minControl[(-variables[i] - 100)]) { /// and this contribution is smaller than the smallest value a cp can take
                                toUse = -contribute; /// use the contribution. not mincontrol...
                            }
                        }

                    }
                    if (toUse == DBL_MAX) return DBL_MAX;
                    if (toUse == -DBL_MAX) return -DBL_MAX;
                    toReturn += lw * toUse;
                } else if (variables[i] <= -116) {  // - ?controlparameter
                    double lw = weights[i];
                    double toUse = (minControl[(-variables[i] - 116)] == 0.0 ? 0.0 : -minControl[(-variables[i] - 116)]);
                    if (finiteContribution) { ///if there is some contribution
                        if (contribute < minControl[(-variables[i] - 116)]) { /// and this contribution is smaller than the smallest value a cp can take
                            toUse = -contribute; /// use the contribution. not mincontrol...
                        }
                    }

                    if (lw < 0) {
                        lw *= -1.0;
                    }
                    if (toUse == DBL_MAX) return DBL_MAX;
                    if (toUse == -DBL_MAX) return -DBL_MAX;
                    toReturn += lw * toUse;
                } else {
                    std::cerr << "Error: asked to evaluate formula containing term with fluent ID " << variables[i] << ", which doesn't represent either a non-static fluent or the duration term -3 or -19\n";
                    assert(false);
                }

            } // cout << "okkes evaluate toReturn: " << toReturn << endl;
            return toReturn;
        };

        double evaluateMin(const vector<double> & maxFluents, const double & minDur, const double & maxDur, const vector<double> & minControl, const vector<double> & maxControl, const double & contribute) const {
            static const int pneCount = RPGBuilder::getPNECount();
            double toReturn = constant;

            bool finiteContribution = false;
            if (contribute != -1.0) {
                finiteContribution = true;
            }

            for (int i = 0; i < size; ++i) {
                if (variables[i] >= pneCount) {
                    const double val = maxFluents[variables[i] - pneCount];
                    if (val == DBL_MAX) return -DBL_MAX;
                    if (val == -DBL_MAX) return DBL_MAX;
                    toReturn -= weights[i] * val;
                } else if (variables[i] >= 0) {
                    const double val = maxFluents[variables[i] + pneCount];
                    if (val == DBL_MAX) return -DBL_MAX;
                    if (val == -DBL_MAX) return DBL_MAX;
                    toReturn -= weights[i] * val;
                } else if (variables[i] == -3) {
                    double lw = weights[i];
                    double toUse = minDur;
                    if (lw < 0) {
                        lw *= -1.0;
                        toUse = -maxDur;
                    }
                    if (toUse == DBL_MAX) return -DBL_MAX;
                    if (toUse == -DBL_MAX) return DBL_MAX;
                    toReturn -= lw * toUse;
                } else if (variables[i] == -19) {
                    double lw = weights[i];
                    double toUse = -maxDur;
                    #ifndef NDEBUG
                    if (lw < 0) {
                        static bool printedMessage = false;
                        if (printedMessage == false) {
                            std::cerr << "Warning: negative weight attached to negative duration term - is this intentional?\n";
                            printedMessage = true;
                        }
                    }
                    #endif
                    if (lw < 0) {
                        lw *= -1.0;
                        toUse = -minDur;
                    }
                    if (toUse == DBL_MAX) return -DBL_MAX;
                    if (toUse == -DBL_MAX) return DBL_MAX;
                    toReturn -= lw * toUse;
                } else if (variables[i] <= -100 && variables[i] > -116) {

                    double lw = weights[i];
                    double toUse = -minControl[(-variables[i] - 100)];

                    if (finiteContribution) {   ///if there is some contribution
                        if (contribute < minControl[(-variables[i] - 100)]) { /// and this contribution is smaller than the smallest value a cp can take
                            toUse = -contribute; /// use the contribution. not mincontrol...
                        }
                    }

                    if (lw < 0) {
                        lw *= -1.0;
                        //toUse = -maxControl[(-variables[i] - 100)];
                        toUse = (finiteContribution ? -contribute : -maxControl[(-variables[i] - 100)]);

                    }
                    if (toUse == DBL_MAX) return -DBL_MAX;
                    if (toUse == -DBL_MAX) return DBL_MAX;
                    toReturn -= lw * toUse;
                } else if (variables[i] <= -116) {  // - ?controlparameter
                    double lw = weights[i];
                    //double toUse = maxControl[(-variables[i] - 116)]; ///don't change this. the minimum effect is achieved with maximum decrease!
                    double toUse = (finiteContribution ? contribute : maxControl[(-variables[i] - 116)]);

                    if (lw < 0) {
                        lw *= -1.0; }
                    if (toUse == DBL_MAX) return -DBL_MAX;
                    if (toUse == -DBL_MAX) return DBL_MAX;
                    toReturn -= lw * toUse;
                } else {
                    std::cerr << "Error: asked to evaluate formula containing term with fluent ID " << variables[i] << ", which doesn't represent either a non-static fluent or the duration term -3 or -19\n";
                    assert(false);
                }
            }

            return toReturn;
        };
    pair<double, double> applyEffectMinMax(MinimalState theState, MinimalState &nextState);
	pair<double, double> applyEffectMinMaxWithExternalEvaluation(MinimalState theState, MinimalState &nextState);

	pair<double, double> applyEffectMinMax(const vector<double> & minFluents, const vector<double> & maxFluents, const double & minDur, const double & maxDur, const vector<double> & minControl, const vector<double> & maxControl);
	double callExternalSolver ( MinimalState & theState);
	pair<double, double>applyIPSAEffect (MinimalState & theState, const int i, string fluentName);

	pair<double, double> applyEffectMinMaxWithExternalEvaluation(const vector<double> & minFluents, const vector<double> & maxFluents, const double & minDur, const double & maxDur, MinimalState & theState, const int i);

        bool operator <(const RPGNumericEffect & e) const;

        void display(ostream & o) const;

    };

    /** @brief A class defining a single duration constraint, in LNF. */
    class DurationExpr
    {

    public:
        /** @brief The weights of the variables in the duration expression */
        vector<double> weights;

        #ifdef STOCHASTICDURATIONS
        /** @brief The IDs of the variables in the duration expression.
         *
         *  The first of each pair, if not equal to -1, is an index into the state vector of variables.
         *  Otherwise, the second entry is used.
         */
        vector<pair<int, PNE*> > variables;
        #else
        /** @brief The IDs of the variables in the duration expression.  @see RPGBuilder::getPNE */
        vector<int> variables;
        #endif

        /** The comparison operator for this duration expression.  Is one of:
         *  - <code>VAL::E_GREATEQ</code> for <code>(>= ?duration ...)</code>
         *  - <code>VAL::E_GREATER</code> for <code>(> ?duration ...)</code>
         *  - <code>VAL::E_EQUALS</code> for <code>(= ?duration ...)</code>
         *  - <code>VAL::E_LESS</code> for <code>(< ?duration ...)</code>
         *  - <code>VAL::E_LESSEQ</code> for <code>(<= ?duration ...)</code>
         */
        VAL::comparison_op op;

        /** @brief The constant term for the duration expression */
        double constant;

        DurationExpr() : weights(), variables(), constant(0.0) {};

        /** @brief Create a duration expression, with unknown operator and empty weights and variables.
         *
         * @param d  The constant term for the duration expression
         */
        DurationExpr(const double & d) : weights(0), variables(0), constant(d) {} ;


        /**
         * Evaluate the minimum possible value that satisfies this duration expression,
         * given the provided lower- and upper-bounds on the values of the task numeric
         * variables.
         *
         * @param minVars  Minimum values of the task variables
         * @param maxVars  Maximum values of the task variables
         *
         * @return  The lowest permissible duration, according to this duration expression.
         */
        double minOf(const int & a, const vector<double> & minVars, const vector<double> & maxVars);

        /**
         * Evaluate the maximum possible value that satisfies this duration expression,
         * given the provided lower- and upper-bounds on the values of the task numeric
         * variables.
         *
         * @param minVars  Minimum values of the task variables
         * @param maxVars  Maximum values of the task variables
         *
         * @return  The greatest permissible duration, according to this duration expression.
         */
        double maxOf(const int & a, const vector<double> & minVars, const vector<double> & maxVars);
    };

    /** @brief A class containing all the duration constraints imposed on an action. */
    class RPGDuration
    {

    public:
        /** The fixed durations of the action, those of the form <code>(= ?duration ...)</code> */
        list<DurationExpr *> fixed;

        /** The minimum durations of the action, those of the form <code>(>= ?duration ...)</code> or <code>(> ?duration ...)</code> */
        list<DurationExpr *> min;

        /** The maximum durations of the action, those of the form <code>(<= ?duration ...)</code> or <code>(< ?duration ...)</code> */
        list<DurationExpr *> max;

        RPGDuration(list<DurationExpr *> & eq, list<DurationExpr *> & low, list<DurationExpr *> & high)
                : fixed(eq), min(low), max(high) {};

        /**
         * Return one of the lists of durations contained in the object.
         *
         * @param i  Which list of durations to return:
         *           - 0 returns the <code>fixed</code> duration expressions;
         *           - 1 returns the <code>min</code> duration expressions;
         *           - 2 returns the <code>max</code> duration expressions.
         *
         * @return  The requested list of duration expressions.
         */
        const list<DurationExpr *> & operator[](const int & i) const {
            switch (i) {
                case 0: {
                    return fixed;
                }
                case 1: {
                    return min;
                }
                case 2: {
                    return max;
                }
            }
            std::cerr << "Fatal internal error: duration expression index should be in the range 0 to 2, inclusive\n";
            exit(1);
        }
    };
    /**
     *  Class to represent the 'action' whose application corresponds to a timed initial literal.
     */
    class FakeTILAction
    {

    public:
        /** The time-stamp of the timed initial literal */
        const double duration;

        /** The facts added at the specified time */
        list<Literal*> addEffects;

        /** The facts deleted at the specified time */
        list<Literal*> delEffects;

        /** The numeric assignment effects at the specified time */
        map<int,double> numEffects;

        list<int> rpgNumericEffects;

        /**
         *   Add the specified add and delete effects to the timed initial literal action.  Is used when
         *   multiple TILs are found at a given time-stamp.
         *
         *   @param adds  Add effects to include in this TIL action
         *   @param dels  Delete effects to include in this TIL action
         */
        void mergeIn(const LiteralSet & adds, const LiteralSet & dels, const map<int,double> & nums) {

            {
                LiteralSet::iterator lsItr = adds.begin();
                const LiteralSet::iterator lsEnd = adds.end();

                for (; lsItr != lsEnd; ++lsItr) {
                    addEffects.push_back(*lsItr);
                }
            }

            {
                LiteralSet::iterator lsItr = dels.begin();
                const LiteralSet::iterator lsEnd = dels.end();

                for (; lsItr != lsEnd; ++lsItr) {
                    delEffects.push_back(*lsItr);
                }
            }

            numEffects.insert(nums.begin(), nums.end());
        }

        /**
         *   Constructor for an action corresponding to a Timed Initial Literal.
         *
         *   @param dur  The time at which the timed initial occurs
         *   @param adds The facts added at time <code>dur</code>
         *   @param dels The facts deleted at time <code>dur</code>
         */
        FakeTILAction(const double & dur, const LiteralSet & adds, const LiteralSet & dels, const map<int,double> & nums)
                : duration(dur) {
            mergeIn(adds, dels, nums);
        }

    };

    class KShotFormula
    {

    public:
        KShotFormula() {};
        virtual int getLimit(const MinimalState & s) const = 0;
        virtual int getOptimisticLimit(const MinimalState & s) const = 0;
        virtual ~KShotFormula() {};
    };

    class UnlimitedKShotFormula : public KShotFormula
    {

    public:
        UnlimitedKShotFormula() : KShotFormula() {};
        virtual int getLimit(const MinimalState &) const {
            return INT_MAX;
        };
        virtual int getOptimisticLimit(const MinimalState &) const {
            return INT_MAX;
        };
        };

    class OneShotKShotFormula : public KShotFormula
    {

    private:
        list<int> watchedLiterals;
    public:
        OneShotKShotFormula(list<int> & toWatch) : KShotFormula(), watchedLiterals(toWatch) {};
        virtual int getLimit(const MinimalState & s) const;
        virtual int getOptimisticLimit(const MinimalState & s) const;
    };

    struct ShotCalculator {

        int variable;
        double greaterThan;
        double decreaseBy;

        ShotCalculator(const int & v, const double & g, const double & d) : variable(v), greaterThan(g), decreaseBy(d) {};
    };

    class KShotKShotFormula : public KShotFormula
    {

    private:
        list<ShotCalculator> formulae;
    public:
        KShotKShotFormula(list<ShotCalculator> & c) : KShotFormula(), formulae(c) {};
        virtual int getLimit(const MinimalState & s) const;
        virtual int getOptimisticLimit(const MinimalState & s) const;
    };

    /** A class defining the linear continuous effects of an action */
    class LinearEffects
    {

    public:
        /**
         * A single linear continuous numeric effect expression.  The gradient of the
         * effect is stored in LNF.
         */
        struct EffectExpression {
            /** The weights for the variables in the LNF expression of the gradient. */
            vector<double> weights;

            /** The variables in the LNF expression of the gradient. */
            vector<int> variables;

            /** The constant term of the LNF expression of the gradient. */
            double constant;

            /**
             * Constructor for a constant-gradient linear continuous effect
             *
             * @param g  The gradient of the effect
             */
            EffectExpression(const double & g, const vector<int> & varsIn, const vector<double> & weightsIn)
                : weights(weightsIn), variables(varsIn), constant(g) {};
        };

//      vector<double> durations;

        /** The IDs of the variables upon which this action has continuous effects.  @see RPGBuilder::getPNE */
        vector<int> vars;

        /**
         * The effects themselves. Each entry is a vector describing the effect upon the corresponding
         * variable in <code>vars</code>.  For now, at most one such vector exists, but in the future
         * this may change if support for piecewise-linear effects is added.
         */
        vector<vector<EffectExpression> > effects;

//      double totalDuration;

        /**
         *  The number of divisions for the continuous numeric effects of the action.  At present this
         *  is always 1, i.e. the gradients of the effects remain fixed across the execution of the
         *  action, but this may change if support for piecewise-linear effects is added.
         */
        int divisions;

    };

    /**A class defining a single control parameter constraint, in form of (op ?controlp constant) */
    class ControlConstraint
    {
        public:
        vector<double> weights;

        /** @brief The IDs of the variables in the control expression.  @see RPGBuilder::getPNE */
        vector<int> variables;

        /** The comparison operator for this duration expression.  Is one of:
         *  - <code>VAL::E_GREATEQ</code> for <code>(>= ?controlp ...)</code>
         *  - <code>VAL::E_GREATER</code> for <code>(> ?controlp ...)</code>
         *  - <code>VAL::E_EQUALS</code> for <code>(= ?controlp ...)</code>
         *  - <code>VAL::E_LESS</code> for <code>(< ?controlp ...)</code>
         *  - <code>VAL::E_LESSEQ</code> for <code>(<= ?controlp ...)</code>
         */
        VAL::comparison_op op;

        double constant;

        ControlConstraint() : weights(), variables(), constant(0.0) {};  //empty expression

        ControlConstraint(const double & d) : weights(), variables(), constant(d) {};

        double minOf (const vector<double> & minVars, const vector<double> & maxVars);

        double maxOf (const vector<double> & minVars, const vector<double> & maxVars);
    };
        /**A class containing ALL constraints *with the same control variable* on an action.
        * For each control parameter precondition introduced, new RPGControl is created that includes min and max values
        * of the control parameter. @see rpgControlExpressions[i][cpID] = new RPGControl(min, max),
        * where cpID is control param ID => 0 for -100th control parameter, 1 for -101th cp etc. cpID holds the same value as in getNum()
        * i is the instantiated operator index
        */
    class RPGControl
    {
         public:

        /** in the form (>= ?controlp d) or (> ?controlp d) imposed on control parameter */
        list<ControlConstraint *> min;

        /** (<= ?controlp d) or (< ?controlp d) */
        list<ControlConstraint *> max;

        RPGControl(list<ControlConstraint *> & low, list<ControlConstraint *> & high)
                : min(low), max(high) {};

        const list< ControlConstraint *> & operator[](const int & i) const {
            switch (i) {
                case 0: {
                    return min;
                }
                case 1: {
                    return max;
                }
            }
            std::cerr << "Fatal internal error: control expression index should be in the range 0 to 1, inclusive\n";
            exit(1);
        }
    };

    class Metric
    {

    public:
        bool minimise;

        list<double> weights;
        list<int> variables;

        Metric(const bool & m) : minimise(m) {};

    };

    static Metric * theMetric;
    static set<int> metricVars;

    struct Constraint {

        string name;

        VAL::constraint_sort cons;
        list<Literal*> goal;
        list<Literal*> trigger;

        list<NumericPrecondition> goalNum;
        list<NumericPrecondition> triggerNum;

        list<int> goalRPGNum;
        list<int> triggerRPGNum;

        double deadline;
        double from;

        double cost;
        bool neverTrue;

        Constraint() : deadline(0.0), from(0.0), cost(0.0), neverTrue(false) {};
        Constraint(const string & n) : name(n), deadline(0.0), from(0.0), cost(0.0), neverTrue(false) {};

    };

    /**
     *  Class to represent a conditional effect.  The class is capable of storing more fields than can
     *  currently be used by the planner - currently, only conditional effects upon metric tracking
     *  variables are supported, conditional on either propositions controlled exclusively by
     *  timed initial literals, or on numeric values.
     */
    class ConditionalEffect
    {

    private:

        /**
         * Propositional preconditions on the conditional effect.  Each entry is pair: the first
         * entry denotes the fact that must hold, the second denotes the time at which it must
         * hold (either <code>VAL::E_AT_START</code>, <code>VAL::E_OVER_ALL</code> or <code>VAL::E_AT_END</code>.
         * For now, the literal must be controlled exclusively by timed initial literals.
         */
        list<pair<Literal*, VAL::time_spec> > propositionalConditions;

        /**
         * Numeric preconditions on the conditional effect.  Each entry is pair: the first
         * entry denotes an entry in <code>RPGBuilder::rpgNumericPreconditions</code>; the second denotes the time at which it must
         * hold (either <code>VAL::E_AT_START</code>, <code>VAL::E_OVER_ALL</code> or <code>VAL::E_AT_END</code>.
         */
        list<pair<int, VAL::time_spec> > numericPreconditions;

        /**
         * Conditional numeric effects.  For now, these must act exclusively on metric tracking variables.
         * Each entry is pair: the first is an index in <code>RPGBuilder::rpgNumericEffects</code>; the second denotes the time at which
         * it occurs (either <code>VAL::E_AT_START</code> or <code>VAL::E_AT_END</code>).
         */
        list<pair<int, VAL::time_spec> > numericEffects;

        /** Conditional propositional add effects.  For now, must be empty. */
        list<pair<Literal*, VAL::time_spec> > propositionalAddEffects;
        /** Conditional propositional delete effects.  For now, must be empty. */
        list<pair<Literal*, VAL::time_spec> > propositionalDeleteEffects;

    public:
        ConditionalEffect() {
        }

        void addCondition(Literal * const l, const VAL::time_spec & t, const int &
#ifndef NDEBUG
                                                                                   actID
#endif
        ) {
            assert(t == VAL::E_AT_START || RPGBuilder::actionIsDurative(actID));
            propositionalConditions.push_back(make_pair(l, t));
        }

        void addCondition(const int & p, const VAL::time_spec & t, const int &
#ifndef NDEBUG
                                                                               actID
#endif
        ) {
            assert(t == VAL::E_AT_START || RPGBuilder::actionIsDurative(actID));
            numericPreconditions.push_back(make_pair(p, t));
        }

        void addNumericEffect(const int & p, const VAL::time_spec & t, const int &
#ifndef NDEBUG
                                                                                   actID
#endif
        ) {
            assert(t == VAL::E_AT_START || RPGBuilder::actionIsDurative(actID));
            numericEffects.push_back(make_pair(p, t));
        }

        void addAddEffect(Literal * const l, const VAL::time_spec & t) {
            propositionalAddEffects.push_back(make_pair(l, t));
        }

        void addDeleteEffect(Literal * const l, const VAL::time_spec & t) {
            propositionalDeleteEffects.push_back(make_pair(l, t));
        }

        const list<pair<Literal*, VAL::time_spec> > & getPropositionalConditions() const {
            return propositionalConditions;
        }

        const list<pair<int, VAL::time_spec> > & getNumericPreconditions() const {
            return numericPreconditions;
        }

        list<pair<int, VAL::time_spec> > & getEditableNumericEffects() {
            return numericEffects;
        }

        const list<pair<int, VAL::time_spec> > & getNumericEffects() const {
            return numericEffects;
        }

        const list<pair<Literal*, VAL::time_spec> > & getPropositionalAddEffects() const {
            return propositionalAddEffects;
        }

        const list<pair<Literal*, VAL::time_spec> > & getPropositionalDeleteEffects() const {
            return propositionalDeleteEffects;
        }


    };

    class NoDuplicatePair
    {

    protected:

        list<Literal*> * first;
        LiteralSet * second;

    public:

        NoDuplicatePair()
                : first((list<Literal*>*)0), second((LiteralSet*)0) {
        }

        NoDuplicatePair(list<Literal*> * const listIn, LiteralSet* const setIn)
                : first(listIn), second(setIn) {
        }

        void push_back(Literal* const toAdd) {
            if (second->insert(toAdd).second) {
                first->push_back(toAdd);
            }
        }

        Literal* back() const {
            return first->back();
        }

        bool operator!() const {
            return (!first);
        }

        template<typename T>
        void insert(T itr, const T & itrEnd) {
            for (; itr != itrEnd; ++itr) {
                push_back(*itr);
            }
        };
    };

    class ProtoConditionalEffect
    {

    public:

        list<Literal*> startPrec;
        LiteralSet startPrecSet;
        list<Literal*> inv;
        LiteralSet invSet;
        list<Literal*> endPrec;
        LiteralSet endPrecSet;

        list<Literal*> startNegPrec;
        LiteralSet startNegPrecSet;
        list<Literal*> negInv;
        LiteralSet negInvSet;
        list<Literal*> endNegPrec;
        LiteralSet endNegPrecSet;


        list<RPGBuilder::NumericPrecondition> startPrecNumeric;
        list<RPGBuilder::NumericPrecondition> invNumeric;
        list<RPGBuilder::NumericPrecondition> endPrecNumeric;

        list<Literal*> startAddEff;
        LiteralSet startAddEffSet;
        list<Literal*> startDelEff;
        LiteralSet startDelEffSet;
        list<RPGBuilder::NumericEffect> startNumericEff;

        list<Literal*> endAddEff;
        LiteralSet endAddEffSet;
        list<Literal*> endDelEff;
        LiteralSet endDelEffSet;
        list<RPGBuilder::NumericEffect> endNumericEff;

        set<int> startNumericEffsOnVar;
        set<int> endNumericEffsOnVar;
    };

    struct Guarded {
        set<int> guardedConditionVars;
        set<int> guardedEffectVars;
    };

protected:

    static bool RPGdebug;
    static bool problemIsNotTemporal;

    static vector<list<pair<int, VAL::time_spec> > > preconditionsToActions;
    static vector<list<pair<int, VAL::time_spec> > > negativePreconditionsToActions;

    static list<pair<int, VAL::time_spec> > preconditionlessActions;
    static list<pair<int, VAL::time_spec> > onlyNumericPreconditionActions;

    static vector<list<Literal*> > actionsToStartPreconditions;
    static vector<list<Literal*> > actionsToInvariants;
    static vector<list<Literal*> > actionsToEndPreconditions;

    static vector<list<Literal*> > actionsToStartNegativePreconditions;
    static vector<list<Literal*> > actionsToNegativeInvariants;
    static vector<list<Literal*> > actionsToEndNegativePreconditions;

    static vector<LiteralSet> actionsToEndOneShots;

    static vector<list<Literal*> > actionsToStartEffects;
    static vector<list<Literal*> > actionsToStartNegativeEffects;
    static vector<list<Literal*> > actionsToEndEffects;
    static vector<list<Literal*> > actionsToEndNegativeEffects;

    static vector<list<pair<int, VAL::time_spec> > > effectsToActions;
    static vector<list<pair<int, VAL::time_spec> > > negativeEffectsToActions;

    static vector<double> actionsToMinDurations;
    static vector<double> actionsToMaxDurations;
    static vector<list<NumericPrecondition*> > fixedDurationExpressions;
    static vector<list<NumericPrecondition*> > minDurationExpressions;
    static vector<list<NumericPrecondition*> > maxDurationExpressions;

    // one for each instantiated action; within that - one for each point along the action, minus the last
    // entry [a][0] specifies duration between a0 and a1
    static vector<vector<RPGDuration*> > rpgDurationExpressions;
    static vector<double> nonTemporalDuration;

    static vector<vector<double> > actionsToMinControls;
    static vector<vector<double> > actionsToMaxControls;
    static vector<vector<double> > rpgActionsToMinControls;
    static vector<vector<double> > rpgActionsToMaxControls;
    static vector<list<pair<int, NumericPrecondition*> > > minControlExpressions; // this includes all control parameters mixed.
    static vector<list<pair<int, NumericPrecondition*> > > maxControlExpressions;
    static vector<list<pair<int, NumericPrecondition*> > > nonConstantControlExpressions;
    static vector<list<pair<int, string> > > controlParamNames;

    static vector<int> howManyCP;

    static vector<vector<RPGControl*> > rpgControlExpressions;

    static vector<LinearEffects*> linearDiscretisation;

    static vector<list<ProtoConditionalEffect*> > actionsToRawConditionalEffects;

    static vector<list<NumericPrecondition> > actionsToStartNumericPreconditions;
    static vector<list<NumericPrecondition> > actionsToNumericInvariants;
    static vector<list<NumericPrecondition> > actionsToEndNumericPreconditions;

    static vector<list<NumericEffect> > actionsToStartNumericEffects;
    static vector<list<NumericEffect> > actionsToEndNumericEffects;

    static vector<list<int> > actionsToRPGNumericStartPreconditions;
    static vector<list<int> > actionsToRPGNumericInvariants;
    static vector<list<int> > actionsToRPGNumericEndPreconditions;

    static vector<list<int> > actionsToRPGNumericStartEffects;
    static vector<list<int> > actionsToRPGNumericEndEffects;

    //ipsa effect
    static vector<list<int> > actionsToRPGNumericStartNonIPSAEffects;
    static vector<list<int> > actionsToRPGNumericStartIPSAEffects;
    static vector<list<int> > actionsToRPGNumericEndNonIPSAEffects;
    static vector<list<int> > actionsToRPGNumericEndIPSAEffects;
    static vector<list<int> > actionsToRPGNumericTilEffects;
    static list<int> fakeRPGNumericIPSAEffects;

    static vector<list<ConditionalEffect> > actionsToConditionalEffects;

    static vector<int> initialUnsatisfiedStartPreconditions;
    static vector<int> initialUnsatisfiedInvariants;
    static vector<int> initialUnsatisfiedEndPreconditions;

    static vector<EpsilonResolutionTimestamp> achievedInLayer;
    static vector<EpsilonResolutionTimestamp> achievedInLayerReset;
    static vector<pair<int, VAL::time_spec> > achievedBy;
    static vector<pair<int, VAL::time_spec> > achievedByReset;

    static vector<EpsilonResolutionTimestamp> negativeAchievedInLayer;
    static vector<EpsilonResolutionTimestamp> negativeAchievedInLayerReset;
    static vector<pair<int, VAL::time_spec> > negativeAchievedBy;
    static vector<pair<int, VAL::time_spec> > negativeAchievedByReset;

    static vector<EpsilonResolutionTimestamp> numericAchievedInLayer;
    static vector<EpsilonResolutionTimestamp> numericAchievedInLayerReset;
    static vector<ActionFluentModification*> numericAchievedBy;
    static vector<ActionFluentModification*> numericAchievedByReset;

// static vector<int> increasedInLayer;
// static vector<pair<int, double> > increasedBy;
// static vector<pair<int, double> > increasedReset;

    static vector<instantiatedOp*> instantiatedOps;

    static vector<Literal*> literals;
    static vector<PNE*> pnes;
    static vector<pair<bool, bool> > staticLiterals;

    static vector<vector<Literal*> > propositionGroups;
    static vector<int> literalToPropositionGroupID;

    static vector<RPGNumericPrecondition> rpgNumericPreconditions;
    static vector<RPGNumericEffect> rpgNumericEffects;
    static vector<list<pair<int, VAL::time_spec> > > rpgNumericEffectsToActions;
    static vector<list<pair<int, VAL::time_spec> > > rpgNumericPreconditionsToActions;

    static vector<ArtificialVariable> rpgArtificialVariables;
    static vector<list<int> > rpgVariableDependencies;

    static vector<list<int> > rpgArtificialVariablesToPreconditions;
    static vector<list<int> > rpgNegativeVariablesToPreconditions;
    static vector<list<int> > rpgPositiveVariablesToPreconditions;

    static vector<int> initialUnsatisfiedNumericStartPreconditions;
    static vector<int> initialUnsatisfiedNumericInvariants;
    static vector<int> initialUnsatisfiedNumericEndPreconditions;

    static vector<list<pair<int, VAL::time_spec> > > processedPreconditionsToActions;
    static vector<list<pair<int, VAL::time_spec> > > processedNegativePreconditionsToActions;
    static vector<list<Literal*> > actionsToProcessedStartPreconditions;
    static vector<list<Literal*> > actionsToProcessedStartNegativePreconditions;
    static vector<int> initialUnsatisfiedProcessedStartPreconditions;

    static vector<list<pair<int, VAL::time_spec> > > processedRPGNumericPreconditionsToActions;

    static vector<list<NumericPrecondition> > actionsToProcessedStartNumericPreconditions;

    static list<Literal*> literalGoals;
    static list<double> literalGoalDeadlines;
    static list<NumericPrecondition> numericGoals;
    static list<double> numericGoalDeadlines;
    static list<pair<int, int> > numericRPGGoals;
    static list<double> rpgNumericGoalDeadlines;

    static vector<Constraint> preferences;
    static map<string, int> prefNameToID;

    static vector<Constraint> constraints;


    static vector<list<int> > actionsToProcessedStartRPGNumericPreconditions;
    static vector<int> initialUnsatisfiedProcessedStartNumericPreconditions;

    static vector<list<int> > mentionedInFluentInvariants;

    static list<FakeTILAction> timedInitialLiterals;
    static vector<FakeTILAction*> timedInitialLiteralsVector;
    static vector<FakeTILAction*> timedInitialLiteralsIPSAVector;

    static list<FakeTILAction> optimisationTimedInitialLiterals;
    static vector<FakeTILAction*> optimisationTimedInitialLiteralsVector;

    static vector<FakeTILAction*> allTimedInitialLiteralsVector;

    static map<int, set<int> > tilsThatAddFact;
    static map<int, set<int> > tilsThatDeleteFact;

    static vector<KShotFormula*> kShotFormulae;
    static vector<bool> selfMutexes;
    static vector<bool> oneShotLiterals;

    static vector<double> maxNeeded;

    /** @brief Semaphore facts and the PNEs they guard.
     *
     * @see <code>RPGBuilder::findSemaphoreFacts</code>.
     */
    static map<int, Guarded> semaphoreFacts;

    /** @brief  A map from action IDs to a fact that renders it uninteresting to apply. */
    static map<int, int> uninterestingnessCriteria;

    /** @brief  The IDs of concurrent redundant actions. */
    static set<int> concurrentRedundantActions;

    /** Facts which are manipulated by effects, but never appear in preconditions/the goal. */
    static LiteralSet factsWithOnlyPointlessEffects;

    /** The pointless start effects of each action. */
    static map<int, map<Literal*, pointless_effect, LiteralLT> > pointlessStartEffects;

    /** The pointless start effects of each action. */
    static map<int, map<Literal*, pointless_effect, LiteralLT> > pointlessEndEffects;

    /** The pointless start effects of each action. */
    static map<int, map<Literal*, pointless_effect, LiteralLT> > pointlessTILEffects;

    static void buildRPGNumericPreconditions();
    static void buildRPGNumericEffects();

    static bool processPreconditions(set<ArtificialVariable> & artificialVariableSet,
                                     map<RPGNumericPrecondition, list<pair<int, VAL::time_spec> > > & rpgNumericPreconditionSet,
                                     list<NumericPrecondition> & currPreList, list<int> & destList, int & toIncrement,
                                     const int & negOffset, const int & offset, int & precCount, int & avCount,
                                     vector<double> & localMaxNeed, const int & i, const VAL::time_spec & passTimeSpec);

    static void buildDurations(vector<list<NumericPrecondition*> > & d, vector<list<NumericPrecondition*> > & e, vector<list<NumericPrecondition*> > & f);
    static void buildControls(vector<list<pair<int, NumericPrecondition*> > > & e, vector<list<pair<int, NumericPrecondition*> > >& f, vector<list<pair<int, NumericPrecondition*> > > & g );

    static void findVariablesAffectedByControlParams(list<pair<int, RPGBuilder::NumericPrecondition *> > & minimum, list<pair<int, RPGBuilder::NumericPrecondition *> > & maximum, const int & opID);

    static vector<int> howManyUniqueCPInAction(vector<list<pair<int, NumericPrecondition*> > > & e, vector<list<pair<int, NumericPrecondition*> > > & f);

    static void findFiniteAchieverActions();
    /** @brief Infer additional start preconditions, due to numeric invariants.
     *
     *  Each numeric invariants (<code>over all</code> conditions) leads to a numeric start precondition in one of two ways:
     *  - if it refers to variables that are not affected by the actions start effects or continuous numeric effects, then it is
     *    copied as-is to be a start numeric precondition
     *  - otherwise, the invariant is regressed through the actions' start effects (and an epsilon amount of its continuous numeric)
     *    effects, and this new constraint added as a start precondition.
     *
     *  @param  pre        The numeric invariant
     *  @param  startEffs  The action's start numeric effects
     *  @param  ctsEffs    The action's continuous numeric effects
     *  @param  commondata  Comomon data used when building up extra preconditions (shared across many actions)
     *  @param  preResult   After the function, this holds the new numeric precondition formed.  It is a pair, where:
     *                      - the first entry is a pointer to the precondition itself
     *                      - the second entry is true if the precondition was new (i.e. never seen before on any action)
     *  @param  avResult    After the function, this holds the artificial variable that was created for the new precondition.  It is a pair, where:
     *                      - the first entry is a pointer to the artificial variable itself (or <code>0</code> if one was needed, i.e. the invariant
     *                        only referred to a single variable)
     *                      - the second entry is true if the artificial variable was new (i.e. never seen before in any other precondition)
     */
    static bool pushInvariantBackThroughStartEffects(const RPGNumericPrecondition & pre, list<int> & startEffs, LinearEffects * ctsEffs,
                                                     InvData & commonData, pair<const RPGNumericPrecondition *, bool> & preResult, pair<ArtificialVariable *, bool> & avResult);

    static void handleNumericInvariants();
    static void findSelfMutexes();
    static void oneShotInferForTILs();
    static void kshotInferForAction(const int & i, MinimalState & refState, LiteralSet & maybeOneShotLiteral, vector<double> & initialFluents, const int & fluentCount);
    static void doSomeUsefulMetricRPGInference();
    static void separateOptimisationTILs();

    /** @brief Read mutex groups from proposition.groups
     *
     * @see <code>RPGBuilder::readPropositionGroups</code>
     */
    static void readPropositionGroupsFile();

    /** @brief Find concurrent redundant actions
     *
     * A concurrent redundant action is one where there is no benefit to self-overlapping it
     * See 'Admissible Makespan Estimates for PDDL2.1 Temporal Planning',
     * by Patrik Haslum:
     *
     * http://ie.technion.ac.il/~dcarmel/heuristics-icaps09/hdip05-haslum.pdf
     */
    static void findConcurrentRedundantActions();

    /** @brief Identify some conditions under which the application of an action is uninteresting
     *
     * This function looks at facts which cannot be deleted.  If an actions only useful effect is to
     * add such a fact (i.e. all other facts are deletes or numerically undesirable) then once the
     * fact is true (or once the action has started) it's not interesting to start it again.
     */
    static void findUninterestingnessCriteria();
    static DurationExpr * buildDE(NumericPrecondition * d);
    static  ControlConstraint * buildControlExpression(NumericPrecondition* d);

    static list<ControlConstraint *>  buildCEList(list<NumericPrecondition *>  & d, const int & cp);

    static list<DurationExpr *> buildDEList(list<NumericPrecondition *> & d);

    /** @brief Remove the (linear) continuous effects from the given list, returning them. */
    static LinearEffects * buildLE(list<NumericEffect> & effList, const string & whereEffectsAreFrom);

    static bool considerAndFilter(LiteralSet & initialState, LiteralSet & revisit, const int & operatorID);
    static void postFilterUnreachableActions();
    static void buildMetric(VAL::metric_spec*);

    /** @brief Find facts that act as semaphores in durative domains.
     *
     *  A semaphore fact is one whose role within the planning domain is to force non-concurrency of
     *  certain actions, e.g. all the actions involving a piece of equipment.  For a fact p to be a
     *  semaphore:
     *
     *  - it must be true in the initial state
     *  - instantaneous actions requiring p must delete it then add it again
     *  - (at start (p)) is coupled with an effect (at start (not (p))) and one of either (at end (p)) or (at start (p))
     *  - (at end (p)) is coupled with an effect (at end (not (p))) and another (at end (p)).
     *  - there is no condition (over all (p))
     *
     *  They can also be goals, the rationale being that 'goals being satisfied' is an infinitely long action
     *  at the end of a plan, requiring the semaphore before it can be applied, i.e. requiring no actions
     *  to be executing (as follows from the PDDL semantics).
     *
     *  To make semaphore facts useful, the fluents they 'guard' are noted by this.  A semaphore fact guards a
     *  fluent if, to refer to the value of that fluent, one must also have the requisite semaphore fact.
     */
    static void findSemaphoreFacts();

    /** Initialise the look-up tables for conditional effects.
     *
     *  Visit the conditional effects noted during action instantiation, and
     *  make ConditionalEffect objects to represent each of them.  For now,
     *  the function also calls <code>postmortem_noADL</code> if there are
     *  any conditional propositional effects, or preconditions on facts that
     *  aren't exclusively controlled by timed initial literals.
     *  @see ConditionalEffect
     */
    static void buildThePropositionalBitOfConditionalEffects();

    /** Identify which facts are static. */
    static void findStaticLiterals();

    /** Remove pointless effects on literals that are never in preconditions.
     *
     *  If a literal is never a precondition (or a goal), then effects
     *  on it are pointless, and can be ignored without affecting completeness.
     *  The main benefit of this analysis is then that more actions become
     *  compression-safe, as they have no pointless end-delete effects.
     */
    static void removePointlessEffects();

    static void pruneStaticPreconditions(list<Literal*> & toPrune, int & toDec);
    static void pruneStaticPreconditions();

    class CommonRegressionData;

    static void postFilterIrrelevantActions();
    static void pruneIrrelevant(const int & operatorID);

    static RPGHeuristic * globalHeuristic;

public:

    static list<pair<int,bool> > taskVariablesEverAffectedByControlParameters;
    static list<pair<int,int> > actionsToArtificialVariables;
    static list<pair<int,int> > finiteToAux;
    //static list<RPGBuilder::ArtificialVariable> nonConstantFiniteAVs;
    static set<pair<int,int> > fluentsAtRisk;
    static set<pair<int, pair<RPGBuilder::RPGNumericEffect, RPGBuilder::RPGNumericEffect > > > watchedFluents;
    static list<pair<int,int> > supportingEffectToPre;
    static map<int,int> infiniteToFinite;
    static list<pair<pair<int,int>,int> > finiteToInfiniteToAux;
    static map<pair<unsigned int,int>,int> variableThreshold;

    static list<pair<int,int> > toBePushedLater;
    static bool calledMiniLP;

        /**
    *   When building RPG Numeric Preconditions of an additional instantiation, processPreconditions needs to have an access to
    *   previously generated RPGNumericPreconditions, so that it would not duplicate the same ones unnecessarily
    */

    static pair<bool, bool> & isStatic(Literal* l);

    static void simplify(pair<list<double>, list<int> > & s);
    static void makeOneSided(pair<list<double>, list<int> > & LHSvariable, pair<list<double>, list<int> > & RHSvariable, const int & negOffset);

    #ifdef STOCHASTICDURATIONS
    static void simplify(pair<list<double>, list<pair<int,PNE*> > > & s);
    static void makeDurationWeightedSum(list<Operand> & formula, pair<list<double>, list<pair<int,PNE*> > > & result);
    static void makeWeightedSum(list<Operand> & formula, pair<list<double>, list<int> > & result);
    #else
    static void makeDurationWeightedSum(list<Operand> & formula, pair<list<double>, list<int> > & result);
    static void makeWeightedSum(list<Operand> & formula, pair<list<double>, list<int> > & result);
    #endif

    static const vector<double> & getNonTemporalDurationToPrint() {
        return nonTemporalDuration;
    }

    static const vector<FakeTILAction*> & getAllTimedInitialLiterals() {
        return allTimedInitialLiteralsVector;
    }

    static vector<list<NumericPrecondition*> > & getFixedDEs() {
        return fixedDurationExpressions;
    }

    static vector<list<NumericPrecondition*> > & getMinDEs() {
        return minDurationExpressions;
    }

    static vector<list<NumericPrecondition*> > & getMaxDEs() {
        return maxDurationExpressions;
    }

    static vector<RPGDuration*> & getRPGDEs(const int & a) {
        return rpgDurationExpressions[a];
    }

    static vector<list<pair<int, NumericPrecondition*> > > & getMinCEs() {
        return minControlExpressions; //do not need to use
    }

    static vector<list<pair<int, NumericPrecondition*> > > & getMaxCEs() {
        return maxControlExpressions;
    }

    static int & getHowManyCP(const int & i) {
        return howManyCP[i];
    }

    static vector<RPGControl*> & getRPGCEs(const int & a) {
        return rpgControlExpressions[a];
    }

    static vector<LinearEffects*> & getLinearDiscretisation() {
        return linearDiscretisation;
    }

    static vector<list<int> > & getStartPreNumerics() {
        return actionsToRPGNumericStartPreconditions;
    }

    static vector<list<int> > & getInvariantNumerics() {
        return actionsToRPGNumericInvariants;
    }

    static vector<list<int> > & getEndPreNumerics() {
        return actionsToRPGNumericEndPreconditions;
    }

    static vector<list<int> > & getStartEffNumerics() {
        return actionsToRPGNumericStartEffects;
    }

    static vector<list<int> > & getEndEffNumerics() {
        return actionsToRPGNumericEndEffects;
    }

     // ipsa effects
    static vector<list<int> > & getStartNonIPSAEffNumerics() {
	return actionsToRPGNumericStartNonIPSAEffects;
    }

    static vector<list<int> > & getStartIPSAEffNumerics(){
	return actionsToRPGNumericStartIPSAEffects;
    }
    static vector<list<int> > & getEndNonIPSAEffNumerics() {
	return actionsToRPGNumericEndNonIPSAEffects;
    }

    static vector<list<int> > & getEndIPSAEffNumerics(){
	return actionsToRPGNumericEndIPSAEffects;
    }
    static vector<list<int> > & getTilNumerics() {
        return actionsToRPGNumericTilEffects;
    }
    static list<int> & getFakeIPSAEffNumerics(){
	return fakeRPGNumericIPSAEffects;
    }
   static vector<list<ConditionalEffect> > & getEditableActionsToConditionalEffects() {
        return actionsToConditionalEffects;
    }

    static const vector<list<ConditionalEffect> > & getActionsToConditionalEffects() {
        return actionsToConditionalEffects;
    }

    static vector<list<Literal*> > & getStartPropositionAdds() {
        return actionsToStartEffects;
    }

    static vector<list<Literal*> > & getStartPropositionDeletes() {
        return actionsToStartNegativeEffects;
    }

    static vector<list<Literal*> > & getEndPropositionAdds() {
        return actionsToEndEffects;
    };
    static vector<list<Literal*> > & getEndPropositionDeletes() {
        return actionsToEndNegativeEffects;
    };

    static vector<list<Literal*> > & getProcessedStartPropositionalPreconditions() {
        return actionsToProcessedStartPreconditions;
    };
    static vector<list<Literal*> > & getStartPropositionalPreconditions() {
        return actionsToStartPreconditions;
    };
    static vector<list<Literal*> > & getStartNegativePropositionalPreconditions() {
        return actionsToStartNegativePreconditions;
    };
    static vector<list<int> > & getProcessedStartRPGNumericPreconditions() {
        return actionsToProcessedStartRPGNumericPreconditions;
    }

    static vector<list<Literal*> > & getInvariantPropositionalPreconditions() {
        return actionsToInvariants;
    };
    static vector<list<Literal*> > & getInvariantNegativePropositionalPreconditions() {
        return actionsToNegativeInvariants;
    };
    static vector<list<Literal*> > & getEndPropositionalPreconditions() {
        return actionsToEndPreconditions;
    };
    static vector<list<Literal*> > & getEndNegativePropositionalPreconditions() {
        return actionsToEndNegativePreconditions;
    };
    static vector<list<int> > & getEndRPGNumericPreconditions() {
        return actionsToRPGNumericEndPreconditions;
    }

    static vector<list<Literal*> > & getProcessedStartNegativePropositionalPreconditions() {
        return actionsToProcessedStartNegativePreconditions;
    };

    /** @return A reference to <code>rpgNumericPreconditions</code>, the preconditions used in the problem, in LNF. */
    static vector<RPGNumericPrecondition> & getNumericPreTable() {
        return rpgNumericPreconditions;
    }

    /** @return A reference to <code>rpgNumericEffects</code>, the effects used in the problem, in LNF. */
    static vector<RPGNumericEffect> & getNumericEff() {
        return rpgNumericEffects;
    }

    /** @return a reference to <code>rpgNumericEffectsToActions</code>: for each numeric effect, which actions have it. */
    static const vector<list<pair<int, VAL::time_spec> > > & getRpgNumericEffectsToActions() {
        return rpgNumericEffectsToActions;
    }

    /** @return a reference to <code>rpgNumericPreconditionsToActions</code>: for each numeric precondition, which actions have it. */
    static const vector<list<pair<int, VAL::time_spec> > > & getRpgNumericPreconditionsToActions() {
        return rpgNumericPreconditionsToActions;
    }

    /**
     * @return A const reference to <code>rpgArtificialVariables</code>, the artificial variables built
     *         so that multi-variable preconditions and can be written in terms of a single 'artificial'
     *         variable.
     */
    static const vector<ArtificialVariable> & getArtificialVariableTable() {
        return rpgArtificialVariables;
    }

    /**
     * @return A const reference to <code>maxNeeded</code>, the maximum amount of each variable needed
     *         to satisfy the preconditions.
     */
    static const vector<double> & getMaxNeeded() {
        return maxNeeded;
    }

    /**
     * @see <code>RPGBuilder::findSemaphoreFacts</code>.
     *
     * @return A const reference to <code>semaphoreFacts</code>.
     */
    static const map<int, Guarded> & getSemaphoreFacts() {
        return semaphoreFacts;
    }

    static vector<list<pair<int, VAL::time_spec> > > & getPresToActions() {
        return processedPreconditionsToActions;
    }

    static vector<list<pair<int, VAL::time_spec> > > & getRawPresToActions() {
        return preconditionsToActions;
    };

    static vector<list<pair<int, VAL::time_spec> > > & getRawNumericPresToActions() {
        return rpgNumericPreconditionsToActions;
    }

    static vector<op_type> realRogueActions;
    static const vector<op_type> & rogueActions;
    static bool sortedExpansion;
    static bool fullFFHelpfulActions;
    static bool modifiedRPG;
    static bool noSelfOverlaps;
    static bool doTemporalAnalysis;
    static bool readPropositionGroups;

    static void initialise();

    static bool nonTemporalProblem() {
        return problemIsNotTemporal;
    };

    static bool actionIsDurative(const int & i) {
        return (!fixedDurationExpressions[i].empty() || !minDurationExpressions[i].empty() || !maxDurationExpressions[i].empty());
    }

    static list<Literal*> & getLiteralGoals() {
        return literalGoals;
    };

    static const list<double> & getLiteralGoalDeadlines() {
        return literalGoalDeadlines;
    }

    #ifdef POPF3ANALYSIS
    static void updateGoalDeadlines(const double & d) {
        {
            list<double>::iterator ldItr = literalGoalDeadlines.begin();
            const list<double>::iterator ldEnd = literalGoalDeadlines.end();

            for (; ldItr != ldEnd; ++ldItr) {
                if (*ldItr > d) {
                    *ldItr = d;
                }
            }
        }
        {
            list<double>::iterator ldItr = numericGoalDeadlines.begin();
            const list<double>::iterator ldEnd = numericGoalDeadlines.end();

            for (; ldItr != ldEnd; ++ldItr) {
                if (*ldItr > d) {
                    *ldItr = d;
                }
            }
        }
        cout << "\n * All goal deadlines now no later than " << d << std::endl;
    }
    #endif

    static RPGHeuristic * generateRPGHeuristic();
    static RPGHeuristic * getHeuristic() {
        if (!globalHeuristic) {
            globalHeuristic = RPGBuilder::generateRPGHeuristic();
        }
        return globalHeuristic;
    }

    static void getInitialState(LiteralSet & initialState, vector<double> & initialFluents);
    static void getNonStaticInitialState(LiteralSet & initialState, vector<double> & initialFluents);
    static instantiatedOp* getInstantiatedOp(const int & i) {
        return instantiatedOps[i];
    };
    static int numberOfActions(){
	return instantiatedOps.size();
    };
    static Literal* getLiteral(const int & i) {
        return literals[i];
    };
    static list<FakeTILAction> & getTILs() {
        return timedInitialLiterals;
    };
    static list<pair<int, VAL::time_spec> > & getEffectsToActions(const int & i) {
        return effectsToActions[i];
    };
    static const vector<list<pair<int, VAL::time_spec> > > & getNegativeEffectsToActions() {
        return negativeEffectsToActions;
    };
    static vector<FakeTILAction*> & getTILVec() {
        return timedInitialLiteralsVector;
    };

    static void getEffects(instantiatedOp* op, const bool & start, list<Literal*> & add, list<Literal*> & del, list<NumericEffect> & numeric);
    static void getPrecInv(instantiatedOp* op, const bool & start, list<Literal*> & precs, list<Literal*> & inv, list<NumericPrecondition> & numericPrec, list<NumericPrecondition> & numericInv);
    // static void getCollapsedAction(instantiatedOp* op, list<Literal*> & pre, list<Literal*> & add, list<Literal*> & del, list<NumericPrecondition> & numericPre, list<NumericEffect> & numericEff);

    static Metric * getMetric() {
        return theMetric;
    }

    static list<int> & getMentioned(const int & i) {
        return mentionedInFluentInvariants[i];
    };

    static bool stepNeedsToHaveFinished(const ActionSegment & act, const MinimalState & s, set<int> & dest);

    static double getOpMinDuration(instantiatedOp* op, const int & div);
    static double getOpMinDuration(const int & op, const int & div);

    static double getOpMaxDuration(instantiatedOp* op, const int & div);
    static double getOpMaxDuration(const int & op, const int & div);


    static pair<double, double> getOpDuration(instantiatedOp* op, const int & div, const vector<double> & minFluents, const vector<double> & maxFluents);
    static pair<double, double> getOpDuration(const int & op, const int & div, const vector<double> & minFluents, const vector<double> & maxFluents);

    static const vector<double> getOpMinControl(instantiatedOp* op); // this function calls minimum value of the control parameter cp
    static const vector<double> getOpMinControl(const int & op);
    static void setOpMinControl(const int & op, const int & cp, const double & value);
    static const vector<double> getOpMaxControl(instantiatedOp* op);
    static const vector<double> getOpMaxControl(const int & op);
    static void setOpMaxControl(const int & op, const int & cp, const double & value);

    static const vector<double> getRPGOpMinControl(const int & op);
    static const vector<double> getRPGOpMaxControl(const int & op);

    static list<pair<int,int> > actionsWithNonConstantControlParameters;

    static vector<vector<double> > getOpControl(const int & a, const vector<double> & minFluents, const vector<double> & maxFluents);
    static vector<vector<double> > getOpControl(instantiatedOp* op, const vector<double> & minFluents, const vector<double> & maxFluents);

    static PNE* getPNE(const int & i) {
        assert(i >= 0);
        if (i >= (int) pnes.size()) {

            cout << " int i is " << i << " pne size is  " << pnes.size() << endl;
        }
        assert(i < (int) pnes.size());
        return pnes[i];
    };

    static string * getCPName(const int & actID, const int & cpID) {

        string * toReturn;

        assert(cpID <= -100);

            list<pair<int, string> >::iterator dItr = controlParamNames[actID].begin();
            const list<pair<int, string> >::iterator dEnd = controlParamNames[actID].end();

        while(dItr != dEnd){
            if (dItr->first == cpID){
                toReturn = &(dItr->second);
            }
         dItr++;
        }
    return toReturn;
    };

    static int getPNECount() {
        return pnes.size();
    };

    static int getAVCount() {
        return rpgArtificialVariables.size();
    };
    static ArtificialVariable & getArtificialVariable(const int & i) {
        return rpgArtificialVariables[i - (2 * getPNECount())];
    };

    static list<int> & getVariableDependencies(const int & i) {
        return rpgVariableDependencies[i];
    };
    static list<int> & affectsRPGNumericPreconditions(int i) {

        static const int off = getPNECount();
        if (i < off) {
            return rpgPositiveVariablesToPreconditions[i];
        };
        i -= off;
        if (i < off) {
            return rpgNegativeVariablesToPreconditions[i];
        }
        i -= off;
        return rpgArtificialVariablesToPreconditions[i];

    }


    static int howManyTimes(const int & actID, const MinimalState & e) {
        return kShotFormulae[actID]->getLimit(e);
    };
    static int howManyTimesOptimistic(const int & actID, const MinimalState & e) {
        return kShotFormulae[actID]->getOptimisticLimit(e);
    };
    static bool literalIsOneShot(const int & lID) {
        return oneShotLiterals[lID];
    };
    static bool isSelfMutex(const int & actID) {
        return selfMutexes[actID];
    };
    static list<pair<int, int> > & getNumericRPGGoals() {
        return numericRPGGoals;
    };
    static list<double> & getNumericRPGGoalDeadlines() {
        return rpgNumericGoalDeadlines;
    };
    static list<NumericPrecondition> & getNumericGoals() {
        return numericGoals;
    };

    /** @brief Find whether applying an action in the given state is concievably worthwhile
     *
     * @see <code>RPGBuilder::findUninterestingnessCriteria</code> and <code>RPGBuilder::findConcurrentRedundantActions</code>
     *
     * @param act   The ID of the action to apply (an index into <code>RPGBuilder::instantiatedOps</code>
     * @param facts  The facts in the current state
     * @param started The actions that have been started in the current state
     * @retval <code>true</code>  The action might be useful to apply
     * @retval <code>false</code> The action is definitely not useful to apply
     */
    static bool isInteresting(const int & act, const StateFacts & facts, const map<int, set<int> > & started);

    static LiteralSet & getEndOneShots(const int & i) {
        return actionsToEndOneShots[i];
    };

    static const vector<vector<Literal*> > & getPropositionGroups() {
        return propositionGroups;
    }

    static const vector<int> & getLiteralToPropositionGroupID() {
        return literalToPropositionGroupID;
    }
};



class RPGHeuristic
{

private:
    class Private;

    Private * const d;

public:

    /** @brief If set to true, the heuristic returns a value of 1 for goal states, or 0 otherwise. */
    static bool blindSearch;

    /** @brief If set to true, the RPG ignores numeric preconditions and effects. */
    static bool ignoreNumbers;

    /** @brief If set to true, the RPG integrates continuous effects, so they occur in full at the start of the action. */
    static bool makeCTSEffectsInstantaneous;

    /** @brief Configure the heuristic guidance provided by the RPG heuristic.
     *
     *  The heuristic configuration is determined according to the
     *  given string (usually passed with the prefix '-g' on the command line).
     *
     *  @param config  Configuration name to use
     */
    static void setGuidance(const char * config);

    static set<int> emptyIntList;

    /** @brief Number of states evaluated during search. */
    static unsigned int statesEvaluated;

    /** @brief If set to true, print RPGs in DOT format. */
    static bool printRPGAsDot;

    RPGHeuristic(const bool & b,
                 vector<list<Literal*> > * atse,
                 vector<list<Literal*> > * atee,
                 vector<list<pair<int, VAL::time_spec> > > * eta,
                 vector<list<Literal*> > * atsne,
                 vector<list<Literal*> > * atene,
                 vector<list<pair<int, VAL::time_spec> > > * neta,
                 vector<list<pair<int, VAL::time_spec> > > * pta,
                 vector<list<Literal*> > * atsp,
                 vector<list<Literal*> > * ati,
                 vector<list<Literal*> > * atep,
                 vector<list<RPGBuilder::NumericEffect> > * atnuse,
                 vector<list<RPGBuilder::NumericEffect> > * atnuee,
                 vector<list<int> > * atrnuse,
                 vector<list<int> > * atrnuee,
                 vector<list<int> > * atnusp,
                 vector<list<int> > * atnui,
                 vector<list<int> > * atnuep,
                 vector<list<int> > * atpnuep,
                 vector<int> * iusp,
                 vector<int> * iuip,
                 vector<int> * iuep,
                 vector<EpsilonResolutionTimestamp> * ail,
                 vector<EpsilonResolutionTimestamp> * ailr,
                 vector<pair<int, VAL::time_spec> > * ab,
                 vector<pair<int, VAL::time_spec> > * abr,
                 vector<EpsilonResolutionTimestamp> * nail,
                 vector<EpsilonResolutionTimestamp> * nailr,
                 vector<ActionFluentModification*> * nab,
                 vector<ActionFluentModification*> * nabr,
                 vector<int> * iunsp,
                 vector<int> * iuni,
                 vector<int> * iunep,
                 vector<RPGBuilder::RPGNumericPrecondition> * rnp,
                 vector<RPGBuilder::RPGNumericEffect> * rne,
                 vector<list<pair<int, VAL::time_spec> > > * ppta,
                 vector<list<pair<int, VAL::time_spec> > > * nppta,
                 vector<list<Literal*> > * atpsp,
                 vector<int> * iupsp,
                 vector<int> * iupsnp,
                 list<pair<int, VAL::time_spec> > * pla,
                 list<pair<int, VAL::time_spec> > * onpa);

    ~RPGHeuristic();

    static vector<EpsilonResolutionTimestamp> & getEarliestForStarts();
    static vector<EpsilonResolutionTimestamp> & getEarliestForEnds();


    #ifdef POPF3ANALYSIS
    /** @brief Call this when Globals::bestSolutionQuality is better. */
    void metricHasChanged();
    #endif

    int getRelaxedPlan(const MinimalState & theState, const list<StartEvent> * startEventQueue, list<FFEvent> & header,
                       const vector<double> & minTimestamps, const double & stateTS,
                       const vector<double> & extrapolatedMin, const vector<double> & extrapolatedMax, const vector<double> & timeAtWhichStateVariableBoundsHold,
                       list<ActionSegment> & helpfulActions, list<pair<double, list<ActionSegment> > > & relaxedPlan,
                       double & finalPlanMakespanEstimate, map<double, list<pair<int, int> > > * justApplied = 0, double tilFrom = EPSILON);

    void findApplicableActions(const MinimalState & theState, const double & stateTime, list<ActionSegment> & applicableActions, list<FFEvent> & plan);
    void filterApplicableActions(const MinimalState & theState, const double & stateTime, list<ActionSegment> & applicableActions, list<FFEvent> & plan);

    bool testApplicability(const MinimalState & theState, const double & stateTime, const ActionSegment & actID, bool fail = false, bool ignoreDeletes = false);

    list<Literal*> & getDeleteEffects(const int & i, const VAL::time_spec & t);
    list<Literal*> & getAddEffects(const int & i, const VAL::time_spec & t);
    list<Literal*> & getPreconditions(const int & i, const VAL::time_spec & t);
    list<Literal*> & getInvariants(const int & i);

    list<int> & getNumericEffects(const int & i, const VAL::time_spec & t);
    RPGBuilder::RPGNumericEffect & getRPGNE(const int & i);

    instantiatedOp* getOp(const int & i);
    EpsilonResolutionTimestamp earliestTILForAction(const int & i, const bool & isStart);

    static EpsilonResolutionTimestamp & getDeadlineRelevancyStart(const int & i);

    static EpsilonResolutionTimestamp & getDeadlineRelevancyEnd(const int & i);


    void doFullExpansion(MinimalState & refState);

};

ostream & operator <<(ostream & o, const RPGBuilder::NumericPrecondition & p);
ostream & operator <<(ostream & o, const RPGBuilder::NumericEffect & p);

ostream & operator <<(ostream & o, const RPGBuilder::RPGNumericPrecondition & p);
ostream & operator <<(ostream & o, const RPGBuilder::ArtificialVariable & p);
ostream & operator <<(ostream & o, const RPGBuilder::RPGNumericEffect & p);

enum whereAreWe { PARSE_UNKNOWN, PARSE_PRECONDITION, PARSE_EFFECT, PARSE_DURATION, PARSE_CONTROL, PARSE_GOAL, PARSE_INITIAL, PARSE_CONDITIONALEFFECT, PARSE_CONTINUOUSEFFECT, PARSE_METRIC, PARSE_DERIVATION_RULE, PARSE_CONSTRAINTS };

extern whereAreWe WhereAreWeNow;

void validatePNE(PNE * c);
void validateLiteral(Literal * l);

void postmortem_noNestedWhens();
void postmortem_noADL();
void postmortem_nonLinearCTS(const string & actName, const string & worksOutAs);
void postmortem_noQuadratic(const string & theOp);
void postmortem_noTimeSpecifierOnAPropPrecondition(const string & actname, const string & effect);
void postmortem_twoSimulataneousNumericEffectsOnTheSameVariable(const string & actname, const string & varname);
void postmortem_fixedAndNotTimeSpecifiers(const string & actname, const bool & multipleEquals);
void postmortem_noTimeSpecifierOnAPropEffect(const string & actname, const string & effect);
void postmortem_noTimeSpecifierOnInstantNumericEffect(const string & actname, const string & effect, const string & suggested, const bool & isAssign);
void postmortem_wrongNumberOfFluentArguments(const string & actname, const bool & haveActName, const whereAreWe & w, const string & predname, const string & lit, const int & givenArgs, const set<int> & realargs);
void postmortem_wrongNumberOfPredicateArguments(const string & actname, const bool & haveActName, const whereAreWe & w, const string & predname, const string & lit, const int & givenargs, const set<int> & realargs);
void postmortem_mathsError(const string & description, const string & help, const whereAreWe & w);
void postmortem_noConstraints(const bool unsupportedPref = false, const char * n = 0);
void postmortem_isViolatedNotExist(const string & s);
void postmortem_fatalConstraint(const string & whichOne);
void postmortem_processesMustHaveNoConditions(const string & actName);
};



#endif
// kate: indent-mode cstyle; space-indent on; indent-width 4; replace-tabs on;
