#ifndef __Comparator_h__
#define __Comparator_h__

#include "Network.h"
#include<vector>

using namespace std;

class Comparator{
 public:
  Comparator(Network net, int i=0, int j=1);
  void compare(int variable, bool printGraph=false);
 private:
  Network network1;
  Network network2;
  
  int mode1, mode2;
  bool getVariables(int variable, vector<double>& x, vector<double>& y);
  void drawGraph(vector<pair<double, double> >& xy, string name);
};
#endif
