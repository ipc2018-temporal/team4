/************************************************************************
 * Copyright 2009, 2011, Strathclyde Planning Group,
 * Department of Computer and Information Sciences,
 * University of Strathclyde, Glasgow, UK
 * http://planning.cis.strath.ac.uk/
 *
 * Amanda Coles, Andrew Coles, Maria Fox, Derek Long - POPF
 * Maria Fox, Richard Howey and Derek Long - VAL
 * Stephen Cresswell - PDDL Parser
 *
 * This file is part of POPF.
 *
 * POPF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * POPF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with POPF.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/

#include "minilp.h"
#include "globals.h"
#include "numericanalysis.h"
#include "temporalanalysis.h"
#include "temporalconstraints.h"
#include "colours.h"

#include "solver.h"

#include <limits>

#include <sstream>
#include <string>
#include <list>
#include <iterator>
#include <algorithm>
#include <iomanip>

using std::ostringstream;
using std::string;
using std::list;
using std::for_each;
using std::endl;

#define NaN std::numeric_limits<double>::signaling_NaN()

namespace Planner{
    //constraints
set<MiniLP::Constraint> MiniLP::Constraint::constraintStore;
int MiniLP::Constraint::constraintCount = 0;

vector<vector<list<RPGBuilder::RPGNumericEffect* > > > MiniLP::instantEffects;
vector<vector<list<const MiniLP::Constraint*> > > MiniLP::constraints;

vector<int> MiniLP::controlVariables;
vector<int> MiniLP::applyToStart;
vector<int> MiniLP::applyToEnd;
vector<int> MiniLP::firstOptimalValuesMin;
vector<int> MiniLP::secondOptimalValuesMin;
vector<int> MiniLP::firstOptimalValuesMax;
vector<int> MiniLP::secondOptimalValuesMax;

vector<double> MiniLP::initialValues;
vector<int> MiniLP::lastLPVariable;
//vector<bool> MiniLP::oneToManyCase;

bool MiniLP::nameLPElements;
MILPSolver * MiniLP::lp;
bool MiniLP::initialised = false;

int MiniLP::numVars;
bool MiniLP::cpdebug = false;

int MiniLP::nextPNEStart = 0;

int MiniLP::stepID = 0;

void MiniLPPrintRow(MILPSolver *, const int &, const int &);

void MiniLP::MiniLPPrint(MILPSolver *lp)
{
    const int cc = lp->getNumCols();
    for (int ci = 0; ci < cc; ++ci) {
        cout << "C" << ci << ", " << lp->getColName(ci) << ", has range [" << lp->getColLower(ci) << "," << lp->getColUpper(ci) << "]\n";
    }


    const int rc = lp->getNumRows();

    MiniLPPrintRow(lp, 0, rc);

};

void MiniLPPrintRow(MILPSolver * lp, const int & rs, const int & rc)
{
    for (int r = 0; r < rc; ++r) {
        if (r < rs) continue;

        cout << r << ",\"" << lp->getRowName(r) << "\",\"";

        vector<pair<int,double> > entries;
        lp->getRow(r, entries);
        vector<pair<int,double> >::iterator thisRow = entries.begin();
        const vector<pair<int,double> >::iterator thisRowEnd = entries.end();

        for (int v = 0; thisRow != thisRowEnd; ++thisRow, ++v) {
            if (v) {
                if (thisRow->second >= 0.0) {
                    cout << " + ";
                    if (thisRow->second != 1.0) cout << thisRow->second << ".";
                } else {
                    if (thisRow->second == -1.0) {
                        cout << " - ";
                    } else {
                        cout << " " << thisRow->second << ".";
                    }
                }
            } else {
                if (thisRow->second == 1.0) {
                    cout << "";
                } else if (thisRow->second == -1.0) {
                    cout << "-";
                } else {
                    cout << thisRow->second << ".";
                }
            }

            cout << lp->getColName(thisRow->first);
        }
        cout << " in [";
        if (lp->getRowLower(r) == -LPinfinity) {
            cout << "-inf,";
        } else {
            cout << lp->getRowLower(r) << ",";
        }
        if (lp->getRowUpper(r) == LPinfinity) {
            cout << "inf]";
        } else {
            cout << lp->getRowUpper(r) << "]";
        }

        cout << "\"\n";
    }

}

class MiniLP::ConstraintAdder {

protected:
    const int currTS;
    const int stepID;
    int effCounter;

    void addNormalEffect(RPGBuilder::RPGNumericEffect* const ceItr) {

        static const int varCount = RPGBuilder::getPNECount();

        const int currVar = ceItr->fluentIndex;

        int sLim = ceItr->size;

        bool hasControlVar = false;

        vector<pair<int,double> > entries;
        vector<pair<int,double> > endEntries;
        vector<pair<int,double> > variableEntries(0);
        entries.reserve(sLim);
        endEntries.reserve(sLim);

        { ///add state LP variables that are seen in numeric effects
        nextPNEStart = lp->getNumCols();
        if(applyToStart[currVar] == -1) { /// this means no LP variables for this state var is added before.
        lp->addCol(variableEntries, -LPinfinity, LPinfinity, MILPSolver::C_REAL);

        if (nameLPElements) { // add variable
            ostringstream namestream;
            namestream << *(RPGBuilder::getPNE(currVar));
            namestream << "_START" << stepID;
            string asString = namestream.str();
            lp->setColName(nextPNEStart, asString);
            }
            applyToStart[currVar] = nextPNEStart;
        }
        if (applyToEnd[currVar] == -1){
        lp->addCol(variableEntries, -LPinfinity, LPinfinity, MILPSolver::C_REAL);

        if (nameLPElements) {
            ostringstream namestream;
            namestream << *(RPGBuilder::getPNE(currVar));
            namestream << "_END" << stepID;
            string asString = namestream.str();
            lp->setColName(nextPNEStart+1, asString);
            }
            applyToEnd[currVar] = nextPNEStart+1;
            }
        }

    int pne = (currTS ? applyToEnd[currVar] : applyToStart[currVar]);
    entries.push_back(make_pair(pne, 1.0));


        for (int s = 0; s < sLim; ++s) {
            double currW = -ceItr->weights[s];
            int varIdx = ceItr->variables[s];
            if (varIdx >= 0) {

                int pneStep = (currTS ? applyToEnd[varIdx] : applyToStart[varIdx]);

                if (varIdx >= varCount) {
                    varIdx -= varCount;
                    currW = ceItr->weights[s];
                }
                entries.push_back(make_pair(pneStep, currW));

            } else if (varIdx <= -100 && varIdx > -116 ) {
                int contStep;
                int contVar = -(varIdx + 100);

                contStep = MiniLP::controlVariables[contVar];

                hasControlVar = true;
                entries.push_back(make_pair(contStep, currW));
            } else if (varIdx <= -116 ) {
                int contStep;
                int contVar = -(varIdx + 116);

                contStep = MiniLP::controlVariables[contVar];

                hasControlVar = true;
                entries.push_back(make_pair(contStep, -currW));
            } else {
                cout << "Unprocessed effect in miniLP, aborting.\n";
                assert(false);
            }
        }

        if (!(ceItr->isAssignment)) {
            entries.push_back(make_pair(lastLPVariable[currVar], -1.0));
            }

        if (cpdebug) {
            if (hasControlVar) {
             cout << "Adding effect dependent on control parameter: \n";
                const int sLim = entries.size();
                for (int sp = 0; sp < sLim; ++sp) {
                    cout << "\t";
                    if (sp > 0) cout << " + "; else cout << "   ";
                    cout << entries[sp].second << " * " << lp->getColName(entries[sp].first) << "\n";
                }
                cout << "\t = " << ceItr->constant << endl;
            } else {
                cout << "Added effect " << *ceItr << " to get value of " << *(RPGBuilder::getPNE(currVar)) << "\n";
            }
        }

        lp->addRow(entries, ceItr->constant, ceItr->constant);

        if (nameLPElements) {
            const int constrIdx = lp->getNumRows() - 1;
            ostringstream namestream;
            namestream << "eff@" << stepID << "n" << effCounter << (currTS ? "END" : "START");
            string asString = namestream.str();
            lp->setRowName(constrIdx, asString);
        }

        if(!currTS) { /// that means there is no end effect added for this variable.
            endEntries.push_back(make_pair(MiniLP::applyToStart[currVar], -1.0));
            endEntries.push_back(make_pair(MiniLP::applyToStart[currVar] + 1, 1.0));
            lp->addRow(endEntries, 0.0, 0.0);
            if (nameLPElements) {
            const int constrIdx = lp->getNumRows() - 1;
            ostringstream namestream;
            namestream << "no-eff@" << stepID << "n" << effCounter << "DUMMY_END";
            string asString = namestream.str();
            lp->setRowName(constrIdx, asString);
            }
        }


        lastLPVariable[currVar] = pne;

      //  (*output)[currVar] = colIdx;

        ++effCounter;
    }

    mutable int suffix;

public:
    map<int, int> * applyTo;
    map<int, int> * output;

    ConstraintAdder(const int & stepIDIn, const int & currTSIn) : stepID(stepIDIn), currTS(currTSIn), effCounter(0), suffix(0) {

    }

    void operator()(const Constraint * const csItr) const {

        const int cSize = csItr->weights.size();
        vector<pair<int,double> > variableEntries(0);
        vector<pair<int,double> > entries;
        entries.reserve(cSize + 2);

        for (int s = 0 ; s < cSize; ++s) {  ///add state LP variables that are seen in numeric preconditions
            if (csItr->variables[s] >= 0) {

            nextPNEStart = lp->getNumCols();

            if (applyToStart[csItr->variables[s]] == -1) {

            lp->addCol(variableEntries, -LPinfinity, LPinfinity, MILPSolver::C_REAL);

                if (nameLPElements) { // add variable
                ostringstream namestream;
                namestream << *(RPGBuilder::getPNE(csItr->variables[s]));
                namestream << "_START" << stepID;
                string asString = namestream.str();
                lp->setColName(nextPNEStart, asString);
                }

            applyToStart[csItr->variables[s]] = nextPNEStart;
            }

            if (applyToEnd[csItr->variables[s]] == -1) { /// which means LP variable for this state var is not added before (in numeric effects)

            lp->addCol(variableEntries, -LPinfinity, LPinfinity, MILPSolver::C_REAL);

                if (nameLPElements) {
                ostringstream namestream;
                namestream << *(RPGBuilder::getPNE(csItr->variables[s]));
                namestream << "_END" << stepID;
                string asString = namestream.str();
                lp->setColName(nextPNEStart+1, asString);
                }

            applyToEnd[csItr->variables[s]] = nextPNEStart+1;
            }

            }
        }

        if (cpdebug) {
            cout << "Adding constraint: ";
            for (int s = 0 ; s < cSize; ++s) {
                    assert(applyToEnd[csItr->variables[s]] != -1);
                int pneStep = (currTS ? applyToEnd[csItr->variables[s]] : applyToStart[csItr->variables[s]]);

                    if (csItr->variables[s] <= -100 && csItr->variables[s] > -116) {
                    if (s) cout << " + ";
                    } else if(csItr->variables[s] <= -116 ) {
                    if (s) cout << " - ";
                    }
                cout << csItr->weights[s] << "*";
                if (csItr->variables[s] == -3) {
                    cout << "?duration";
                } else if (csItr->variables[s] == -4) {
                    cout << "total-time";
                } else if (csItr->variables[s] <= -100 && csItr->variables[s] > -116) {
                   cout << lp->getColName(MiniLP::controlVariables[- csItr->variables[s] - 100]);
                } else if (csItr->variables[s] <= -116 ) {
                   cout << lp->getColName(MiniLP::controlVariables[- csItr->variables[s] - 116]);
                } else {
                cout << lp->getColName(pneStep);
                }
            }
            if (csItr->lower != -DBL_MAX) {
                cout << ", >= " << csItr->lower;
            }
            if (csItr->upper != DBL_MAX) {
                cout << ", <= " << csItr->upper;
            }
            cout << std::endl;
        }

        for (int s = 0 ; s < cSize; ++s) {
            if (csItr->variables[s] >= 0) {

            int pneStep = (currTS ? lastLPVariable[csItr->variables[s]] : lastLPVariable[csItr->variables[s]]);
                entries.push_back(make_pair(pneStep, csItr->weights[s]));
                assert(entries.back().second != 0.0);



            } else if (csItr->variables[s] <= -100 && csItr->variables[s] > -116) {
                     entries.push_back(make_pair(MiniLP::controlVariables[- csItr->variables[s] - 100], csItr->weights[s]));
            } else if (csItr->variables[s] <= -116 ) {
                     entries.push_back(make_pair(MiniLP::controlVariables[- csItr->variables[s] - 116], -csItr->weights[s]));
            } else {
                 std::cerr << "Fatal internal error: variable index " << csItr->variables[s] << " used\n";
                    exit(1);
            }
        }

        lp->addRow(entries,
                           (csItr->lower != -DBL_MAX ? csItr->lower : -LPinfinity),
                           (csItr->upper != DBL_MAX  ? csItr->upper : LPinfinity ) );

        if (nameLPElements) {
            const int constrIdx = lp->getNumRows() - 1;
            ostringstream namestream;
            namestream << "pre@t" << stepID << "n" << suffix;
            string asString = namestream.str();
            lp->setRowName(constrIdx, asString);
            ++suffix;
        }
    }

    void operator()(const pair<const Constraint*, unsigned int> & csItr) const {
        operator()(csItr.first);
    }

    void operator()(RPGBuilder::RPGNumericEffect* const ceItr) {
                addNormalEffect(ceItr);
                return;
        }
};

struct MiniLP::ControlParameterAdder
{

    int stepAt;
    int cpID;
    VAL::comparison_op controlType;

    ControlParameterAdder(const int & cpIDIn)
            : cpID(cpIDIn), controlType(VAL::E_EQUALS), stepAt(-1) {
    }

    void setTS(const int & as, const VAL::comparison_op & ct) {
        stepAt = as;
        controlType = ct;
    }

    void operator()(RPGBuilder::ControlConstraint* const currCE) {

        const int vSize = currCE->weights.size();
        vector<pair<int,double> > entries(1 + vSize);

        entries[0] = make_pair(stepAt, 1.0);
//cout << "stepAt "  << stepAt << "  col "<<  lp->getNumCols()  << endl;
        assert(stepAt < lp->getNumCols());

        {
            for (int v = 0; v < vSize; ++v) {
                entries[1+v].second = -(currCE->weights[v]);
                assert(entries[1+v].second != -0.0);
                int & vToUse = lastLPVariable[currCE->variables[v]];
                if (vToUse != -1) {
                    entries[1+v].first = vToUse;
                } else {
                    std::cerr << "Internal error: negative variable " << vToUse << " in control expression\n";
                    exit(1);
                }
            }
        }

        switch (controlType) {
        case VAL::E_GREATEQ: {
            if (cpdebug) cout << "minimum of control parameter " << - (cpID + 100)  << " is " << currCE->constant << "\n";

            lp->addRow(entries, currCE->constant, LPinfinity);
            if (nameLPElements) {
                int constrIdx = lp->getNumRows() - 1;
                ostringstream namestream;
                namestream << "CP" << "min" << cpID;
                string asString = namestream.str();
                lp->setRowName(constrIdx, asString);
            }
            break;
        }
        case VAL::E_LESSEQ: {
            if (cpdebug) cout << "maximum of control parameter " << - (cpID + 100) << " is " << currCE->constant <<  "\n";

            lp->addRow(entries, -LPinfinity, currCE->constant);
            if (nameLPElements) {
                int constrIdx = lp->getNumRows() - 1;
                ostringstream namestream;
                namestream << "CP" << "max" << cpID;
                string asString = namestream.str();
                lp->setRowName(constrIdx, asString);
            }
            break;
        }
        default:
            assert(false);
        }

    }

};

MiniLP::MiniLP(const int & actID, const int & cpIDIn)
{
    nameLPElements = cpdebug;

    const int operatorCount = instantiatedOp::howMany();
    int controlNum = 0;

    if (RPGBuilder::getHowManyCP(actID) > 0) { // only count control parameters once per action
           ///returns number of unique control parameters exist in the action in total.
    controlNum = RPGBuilder::getHowManyCP(actID) ;
      }

    if(!initialised) initialise(actID);

    controlVariables.resize(controlNum,-1);
    lastLPVariable.resize(numVars, -1);
    applyToStart.resize(numVars, -1);
    applyToEnd.resize(numVars, -1);
    lastLPVariable.resize(numVars);
    firstOptimalValuesMin.resize(numVars);
    secondOptimalValuesMin.resize(numVars);
    firstOptimalValuesMax.resize(numVars);
    secondOptimalValuesMax.resize(numVars);
//    oneToManyCase.resize(operatorCount, false);

     if (cpdebug) cout << "  - Making MiniLP for action " << *(RPGBuilder::getInstantiatedOp(actID)) << " that has "
 << controlNum << " control variables, and "<< numVars << " variables\n";

    lp = getNewSolver();

    lp->addEmptyRealCols(numVars);

	 if (!cpdebug) lp->hush();
    /**  initialise the vector last LP Variable  */
        for (int i = 0; i < numVars; ++i)  {
        lastLPVariable[i] = i;     //MiniLP::applyToStart[i];
        }

        /** create initial value variables*/
	 	for (int i = 0; i < numVars; ++i) {
        const double curr = initialValues[i];
        lp->setColLower(i, curr);
        lp->setColUpper(i, curr);

        if (nameLPElements) {
            ostringstream namestream;
            namestream << *(RPGBuilder::getPNE(i));
            namestream << "[0.000]";
            string asString = namestream.str();
            lp->setColName(i, asString);
			}
	 	}
    ///reset applyToStart and End..not to get confused with previous instantiation of actions..
	     for (int pne = 0; pne < numVars; ++pne ){
        applyToStart[pne] = -1;
        applyToEnd[pne] = -1;
        }

    /// add first actions variables and constraints
	 addActionVariablesAndConstraints(controlNum,actID,stepID);

    ///now, solve LP for the first action constraints
    if (cpdebug) MiniLPPrint(lp);

    for (int pne = 0; pne < numVars; ++pne ){
        if (applyToEnd[pne] != -1) {
            lp->clearObjective();

            int optimise = applyToEnd[pne];

        lp->setObjCoeff(optimise, 1.0);

    if (cpdebug){
        cout << "About to call solve, minimising " << lp->getColName(optimise) << "\n";
        }
        lp->setMaximiseObjective(false);
        lp->mustSolve(false);

            //firstOptimalValuesMin[pne] = lp->getSingleSolutionVariableValue(optimise);

    if (cpdebug) {
        cout << "About to call solve, maximising " << lp->getColName(optimise) << "\n";
    }
        lp->setMaximiseObjective(true);
        lp->mustSolve(false);

          //  firstOptimalValuesMax[pne] = lp->getSingleSolutionVariableValue(optimise);
    } else {
       if (cpdebug) cout << "something went wrong. The end of action is not seen. " << endl;
    }
}
  /*
    ///increment dummy stepID by 1.
	 stepID++;
    ///reset applyToStart and End..not to get confused with previous action..
	     for (int pne = 0; pne < numVars; ++pne ){
        applyToStart[pne] = -1;
        applyToEnd[pne] = -1;
        }
    ///add second action constraints and variables.
	 addActionVariablesAndConstraints(controlNum, actID, stepID);

    ///now, solve LP for the *first* and *second* action constraints together. We will then compare the optimal solutions.
    if (cpdebug) MiniLPPrint(lp);
for (int pne = 0; pne < numVars; ++pne ){
    if (applyToEnd[pne] != -1) {
        lp->clearObjective();

        int optimise = applyToEnd[pne];

        lp->setObjCoeff(optimise, 1.0);

    if (cpdebug){
        cout << "About to call solve, Minimising " << lp->getColName(optimise) << "\n";
    }
        lp->setMaximiseObjective(false);
        lp->mustSolve(false);

            secondOptimalValuesMin[pne] = lp->getSingleSolutionVariableValue(optimise);

    if (cpdebug) {
        cout << "About to call solve, Maximising " << lp->getColName(optimise) << "\n";
    }
        lp->setMaximiseObjective(true);
        lp->mustSolve(false);

            secondOptimalValuesMax[pne] = lp->getSingleSolutionVariableValue(optimise);

   } else {
       if (cpdebug) cout << "something went wrong. The end of action is not seen. " << endl;
    }
}
         for (int pne = 0; pne < numVars; ++pne ){
            if((firstOptimalValuesMax[pne] == secondOptimalValuesMax[pne]) && (firstOptimalValuesMin[pne] == secondOptimalValuesMin[pne])) {
            if(cpdebug) cout << "   -   MiniLP found a special case. Avoid applying "  << *(RPGBuilder::getInstantiatedOp(actID)) << " again.." << endl;
               oneToManyCase[actID] = true;
            }
        } */

        /// now, optimise the limits of control params
            for(int  cp = 0; cp < controlNum; ++cp){
        lp->clearObjective();
        int optimise = controlVariables[cp];

            lp->setObjCoeff(optimise, 1.0);

            lp->setMaximiseObjective(false);
            lp->mustSolve(false);

            /// recalculate the minimum bound of cp
            if (cpdebug) cout <<   "    +   Minimum limit of control parameter of " << (-100 -cp) << " was " << RPGBuilder::getOpMinControl(actID)[cp] ;
            RPGBuilder::setOpMinControl(actID,cpIDIn,lp->getSingleSolutionVariableValue(optimise) );
            if (cpdebug) cout <<   "    +   is now " << RPGBuilder::getOpMinControl(actID)[cp]  << endl;

            lp->setMaximiseObjective(true);
            lp->mustSolve(false);

            /// recalculate the maximum bound of cp
            if (cpdebug) cout <<   "    +   Maximum limit of control parameter of " << (-100 -cp) << " was " << RPGBuilder::getOpMaxControl(actID)[cp] ;
            RPGBuilder::setOpMaxControl(actID,cpIDIn,lp->getSingleSolutionVariableValue(optimise) );
            if (cpdebug) cout <<   "    +   is now " << RPGBuilder::getOpMaxControl(actID)[cp] << endl ;
            }

};

MiniLP::~MiniLP()
{    controlVariables.resize(16,-1);
    lastLPVariable.resize(RPGBuilder::getPNECount(), -1);
    applyToStart.resize(RPGBuilder::getPNECount(), -1);
    applyToEnd.resize(RPGBuilder::getPNECount(), -1);
    stepID = 0;
    initialised = false;
    delete lp;
};

const MiniLP::Constraint* MiniLP::buildConstraint(RPGBuilder::RPGNumericPrecondition & pre)
{
    if (cpdebug) cout << pre << " with op " << pre.op << " becomes:";

    Constraint toReturn;

    switch (pre.op) {
    case VAL::E_GREATER: {
        toReturn.upper = DBL_MAX;
        toReturn.lower = pre.RHSConstant + EPSILON/10; // HACK - some small value to fake greater-than using a GE
        if (cpdebug) cout << " >= " << toReturn.lower;
        break;
    }
    case VAL::E_GREATEQ: {
        toReturn.upper = DBL_MAX;
        toReturn.lower = pre.RHSConstant;
        if (cpdebug) cout << " >= " << toReturn.lower;
        break;
    }
    case VAL::E_LESS: {
        toReturn.lower = -DBL_MAX;
        toReturn.upper = pre.RHSConstant - EPSILON/10; // HACK - some small value to fake less-than using a LE
        if (cpdebug) cout << " <= " << toReturn.upper;
        break;
    }
    case VAL::E_LESSEQ: {
        toReturn.lower = -DBL_MAX;
        toReturn.upper = pre.RHSConstant;
        if (cpdebug) cout << " <= " << toReturn.upper;
        break;
    }
    case VAL::E_EQUALS: {
        toReturn.lower = toReturn.upper = pre.RHSConstant;
        break;
    }
    };

    int v = pre.LHSVariable;
    if (v < numVars) { // simple variable or duration &c
        toReturn.weights = vector<double>(1);
        toReturn.variables = vector<int>(1);

        toReturn.weights[0] = pre.LHSConstant;
        toReturn.variables[0] = pre.LHSVariable;
        if (cpdebug) cout << "\n Constraint on simple variable: \n";
    } else {
        v -= numVars;

        if (v < numVars) { // negative variable

            if (cpdebug) cout << "\n Constraint on negative variable: \n";
            toReturn.weights = vector<double>(1);
            toReturn.variables = vector<int>(1);

            toReturn.weights[0] = -1 * pre.LHSConstant;
            toReturn.variables[0] = pre.LHSVariable;
        } else {
            if (cpdebug) cout << "\n Constraint on AV: \n";

            RPGBuilder::ArtificialVariable & av = RPGBuilder::getArtificialVariable(pre.LHSVariable);
            const int loopLim = av.size;
            toReturn.weights = vector<double>(loopLim);
            toReturn.variables = vector<int>(loopLim);

            for (int s = 0; s < loopLim; ++s) {
                toReturn.weights[s] = av.weights[s];
                int lv = av.fluents[s];
                if (lv >= numVars) {
                    lv -= numVars;
                    toReturn.weights[s] *= -1;
                }
                toReturn.variables[s] = lv;
            }

            if (pre.op == VAL::E_GREATER || pre.op == VAL::E_GREATEQ) {
                toReturn.lower -= av.constant; // constant term from the AV
            } else if (pre.op == VAL::E_EQUALS) {
                toReturn.lower -= av.constant;
                toReturn.upper -= av.constant;
            } else {
                toReturn.upper += av.constant; // constant term from the AV
            }

        }
    }

    return Constraint::requestConstraint(toReturn);
};

void MiniLP::addActionVariablesAndConstraints(const int & controlNum, const int & actID, const int & stepID ){

 static vector<pair<int,double> > emptyEntries(0);

    {      /**  add control parameter LP variables, and constraints. */
     int nextCP = lp->getNumCols();
       for(int  cp = 0; cp < controlNum; ++cp){  //nextCP = numVars + cp

        lp->addCol(emptyEntries, 0.0, LPinfinity, MILPSolver::C_REAL);

        if (nameLPElements) {   // control parameter variables are created
            ostringstream namestream;
            namestream << "?control" << cp << "act" << stepID;
            string asString = namestream.str();
            lp->setColName(nextCP, asString);
        if (cpdebug) {cout << "C" << nextCP << " becomes " << asString << "\n";};
        }
        //lp->setColLower(nextCP, 0.0);
       // lp->setColUpper(nextCP, 99999);

        ControlParameterAdder CPAdder(cp); // control param constraints are added. CPMin and CPMax

        const vector<RPGBuilder::RPGControl*> & currCEs = RPGBuilder::getRPGCEs(actID);

        if(!(*(currCEs[cp]))[0].empty()) {
        CPAdder.setTS(nextCP, VAL::E_GREATEQ);
        for_each((*(currCEs[cp]))[0].begin(), (*(currCEs[cp]))[0].end(), CPAdder);
        }

        if(!(*(currCEs[cp]))[1].empty()) {
        CPAdder.setTS(nextCP, VAL::E_LESSEQ);
        for_each((*(currCEs[cp]))[1].begin(), (*(currCEs[cp]))[1].end(), CPAdder);
        }
        controlVariables[cp] = nextCP;
		nextCP++;
        }
    }

        for (int currTS = 0; currTS < 2; ++currTS) { // currTS = 0 -> AT START  // currTS = 1 -> AT END

        ConstraintAdder adder(stepID, currTS);
           { // preconditions
            list<const Constraint*> & currList = constraints[actID][currTS];
            for_each(currList.begin(), currList.end(), adder);
            }

          // { // effects
          //  list<RPGBuilder::RPGNumericEffect* > & currEffs = instantEffects[actID][currTS];
          //  for_each(currEffs.begin(), currEffs.end(), adder);
         //  }
        }

};

void MiniLP::initialise(const int & a){

initialised = true;
const int actCount = RPGBuilder::getFixedDEs().size();

const bool initDebug = cpdebug;

    numVars = RPGBuilder::getPNECount();

    if(initDebug) cout << "  - initialising MiniLP lookups " << endl;
    {
        LiteralSet propositional; // don't care...
        RPGBuilder::getInitialState(propositional, initialValues);
    }   // get initial values of variables.
  // cout << "initial value is " << initialValues[0] << " " << initialValues[1] << endl;
    instantEffects.resize(actCount);
    constraints.resize(actCount);

            { // instantaneous effects
                vector<list<RPGBuilder::RPGNumericEffect* > > & toFill = instantEffects[a] = vector<list<RPGBuilder::RPGNumericEffect* > >(2);

                for (int pass = 0; pass < 2; ++pass) {
                    list<int> & currList = (pass ? RPGBuilder::getEndEffNumerics()[a] : RPGBuilder::getStartEffNumerics()[a]);

                    list<int>::iterator clItr = currList.begin();
                    const list<int>::iterator clEnd = currList.end();

                    for (; clItr != clEnd; ++clItr) {

                        RPGBuilder::RPGNumericEffect & currEff = RPGBuilder::getNumericEff()[*clItr];

                        toFill[pass].push_back(&currEff);

                        if (initDebug) {
                            if (pass) {
                                cout << *(RPGBuilder::getInstantiatedOp(a)) << " end writes to " << *(RPGBuilder::getPNE(currEff.fluentIndex)) << "\n";
                            } else {
                                cout << *(RPGBuilder::getInstantiatedOp(a)) << " start writes to " << *(RPGBuilder::getPNE(currEff.fluentIndex)) << "\n";
                            }
                        }
                    }
                };
            }

            { // preconditions (not invariants)
                vector<list<const Constraint*> > & toFill = constraints[a] = vector<list<const Constraint*> >(2);

                for (int pass = 0; pass < 2; ++pass) {
                    list<int> & currList = (pass ? RPGBuilder::getEndPreNumerics()[a] : RPGBuilder::getStartPreNumerics()[a]);

                    list<int>::iterator clItr = currList.begin();
                    const list<int>::iterator clEnd = currList.end();

                    for (; clItr != clEnd; ++clItr) {
                        toFill[pass].push_back(buildConstraint(RPGBuilder::getNumericPreTable()[*clItr]));

                    }
                };
            }

};

};
