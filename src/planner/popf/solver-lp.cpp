#include "solver-lp.h"
#include <lp_lib.h>

#include <iostream>
using std::cout;
using std::endl;

#ifndef MULTIPLESOLVERS
void readParams(char * argv[], const int & a)
{

}
#endif

MILPSolverLP::MILPSolverLP(): solptr(0), rows(0), cols(0)
{
    lp = make_lp(0,0);
}

MILPSolverLP::MILPSolverLP(const MILPSolverLP & mlp) : solptr(0), rows(mlp.rows), cols(mlp.cols)
{
    lp = copy_lp(mlp.lp);
}

MILPSolverLP::~MILPSolverLP()
{
    delete_lp(lp);
    delete[] solptr;
}

MILPSolver * MILPSolverLP::clone()
{
    return new MILPSolverLP(*this);
}

double MILPSolverLP::getInfinity()
{
    return 1e30;
}

int MILPSolverLP::getNumCols()
{
    return cols;
}
int MILPSolverLP::getNumRows()
{
    return rows;
}

void MILPSolverLP::setColName(const int & var, const string & asString)
{
    set_col_name(lp,var+1,const_cast<char*>(asString.c_str()));
}

string MILPSolverLP::getColName(const int & var)
{
    return string(get_col_name(lp,var+1));
}
void MILPSolverLP::setRowName(const int & cons, const string & asString)
{
    set_row_name(lp,2*cons+1,const_cast<char*>(asString.c_str()));
}
string MILPSolverLP::getRowName(const int & cons)
{
    return string(get_row_name(lp,2*cons+1));
}

double MILPSolverLP::getColUpper(const int & var)
{
    return get_upbo(lp,var+1);
}
void MILPSolverLP::setColUpper(const int & var, const double & b)
{
    //cout << "Setting constraint on C" << var+1 << " to [Inf"<< "," << b << "] Got " << cols << "\n";
    set_upbo(lp,var+1,b);
}
double MILPSolverLP::getColLower(const int & var)
{
    return get_lowbo(lp,var+1);
}
void MILPSolverLP::setColLower(const int & var, const double & b)
{
    //cout << "Setting constraint on C" << var+1 << " to [" << b << ",Inf] Got" << cols << "\n";
    set_lowbo(lp,var+1,b);
}
void MILPSolverLP::setColBounds(const int & var, const double & lb, const double & ub)
{
    //cout << "Setting constraint on C" << var+1 << " to [" << lb << "," << ub << "] Got " << cols << "\n";
    set_bounds(lp,var+1,lb,ub);
}

bool MILPSolverLP::isColumnInteger(const int & var)
{
    return (is_int(lp,var+1)==1);
}

double MILPSolverLP::getRowUpper(const int & var)
{
    return get_rh(lp,2*var+2);
}
void MILPSolverLP::setRowUpper(const int & var, const double & b)
{
    set_rh(lp,2*var+2,b);
}
double MILPSolverLP::getRowLower(const int & var)
{
    return get_rh(lp,2*var+1);
}
void MILPSolverLP::setRowLower(const int & var, const double & b)
{
    set_rh(lp,2*var+1,b);
}

void MILPSolverLP::addCol(const vector<pair<int,double> > & entries, const double & lb, const double & ub, const ColumnType & type)
{
    double cl[2*rows+1];
    for(int i = 0;i < rows;++i)
    {
        cl[2*i] = 0;
        cl[2*i+1] = 0;
    }
    cl[2*rows] = 0;
    for(vector<pair<int,double> >::const_iterator i = entries.begin();i != entries.end();++i)
    {
        cl[2*i->first+1]=i->second;
        cl[2*i->first+2]=i->second;
    }
    add_column(lp,cl);
    ++cols;
    //cout << "Adding C" << cols-1 << " and Setting constraint on C" << cols-1 << " to [" << lb << "," << ub << "] Got " << cols << "\n";
    setColBounds(cols-1,lb,ub);
}
void MILPSolverLP::addRow(const vector<pair<int,double> > & entries, const double & lb, const double & ub)
{
    double rvs[1+cols];
    for(int i = 0;i <= cols;++i)
    {
        rvs[i] = 0;
    }
    for(vector<pair<int,double> >::const_iterator i = entries.begin();i != entries.end();++i)
    {
        rvs[i->first + 1] = i->second;
    }
    add_constraint(lp,rvs,2,lb);
    add_constraint(lp,rvs,1,ub);
    ++rows;
}
void MILPSolverLP::setMaximiseObjective(const bool & maxim)
{
    if(maxim)
    {
        set_maxim(lp);
    }
    else
    {
        set_minim(lp);
    }
}

void MILPSolverLP::clearObjective()
{
    double ovs[1+cols];
    for(int i = 0;i <= cols;++i)
    {
        ovs[i] = 0;
    }
    set_obj_fn(lp,ovs);
}
void MILPSolverLP::setObjective(double * const entries)
{
    double ovs[1+cols];
    for(int i = 1;i <= cols;++i)
    {
        ovs[i] = entries[i-1];
    }
    ovs[0] = 0;
    set_obj_fn(lp,ovs);
}
void MILPSolverLP::setObjCoeff(const int & var, const double & w)
{
    set_obj(lp,var+1,w);
}

void MILPSolverLP::writeLp(const string & filename)
{
    write_lp(lp,const_cast<char*>(filename.c_str()));
}
bool MILPSolverLP::solve(const bool & skipPresolve)
{
    // Should set presolve...
    //static char name[] = "FirstLPa";

    //cout << "Writing "<<name << "\n";
    //writeLp(name);
    //++name[7];
    return (::solve(lp)==0);
}

const double * MILPSolverLP::getSolution()
{
    double * sol;
    get_ptr_primal_solution(lp,&sol);
    delete[] solptr;
    solptr = new double[cols];
    for(int i = 0;i < cols;++i)
    {
        solptr[i] = sol[i+2*rows+1];
    }
    return solptr;
}
const double * MILPSolverLP::getSolutionRows()
{
    double * sol;
    get_ptr_primal_solution(lp,&sol);
    delete[] solptr;
    solptr = new double[rows];
    for(int i = 0;i < rows;++i)
    {
        solptr[i] = sol[2*i+1];
    }
    return solptr;
}
const double * MILPSolverLP::getPartialSolution(const int & from, const int & to)
{
    if(from >= to) return 0;
    double * sol;
    get_ptr_primal_solution(lp,&sol);
    delete[] solptr;
    solptr = new double[to - from];
    for(int i = from;i < to;++i)
    {
        solptr[i-from] = sol[i+2*rows+1];
    }
    return solptr;
}
double MILPSolverLP::getObjValue()
{
    return get_objective(lp);
}

void MILPSolverLP::getRow(const int & i, vector<pair<int,double> > & entries)
{
    double rvals[1+cols];
    get_row(lp,2*i+1,rvals);
    for(int ii = 0;ii < cols;++ii)
    {
        entries.push_back(std::make_pair(ii,rvals[1+ii]));
    }
}

bool MILPSolverLP::supportsQuadratic() const
{
    return false;
}

void MILPSolverLP::hush()
{
    set_verbose(lp,1);
}



#ifndef MULTIPLESOLVERS

MILPSolver * getNewSolver()
{
    MILPSolver * const toReturn = new MILPSolverLP();
    return toReturn;
}

const double LPinfinity = 1e30;

#endif
