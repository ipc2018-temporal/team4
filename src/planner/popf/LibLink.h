#ifndef LIBLINK_H_INCLUDED
#define LIBLINK_H_INCLUDED

#include <ptree.h>
#include <string>
#include <vector>

using std::string;
using std::vector;

using VAL::class_def;

namespace Planner {

class LibLoader {
private:
    static void * getLib(class_def * cd);
public:
    static double makeCall(class_def * cd,const string & nm,vector<string> args);
};


};



#endif // LIBLINK_H_INCLUDED
