/************************************************************************
 * Copyright 2009, 2011, Strathclyde Planning Group,
 * Department of Computer and Information Sciences,
 * University of Strathclyde, Glasgow, UK
 * http://planning.cis.strath.ac.uk/
 *
 * Amanda Coles, Andrew Coles, Maria Fox, Derek Long - POPF
 * Maria Fox, Richard Howey and Derek Long - VAL
 * Stephen Cresswell - PDDL Parser
 *
 * This file is part of POPF.
 *
 * POPF is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * POPF is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with POPF.  If not, see <http://www.gnu.org/licenses/>.
 *
 ************************************************************************/
#include "FFSolver.h"

#include <map>
#include <list>
#include <set>
#include <vector>
#include <cstring>

using std::map;
using std::list;
using std::set;
using std::vector;

class MILPSolver;

namespace Planner
{

class MiniLP{

public:
    MiniLP(const int & actID, const int & cp);
    ~MiniLP();
    static void MiniLPPrint(MILPSolver *lp);
  //  static vector<bool> oneToManyCase;

protected:

    /** @brief Class to encode a precondition constraint in a form that can be efficiently added to an LP. */
    class Constraint
    {

    public:
        vector<double> weights;
        vector<int> variables;

        double lower;
        double upper;

        Constraint() : lower(0.0), upper(0.0) {
        };

        static const Constraint * requestConstraint(const Constraint & c) {
            const pair<set<Constraint>::iterator, bool> insResult = constraintStore.insert(c);
            if (insResult.second) {
                insResult.first->ID = constraintCount++;
            }
            return &(*(insResult.first));
        }

        bool operator <(const Constraint & c) const {
            const unsigned int thisVC = weights.size();
            const unsigned int otherVC = c.weights.size();

            if (thisVC < otherVC) return true;
            if (thisVC > otherVC) return false;

            if (fabs(lower - c.lower) > 0.0000001) {
                if (lower < c.lower) return true;
                if (lower > c.lower) return false;
            }

            if (fabs(upper - c.upper) > 0.0000001) {
                if (upper < c.upper) return true;
                if (upper > c.upper) return false;
            }


            for (unsigned int i = 0; i < thisVC; ++i) {
                if (variables[i] < c.variables[i]) return true;
                if (variables[i] > c.variables[i]) return false;

                if (fabs(weights[i] - c.weights[i]) > 0.0000001) {
                    if (weights[i] < c.weights[i]) return true;
                    if (weights[i] > c.weights[i]) return false;
                }
            }

            return false;
        }

    protected:

        static set<Constraint> constraintStore;
        static int constraintCount;
        mutable int ID;
        friend class ConstraintPtrLT;
    };

    struct ConstraintAdder;
    friend struct ConstraintAdder;
    struct ControlParameterAdder;
    friend struct ControlParameterAdder;

	/** @brief The LP for the plan being evaluated. */
    static MILPSolver * lp;

        /** @brief Whether LP elements should be named.
     *
     * This is an internal flag for debugging.  If set to <code>true</code>, i.e. if
     * <code>LPScheduler::lpDebug</code> is non-zero, the rows and columns of the LP
     * are given meaningful names, rather than the defaults.  As naming the elements
     * carries a small overhead, and serves no purpose other than making the resulting
     * LP easier for humans to comprehend, the variable takes a value of <code>false</code>
     * unless LP debugging output is enabled.
     */
    static bool nameLPElements;

    /** @brief The LP variables of each control variables in corresponding plan steps. */
    static vector<int> controlVariables;
    static vector<int> applyToStart;
    static vector<int> applyToEnd;
    static vector<int> firstOptimalValuesMin;
    static vector<int> secondOptimalValuesMin;
    static vector<int> firstOptimalValuesMax;
    static vector<int> secondOptimalValuesMax;

    /** @brief The preconditions of the each action.
     *
     * Each entry is itself a vector:
     *  - <code>constraints[a][0]</code> contains the conditions that must hold at the start of action <code>a</code>
     *  - <code>constraints[a][1]</code> contains the conditions that must hold throughout the execution of action <code>a</code>
     *  - <code>constraints[a][2]</code> contains the conditions that must hold at the end of action <code>a</code>
     */
    static vector<vector<list<const Constraint*> > > constraints;

    static const Constraint* buildConstraint(RPGBuilder::RPGNumericPrecondition & d);

    /** @brief The values of each variable in the initial state */
    static vector<double> initialValues;

    static vector<int> lastLPVariable;

    /** @brief  Initialise the lookup tables used when building the LP. */
    static void initialise(const int & a);

    static void addActionVariablesAndConstraints(const int & cnt, const int & a, const int & s);

        /** @brief The instantaneous numeric effects of each action.
     *
     * Each entry is itself a vector:
     *  - <code>instantEffects[a][0]</code> contains the instantaneous numeric effects for the start of action <code>a</code>
     *  - <code>instantEffects[a][1]</code> contains the instantaneous numeric effects for the end of action <code>a</code>
     */
    static vector<vector<list<RPGBuilder::RPGNumericEffect* > > > instantEffects;

    /** @brief  Control how much debugging information is printed. */
    static bool cpdebug;
    static int numVars;
    static bool initialised;
    static bool addingSecondAction;
    static int stepID;
    static int nextPNEStart;
};
};
