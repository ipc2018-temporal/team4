if (earliestTIL.first != INT_MAX) {
  for (int tilID = theState.nextTIL; tilID <= earliestTIL.first; ++tilID) {
    const double tilTime = RPGBuilder::getTILVec()[tilID]->duration;
            
    list<pair<double, list<ActionSegment> > >::iterator rpItr = relaxedPlan.begin();
    const list<pair<double, list<ActionSegment> > >::iterator rpEnd = relaxedPlan.end();
            
    for (; rpItr != rpEnd; ++rpItr) {
      if (rpItr->first > tilTime) {
	break;
      }
    }
            
    if (rpItr != rpEnd) {

      if (EpsilonResolutionTimestamp(rpItr->first,true) == EpsilonResolutionTimestamp(tilTime,true) ) {
	rpItr->second.push_back(ActionSegment((instantiatedOp*) 0,VAL::E_AT,tilID, RPGHeuristic::emptyIntList));
	++h;
	if (tilID == theState.nextTIL) {
	  helpfulActions.push_back(rpItr->second.back());
	}
                    
      } else {
	pair<double, list<ActionSegment> > insPair;
                    
	insPair.first = tilTime;                    
	rpItr = relaxedPlan.insert(rpItr, insPair);
                    
	rpItr->second.push_back(ActionSegment((instantiatedOp*) 0,VAL::E_AT,tilID, RPGHeuristic::emptyIntList));
	++h;
                    
	if (tilID == theState.nextTIL) {
	  helpfulActions.push_back(rpItr->second.back());
	}
                    
      }
    } else {
      pair<double, list<ActionSegment> > insPair;
                    
      insPair.first = tilTime;                    
      relaxedPlan.push_back(insPair);
      relaxedPlan.back().second.push_back(ActionSegment((instantiatedOp*) 0,VAL::E_AT,tilID, RPGHeuristic::emptyIntList));
      ++h;
      if (tilID == theState.nextTIL) {
	helpfulActions.push_back(rpItr->second.back());
      }
    }
  }
 }
