#ifndef __AllowViolation
#define __AllowViolation

#include <iostream>
#include <utility>
#include <string>
#include <list>
#include <set>

namespace AllowViolation {
    extern bool allowViolation;
    extern std::set <int> precViolated;
    extern bool isNowViolated;
    extern std::list<int> stepInBetween;
    extern bool isNotViolatedAnymore;
    extern int oldStepID;
};

#endif
