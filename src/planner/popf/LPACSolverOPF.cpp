#include <iostream>
#include <cassert>
#include <OsiClpSolverInterface.hpp>
#include <CoinPackedMatrix.hpp>
#include <CoinPackedVector.hpp>
#include <math.h>
#include <map>
#include <sstream>
#include "Network.h"
#include "Busbar.h"
#include "Solver.h"
#include "LPACSolver.h"
#include "LPACSolverOPF.h"

using namespace std;

LPACSolverOPF::LPACSolverOPF(Network* net, int t, int seg):LPACSolver(net,t,seg){
}

bool LPACSolverOPF::solvePF(){
  cout << "Inside LPAC solver OPF" << endl;
  buildModel();
  return solveModel();
}


void LPACSolverOPF::buildModel(){
 
  calculateCosApproximation();
  map<pair<int, int>, double> mapTap = theNetwork->getMapTap();
  // column definition
  map<int, Generator*> generators = theNetwork->getGenerators();
  int nGen = generators.size();
  nCol = 2*nBus+nBranch+2*nGen;
  
  cout << "N variables : " << nCol << endl;
  double *objective = new double[nCol];
  double *col_lb = new double[nCol];
  double *col_ub = new double[nCol];
  string *col_name = new string[nCol];
  map<pair<int, int>, int> mapCos;
  map<int, int> mapDelta;
  map<int, int> mapPhi;
  map<int, Busbar*>::iterator busIt = busbars.begin();
  map<int, Busbar*>::iterator busEnd = busbars.end();
  double weigthCos = 1;
  double weigthGen = 0;
  for(int i=0;busIt!=busEnd;++i, ++busIt){
    stringstream namePhi;
    int indexPhi = i+nBus;
    stringstream name;
    name << "delta_{" << busIt->second->getBus_i() <<"}";
    
    col_name[i]=name.str();
    //    cout << i << " " << name.str() << " added " << endl;
    col_lb[i]=-model->getInfinity();
    col_ub[i]=model->getInfinity();
    objective[i]=0;
    model->setColName(i, col_name[i]);
    mapDelta[busIt->second->getBus_i()]=i;
    mapPhi[busIt->second->getBus_i()]=indexPhi;

    namePhi <<"phi_{" << busIt->second->getBus_i() << "}";
    //    cout << indexPhi << " " << namePhi.str() << " added " << endl;
    col_name[indexPhi]=namePhi.str();
    col_lb[indexPhi]=busIt->second->getVmin()-voltageTarget[busIt->first];
    col_ub[indexPhi]=busIt->second->getVmax()-voltageTarget[busIt->first];
    objective[indexPhi]=0;
    model->setColName(indexPhi, col_name[indexPhi]);
  }


  map<int, Branch*>::iterator brIt = branches.begin();
  map<int, Branch*>::iterator brEnd = branches.end();
  for(int i=2*nBus;brIt!=brEnd;++i,++brIt){
    Branch *branch = brIt->second;
    stringstream name;
    int fBus = branch->getFBus();
    int tBus = branch->getTBus();
    name << "cos" << fBus << "_"<< tBus;
    mapCos[make_pair(fBus,tBus)]=i;
    mapCos[make_pair(tBus,fBus)]=i;
    col_name[i]=name.str();
    col_lb[i]=0;
    col_ub[i]=1.;
    objective[i]=-weigthCos;
    model->setColName(i, col_name[i]);
    //    cout << i << " " << name.str() << " added " << endl;
  }
  
  map<int, Generator*>::iterator genIt = generators.begin();
  map<int, Generator*>::iterator genEnd = generators.end();
  map<int, GeneratorCostData*> gcd=theNetwork->getGCD();
  for(int i=2*nBus+nBranch;genIt!=genEnd;++genIt,++i){
    Generator *generator = genIt->second;
    stringstream name;
    int idGen = genIt->first;
    int idBus = generator->getBus();
    int index = i + nGen;
    name << "pGen" << idGen;
    col_name[i]=name.str();
    col_lb[i]=generator->getPmin();
    col_ub[i]=generator->getPmax();
    objective[i]=-weigthGen*gcd[idGen]->getX1();
    mapPGen[idBus]=i;
    stringstream nameQ;
    nameQ << "qGen" << idGen;
    col_name[index]=nameQ.str();
    col_lb[index]=generator->getQmin();
    col_ub[index]=generator->getQmax();
    mapQGen[idBus]=index;
    objective[index]=0;//-weigthGen*gcd[idGen]->getX1();
  }
  // set row
  nRow = 2*nBus+s*nBranch;
  double *row_lb = new double[nRow];
  double *row_ub = new double[nRow];
  
  // Define constraint matrix;
  
  CoinPackedMatrix *matrix = new CoinPackedMatrix(false, 0, 0);
  matrix->setDimensions(0, nCol);

  

  //
  busIt = busbars.begin();
  map<int,CoinPackedVector> rows;
  for(int i=0;busIt!=busEnd;++i, ++busIt){
    int idBus = busIt->first;
    Busbar *busbar = busIt->second;
    int type = busbar->getType();
    int indexP = i;
    int indexQ = i+nBus;
    if(type==3){
      rows[i].insert(i,1.);
      row_lb[i]=0.;
      row_ub[i]=0.;

      rows[indexQ].insert(indexQ,1.);
      row_lb[indexQ]=voltage[idBus]-1.;
      row_ub[indexQ]=voltage[idBus]-1.;
    }else{
      int indexB = busIt->first;
      double totalP = -activePowerLoad[indexB];
      double totalQ = -reactivePowerLoad[indexB];
      //      cout << "bus " << i << " " <totalP << " ";
      map<int,Busbar*>::iterator busSIt=busbars.begin();
      map<int,Busbar*>::iterator busSEnd=busbars.end();
      map<int, double> coefficientsP;
      map<int, double> coefficientsQ;
      for(;busSIt!=busSEnd;++busSIt){
	Busbar* busbarS = busSIt->second;
	int idBusS = busbarS->getBus_i();
	if(idBus==idBusS){
	  continue;
	}
	pair<int,int> index(idBus,idBusS);
	if(mapCos.find(index)==mapCos.end()){
	  continue;
	}
	totalP-=reY[index]/mapTap[index];
	totalQ+=(imY[index]-bY[index])/mapTap[index];
	int idCos = mapCos[index];
      
	coefficientsP[idCos]-=reY[index];
	coefficientsP[mapDelta[idBus]]-=(imY[index]);
	coefficientsP[mapDelta[idBusS]]+=(imY[index]);
	if(type==1){

	  coefficientsQ[mapDelta[idBus]]-=reY[index];
	  coefficientsQ[mapDelta[idBusS]]+=reY[index];
	  coefficientsQ[idCos]+=(imY[index]);
	  coefficientsQ[mapPhi[idBus]]-=(imY[index]);
	  coefficientsQ[mapPhi[idBusS]]+=(imY[index]);   
	}
      }
      if(mapPGen.find(idBus)!=mapPGen.end()){
	coefficientsP[mapPGen[idBus]]=1.;
	if(type==1){                    // change this
	  coefficientsQ[mapQGen[idBus]]=1.;
	}
      }
      
      row_lb[indexP]=totalP;
      row_ub[indexP]=totalP;
      if(type==2){                    // and this
        coefficientsQ[indexQ]=1;
        row_lb[indexQ]=voltage[idBus]-1;
        row_ub[indexQ]=voltage[idBus]-1;
      }else{
        row_lb[indexQ]=totalQ;
        row_ub[indexQ]=totalQ;
      }
    
      map<int, double>::iterator cpIt = coefficientsP.begin();
      map<int, double>::iterator cpEnd = coefficientsP.end();
      for(;cpIt!=cpEnd;++cpIt){
	rows[indexP].insert(cpIt->first,cpIt->second);
      }
      map<int, double>::iterator cqIt = coefficientsQ.begin();
      map<int, double>::iterator cqEnd = coefficientsQ.end();
      for(;cqIt!=cqEnd;++cqIt){
	rows[indexQ].insert(cqIt->first,cqIt->second);
      }
    }
  }
  brIt=branches.begin();
  //map<int,Branch*>::iterator brEnd=branches.end();
  for(int i=2*nBus;brIt!=brEnd;++brIt){
    Branch *branch = brIt->second;
    int idBusT = branch->getTBus();
    int idBusF = branch->getFBus();
    pair<int,int>index(idBusT,idBusF);
    for(int j=0;j<s;j++,i++){
      double sloc = mCos[j];
      double max = qCos[j];
      double min = -model->getInfinity();
      rows[i].insert(mapCos[index],1);
      rows[i].insert(mapDelta[idBusT],-sloc);
      rows[i].insert(mapDelta[idBusF],sloc);
      row_lb[i]=min;
      row_ub[i]=max;
    }
  }
  
  for(int i=0;i<nRow;i++){
    matrix->appendRow(rows[i]);
  }
  model->loadProblem(*matrix, col_lb, col_ub, objective, row_lb, row_ub);
}


void LPACSolverOPF::calculatePowerGenerators(){
  Solver::calculatePowerGenerators();
  map<int, Generator*>::iterator genIt=generators.begin();
  map<int, Generator*>::iterator genEnd=generators.end();
  for(int i=2*nBus+nBranch;genIt!=genEnd;++genIt,++i){
    Generator *generator = genIt->second;
    int idBus = generator->getBus();
    map<int, Busbar*>::iterator busIt = busbars.find(idBus);
    if(busIt  == busbars.end()){
      assert("Busbar not found");
    }
    Busbar *busbar = busIt->second;
    int type = busbar->getType();
    if (type==1){
      generator->setPg(solution[i]*baseMVA);
      generator->setQg(solution[i+generators.size()]*baseMVA);
      
    }
    
    
  }
  
}
