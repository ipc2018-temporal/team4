#ifndef __LPACSolver_h__
#define __LPACSolver_h__

#include "Solver.h"
//#include <OsiClpSolverInterface.hpp>

class LPACSolver: public Solver{
 public:
  LPACSolver(Network* net, int t = 2, int seg = 7);
  virtual bool solvePF();
 protected:
  /***
      type 0 : hot start
            1 : warm start
	    2 : cold start [default]
   */
  int type;
  int s;
  OsiClpSolverInterface *model;
  double* mCos;
  double* qCos;
  map<int, double> voltageTarget;
  map<pair<int, int> , double> cosine;
  map<pair<int, int>, int> mapCos;
  map<int, int> mapDelta;
  map<int, int> mapPhi;
  int nCol;
  int nRow;
  int nBus;
  int nBranch;
  const double *solution;
  virtual void buildModel();
  bool solveModel();
  void setColumn();
  void calculateCosApproximation();
  void readModel();
  virtual void calculateLinePF();
  void calculateAccordingModel();
  virtual void takeSolution();
};

#endif
