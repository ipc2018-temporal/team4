#ifndef __Solver_h__
#define __Solver_h__

#include <set>
#include "Network.h"
#include "Busbar.h"
#include "Generator.h"
#include "Branch.h"

using namespace std;

class Network;

class Solver{
 public:
  Solver();
  Solver(int i);
  Solver(Network* net);
  virtual ~Solver(){
    cout << "close solver" << endl;
  };
  bool solve();
  void printSolution();
  void updateNetwork(){
    initialValue();
  }
  bool getIsOptimal(){
    return isOptimal;
  }
 protected:
  Network* theNetwork;
  int nNode;
  bool isSolved;
  bool isOptimal;
  map<int,double> voltage;
  map<int, double> delta;
  map<int, Busbar*> busbars;
  map<int, Generator*> generators;
  map<int, Branch*> branches;
  map<pair<int,int>, double> reY;
  map<pair<int,int>, double> imY;
  map<pair<int,int>, double> bY;
  map<int, double> activePowerGen;
  map<int, double> activePowerLoad;
  map<int, double> reactivePowerGen;
  map<int, double> reactivePowerLoad;
  set<int> toShed;
  int baseMVA;
  virtual bool solvePF();
  virtual void calculateLinePF();
  virtual void calculatePowerGenerators();
  virtual void calculateObjectiveFunction();
 private:
  void initialValue();
  virtual void passResultToNetwork();
};

#endif
