#ifndef __IPSA
#define __IPSA

#include <iostream>
#include <utility>
#include <string>
#include <list>
#include <sstream>
#include <fstream>
#include <map>
#include "Network.h"
#include "solver.h"

#define NBUSMAX 100000

using namespace std;

namespace IPSA{
  extern char* fileName="";
  extern char* moduleName="";
  extern bool isIPSA = false;
  std::map<std::string, double> newStatus;
  extern int count = 0;
  extern std::ostringstream datfn;
  extern std::string fn = "functions.dat";
  extern bool isGood = false;
  extern std::ofstream datfile(fn.c_str());
  extern bool restore = false;
  extern Network theNetwork(0);
  extern Solver* solver(0);
}

#endif
